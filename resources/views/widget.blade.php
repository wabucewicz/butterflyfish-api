<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ $boat->name }} - schedule</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

    <style type="text/css">

        /*modal fullscreen */

        .modal.modal-fullscreen {
            /* Maximize the main wrappers on the screen */
            /* Make the parent wrapper of the modal box a full-width block */
            /* Remove borders and effects on the content */
            /**
               * /!\ By using this feature, you force the header and footer to be placed
               * in an absolute position. You must handle by yourself the margin of the
               * content.
               */
        }
        .modal.modal-fullscreen .modal-dialog,
        .modal.modal-fullscreen .modal-content {
            bottom: 0;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
        }
        .modal.modal-fullscreen .modal-dialog {
            margin: 0;
            width: 100%;
            animation-duration:0.6s;
        }
        .modal.modal-fullscreen .modal-content {
            border: none;
            -moz-border-radius: 0;
            border-radius: 0;
            -webkit-box-shadow: inherit;
            -moz-box-shadow: inherit;
            -o-box-shadow: inherit;
            box-shadow: inherit;
            /* change bg color below */
            /* background:#1abc9c; */
        }
        .modal.modal-fullscreen.force-fullscreen {
            /* Remove the padding inside the body */
        }
        .modal.modal-fullscreen.force-fullscreen .modal-body {
            padding: 0;
        }
        .modal.modal-fullscreen.force-fullscreen .modal-header,
        .modal.modal-fullscreen.force-fullscreen .modal-footer {
            left: 0;
            position: absolute;
            right: 0;
        }
        .modal.modal-fullscreen.force-fullscreen .modal-header {
            top: 0;
        }
        .modal.modal-fullscreen.force-fullscreen .modal-footer {
            bottom: 0;
        }
        button.close {
            -webkit-appearance: none;
            padding: 0px;
            cursor: pointer;
            background: 0 0;
            border: 0;
            margin-right: 4px;
        }
        .close {
            float: right;
            font-size: 50px;
             font-weight: normal;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            filter: alpha(opacity=20);
            opacity: .2;
        }
        .modal-header {
            padding: 15px;
            border-bottom: none;
        }



    </style>
</head>
<body>

<div class="container">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Date from</th>
            <th>Date to</th>
            <th>Emberekation</th>
            <th>Disemberekation</th>
            <th>Itinerary</th>
            <th>Places</th>
            <th>Price</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($boat->cruises as $cruise)
        <tr>
            <td>{{ $cruise->start_date }}</td>
            <td>{{ $cruise->end_date }}</td>
            <td>{{ $cruise->start_place }}</td>
            <td>{{ $cruise->end_place }}</td>
            <td>{{ isset($cruise->itinerary) ? $cruise->itinerary->name : '-' }}</td>
            <td>{{ $cruise->free_pax }}</td>
            <td>{{ $cruise->price_per_pax > 0 ? $cruise->price_per_pax . ' ' . $cruise->currency : '-' }}</td>
            <td>
                <button class="btn btn-xs btn-primary" data-toggle="modal" data-target="#book-modal" contenteditable="false" data-start="{{ $cruise->start_date }}" data-end="{{ $cruise->end_date }}">Book</button>
            </td>
        </tr>
        @endforeach
    </table>
</div>

<div class="modal fade modal-fullscreen  footer-to-bottom" id="book-modal" tabindex="-1" role="dialog" aria-labelledby="book-modal" aria-hidden="true">
    <div class="modal-dialog animated zoomIn">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <div style="margin: 0 auto; max-width: 600px;">
                <div class="modal-header">
                    <h4 class="modal-title">{{ $boat->name }}:  from <span id="book-start-date"></span> to <span id="book-end-date"></span></h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="first-name">First name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="first-name" placeholder="Enter your first name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="last-name">Last name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="last-name" placeholder="Enter your last name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="people">People</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" id="people" placeholder="Enter number of people">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="email">Email</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="email" placeholder="Enter your email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="phone">Contact phone</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="phone" placeholder="Enter your phone">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                    <button type="button" class="btn btn-primary" onclick="">Book</button>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript">

    $('#book-modal').on('show.bs.modal', function(e) {

        var $modal = $(this);
            $target = $(e.relatedTarget);

        $('#book-start-date').text($target.data('start'));
        $('#book-end-date').text($target.data('end'));

//        $.ajax({
//            cache: false,
//            type: 'POST',
//            url: 'backend.php',
//            data: 'EID=' + essayId,
//            success: function(data) {
//                $modal.find('.edit-content').html(data);
//            }
//        });
    });

</script>

</body>
</html>
