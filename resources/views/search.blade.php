<!DOCTYPE html>
<html lang="en">
<head>
    <title>Search offers</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css">

</head>
<body>

<div class="container">

    <div class="row" style="margin-top: 50px; margin-bottom: 30px;">
        <div class="col-xs-12">
            <h3>Search offers</h3>
        </div>
    </div>
    <div class="row" style="margin-bottom: 30px;">
        <div class="col-xs-12"">
            <form>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="q">What do you want to see?</label>
                            <input type="text" class="form-control" name="q" id="q" placeholder="What do you want to see?" value="{{ Request::input('q') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="from">Date from</label>
                            <input type="text" class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" name="from" id="from" placeholder="Date from" value="{{ Request::input('from') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="to">Date to</label>
                            <input type="text" class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" name="to" id="to" placeholder="Date to" value="{{ Request::input('to') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="pax">Passengers</label>
                            <input type="text" class="form-control" name="pax" id="pax" placeholder="Passengers" value="{{ Request::input('pax') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn btn-block btn-default">Go!</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @if(Request::input('q') && Request::input('from') && Request::input('to') && Request::input('pax') && count($offers) == 0)

        <div class="row">
            <div class="col-xs-12 text-center">Nothing to show</div>
        </div>

    @elseif(Request::input('q') && Request::input('from') && Request::input('to') && Request::input('pax') && count($offers) > 0)

        <div class="row">
            <div class="col-xs-12"">
                <div class="row">
                    <div class="col-md-2">Filters: to do</div>
                    <div class="col-md-10">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Boat</th>
                                <th>Start date</th>
                                <th>End date</th>
                                <th>Embarkation</th>
                                <th>Disembarkation</th>
                                <th>Free pax</th>
                                <th>Price per pax</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($offers as $offer)
                                <tr>
                                    <td>{{ $offer->boat->name  }}</td>
                                    <td>{{ $offer->start_date  }}</td>
                                    <td>{{ $offer->end_date  }}</td>
                                    <td>{{ $offer->start_place  }}</td>
                                    <td>{{ $offer->end_place  }}</td>
                                    <td>{{ $offer->free_pax  }}</td>
                                    <td>{{ $offer->price_per_pax . ' ' . $offer->currency  }}</td>
                                    <td><button class="btn btn-info">Book</button></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    @endif



</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>


</body>
</html>
