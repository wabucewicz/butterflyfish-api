<?php

use App\Models\Boat;
use App\Models\BoatOperator;
use App\Models\CruiseOperator;
use Illuminate\Database\Seeder;

class UpdateMissingHashCodesForOperators extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $boats = Boat::all();
        foreach ($boats as $boat) {
            do {
                $hashCode = md5(uniqid());
            } while ($boats->contains('hash_code', $hashCode));
            $boat->hash_code = $hashCode;
            $boat->save();
        }

        $boatOperators = BoatOperator::all();
        foreach ($boatOperators as $boatOperator) {
            do {
                $hashCode = md5(uniqid());
            } while ($boatOperators->contains('hash_code', $hashCode));
            $boatOperator->hash_code = $hashCode;
            $boatOperator->save();
        }

        $cruiseOperators = CruiseOperator::all();
        foreach ($cruiseOperators as $cruiseOperator) {
            do {
                $hashCode = md5(uniqid());
            } while ($cruiseOperators->contains('hash_code', $hashCode));
            $cruiseOperator->hash_code = $hashCode;
            $cruiseOperator->save();
        }

    }
}
