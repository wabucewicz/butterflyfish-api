<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [

            // BOATS
            [
                'name' => 'boats.list_all',
                'display_name' => 'List all boats',
                'description' => 'User can list all boats'
            ],
            [
                'name' => 'boats.list_associated',
                'display_name' => 'List associated boats',
                'description' => 'User can list only associated boats to him as operator'
            ],
            [
                'name' => 'boats.show_all',
                'display_name' => 'Show all boats',
                'description' => 'User can show all boats'
            ],
            [
                'name' => 'boats.show_associated',
                'display_name' => 'Show associated boats',
                'description' => 'User can show only associated boats to him as operator'
            ],
            [
                'name' => 'boats.create',
                'display_name' => 'Create boats',
                'description' => 'User can create new boats'
            ],
            [
                'name' => 'boats.edit_all',
                'display_name' => 'Edit all boats',
                'description' => 'User can edit all boats'
            ],
            [
                'name' => 'boats.edit_associated',
                'display_name' => 'Edit associated boats',
                'description' => 'User can edit only associated boats to him as operator'
            ],
            [
                'name' => 'boats.delete_all',
                'display_name' => 'Delete all boats',
                'description' => 'User can delete all boats'
            ],
            [
                'name' => 'boats.delete_associated',
                'display_name' => 'Delete associated boats',
                'description' => 'User can delete only associated boats to him as operator'
            ],
            [
                'name' => 'boats.assign_operators',
                'display_name' => 'Assign operators',
                'description' => 'User can assign operators to boat'
            ],

            // DECKS
            [
                'name' => 'decks.list_all',
                'display_name' => 'List all decks',
                'description' => 'User can list all decks'
            ],
            [
                'name' => 'decks.list_associated',
                'display_name' => 'List associated decks',
                'description' => 'User can list only associated decks to him as operator'
            ],
            [
                'name' => 'decks.show_all',
                'display_name' => 'Show all decks',
                'description' => 'User can show all decks'
            ],
            [
                'name' => 'decks.show_associated',
                'display_name' => 'Show associated decks',
                'description' => 'User can show only associated decks to him as operator'
            ],
            [
                'name' => 'decks.create_all',
                'display_name' => 'Create decks for all boats',
                'description' => 'User can create new decks for all boats'
            ],
            [
                'name' => 'decks.create_associated',
                'display_name' => 'Create decks only for associated boats',
                'description' => 'User can create new decks only for associated boats'
            ],
            [
                'name' => 'decks.edit_all',
                'display_name' => 'Edit all decks',
                'description' => 'User can edit all decks'
            ],
            [
                'name' => 'decks.edit_associated',
                'display_name' => 'Edit associated decks',
                'description' => 'User can edit only associated decks to him as operator'
            ],
            [
                'name' => 'decks.delete_all',
                'display_name' => 'Delete all decks',
                'description' => 'User can delete all decks'
            ],
            [
                'name' => 'decks.delete_associated',
                'display_name' => 'Delete associated decks',
                'description' => 'User can delete only associated decks to him as operator'
            ],

            // CABINS
            [
                'name' => 'cabins.list_all',
                'display_name' => 'List all cabins',
                'description' => 'User can list all cabins'
            ],
            [
                'name' => 'cabins.list_associated',
                'display_name' => 'List associated cabins',
                'description' => 'User can list only associated cabins to him as operator'
            ],
            [
                'name' => 'cabins.show_all',
                'display_name' => 'Show all cabins',
                'description' => 'User can show all cabins'
            ],
            [
                'name' => 'cabins.show_associated',
                'display_name' => 'Show associated cabins',
                'description' => 'User can show only associated cabins to him as operator'
            ],
            [
                'name' => 'cabins.create_all',
                'display_name' => 'Create cabins for all boats',
                'description' => 'User can create new cabins for all boats'
            ],
            [
                'name' => 'cabins.create_associated',
                'display_name' => 'Create cabins only for associated boats',
                'description' => 'User can create new cabins only for associated boats'
            ],
            [
                'name' => 'cabins.edit_all',
                'display_name' => 'Edit all cabins',
                'description' => 'User can edit all cabins'
            ],
            [
                'name' => 'cabins.edit_associated',
                'display_name' => 'Edit associated cabins',
                'description' => 'User can edit only associated cabins to him as operator'
            ],
            [
                'name' => 'cabins.delete_all',
                'display_name' => 'Delete all cabins',
                'description' => 'User can delete all cabins'
            ],
            [
                'name' => 'cabins.delete_associated',
                'display_name' => 'Delete associated cabins',
                'description' => 'User can delete only associated cabins to him as operator'
            ],

            // CRUISES
            [
                'name' => 'cruises.list_all',
                'display_name' => 'List all cruises',
                'description' => 'User can list all cruises'
            ],
            [
                'name' => 'cruises.list_associated',
                'display_name' => 'List associated cruises',
                'description' => 'User can list only associated cruises to him as operator'
            ],
            [
                'name' => 'cruises.show_all',
                'display_name' => 'Show all cruises',
                'description' => 'User can show all cruises'
            ],
            [
                'name' => 'cruises.show_associated',
                'display_name' => 'Show associated cruises',
                'description' => 'User can show only associated cruises to him as operator'
            ],
            [
                'name' => 'cruises.create_all',
                'display_name' => 'Create cruises for all boats',
                'description' => 'User can create new cruises for all boats'
            ],
            [
                'name' => 'cruises.create_associated',
                'display_name' => 'Create cruises only for associated boats',
                'description' => 'User can create new cruises only for associated boats'
            ],
            [
                'name' => 'cruises.edit_all',
                'display_name' => 'Edit all cruises',
                'description' => 'User can edit all cruises'
            ],
            [
                'name' => 'cruises.set_all_dates',
                'display_name' => 'Edit all cruise dates',
                'description' => 'User can edit all cruises dates'
            ],
            [
                'name' => 'cruises.set_all_places',
                'display_name' => 'Edit all cruise start/end places',
                'description' => 'User can edit all cruises start/end places'
            ],
            [
                'name' => 'cruises.set_all_confirm',
                'display_name' => 'Edit all cruise confirmation',
                'description' => 'User can edit all cruises confirmation'
            ],
            [
                'name' => 'cruises.set_all_itinerary',
                'display_name' => 'Edit all cruise itinerary',
                'description' => 'User can edit all cruises itinerary'
            ],
            [
                'name' => 'cruises.set_all_price',
                'display_name' => 'Edit all cruise price',
                'description' => 'User can edit all cruises price'
            ],
            [
                'name' => 'cruises.set_all_currency',
                'display_name' => 'Edit all cruise currency',
                'description' => 'User can edit all cruises currency'
            ],
            [
                'name' => 'cruises.set_all_pax',
                'display_name' => 'Edit all cruise pax',
                'description' => 'User can edit all cruises pax'
            ],
            [
                'name' => 'cruises.set_all_surcharges',
                'display_name' => 'Edit all cruise surcharges',
                'description' => 'User can edit all cruises surcharges'
            ],
            [
                'name' => 'cruises.set_all_promotions',
                'display_name' => 'Edit all cruise promotions',
                'description' => 'User can edit all cruises promotions'
            ],
            [
                'name' => 'cruises.edit_associated',
                'display_name' => 'Edit associated cruises',
                'description' => 'User can edit associated cruises'
            ],
            [
                'name' => 'cruises.set_associated_dates',
                'display_name' => 'Edit associated cruise dates',
                'description' => 'User can edit associated cruises dates'
            ],
            [
                'name' => 'cruises.set_associated_places',
                'display_name' => 'Edit associated cruise start/end places',
                'description' => 'User can edit associated cruises start/end places'
            ],
            [
                'name' => 'cruises.set_associated_confirm',
                'display_name' => 'Edit associated cruise confirmation',
                'description' => 'User can edit associated cruises confirmation'
            ],
            [
                'name' => 'cruises.set_associated_itinerary',
                'display_name' => 'Edit associated cruise itinerary',
                'description' => 'User can edit associated cruises itinerary'
            ],
            [
                'name' => 'cruises.set_associated_price',
                'display_name' => 'Edit associated cruise price',
                'description' => 'User can edit associated cruises price'
            ],
            [
                'name' => 'cruises.set_associated_currency',
                'display_name' => 'Edit associated cruise currency',
                'description' => 'User can edit associated cruises currency'
            ],
            [
                'name' => 'cruises.set_associated_pax',
                'display_name' => 'Edit associated cruise pax',
                'description' => 'User can edit associated cruises pax'
            ],
            [
                'name' => 'cruises.set_associated_surcharges',
                'display_name' => 'Edit associated cruise surcharges',
                'description' => 'User can edit associated cruises surcharges'
            ],
            [
                'name' => 'cruises.set_associated_promotions',
                'display_name' => 'Edit associated cruise promotions',
                'description' => 'User can edit associated cruises promotions'
            ],
            [
                'name' => 'cruises.delete_all',
                'display_name' => 'Delete all cruises',
                'description' => 'User can delete all cruises'
            ],
            [
                'name' => 'cruises.delete_associated',
                'display_name' => 'Delete associated cruises',
                'description' => 'User can delete only associated cruises to him as operator'
            ],
            [
                'name' => 'cruises.assign_all',
                'display_name' => 'Assign all cruises',
                'description' => 'User can assign all cruises'
            ],
            [
                'name' => 'cruises.assign_associated',
                'display_name' => 'Assign associated cruises',
                'description' => 'User can assign only associated cruises to him as operator'
            ],
            [
                'name' => 'cruises.unassign_all',
                'display_name' => 'Unassign all cruises',
                'description' => 'User can unassign all cruises'
            ],
            [
                'name' => 'cruises.unassign_associated',
                'display_name' => 'Unassign associated cruises',
                'description' => 'User can unassign only associated cruises to him as operator'
            ],

            // ITINERARIES
            [
                'name' => 'itineraries.list_all',
                'display_name' => 'List all itineraries',
                'description' => 'User can list all itineraries'
            ],
            [
                'name' => 'itineraries.list_associated',
                'display_name' => 'List associated itineraries',
                'description' => 'User can list only associated itineraries to him as operator'
            ],
            [
                'name' => 'itineraries.show_all',
                'display_name' => 'Show all itineraries',
                'description' => 'User can show all itineraries'
            ],
            [
                'name' => 'itineraries.show_associated',
                'display_name' => 'Show associated itineraries',
                'description' => 'User can show only associated itineraries to him as operator'
            ],
            [
                'name' => 'itineraries.create',
                'display_name' => 'Create itineraries',
                'description' => 'User can create new itineraries'
            ],
            [
                'name' => 'itineraries.edit_all',
                'display_name' => 'Edit all itineraries',
                'description' => 'User can edit all itineraries'
            ],
            [
                'name' => 'itineraries.edit_associated',
                'display_name' => 'Edit associated itineraries',
                'description' => 'User can edit only associated itineraries to him as operator'
            ],
            [
                'name' => 'itineraries.delete_all',
                'display_name' => 'Delete all itineraries',
                'description' => 'User can delete all itineraries'
            ],
            [
                'name' => 'itineraries.delete_associated',
                'display_name' => 'Delete associated itineraries',
                'description' => 'User can delete only associated itineraries to him as operator'
            ],

            // DATA EXCHANGE
            [
                'name' => 'data_exchange.import_csv',
                'display_name' => 'Import from CSV',
                'description' => 'User can import data from CSV sources'
            ],
            [
                'name' => 'data_exchange.export_csv',
                'display_name' => 'Export to CSV',
                'description' => 'User can export data to CSV file'
            ],

            // BOAT OPERATORS
            [
                'name' => 'boat_operators.list_all',
                'display_name' => 'List all boat operators',
                'description' => 'User can list all boat operators'
            ],
            [
                'name' => 'boat_operators.show_all',
                'display_name' => 'Show all boat operators',
                'description' => 'User can show all boat operators'
            ],
            [
                'name' => 'boat_operators.create',
                'display_name' => 'Create boat operators',
                'description' => 'User can create new boat operators'
            ],
            [
                'name' => 'boat_operators.edit_all',
                'display_name' => 'Edit all boat operators',
                'description' => 'User can edit all boat operators'
            ],
            [
                'name' => 'boat_operators.delete_all',
                'display_name' => 'Delete all boat operators',
                'description' => 'User can delete all boat operators'
            ],

            // CRUISE OPERATORS
            [
                'name' => 'cruise_operators.list_all',
                'display_name' => 'List all cruise operators',
                'description' => 'User can list all cruise operators'
            ],
            [
                'name' => 'cruise_operators.show_all',
                'display_name' => 'Show all cruise operators',
                'description' => 'User can show all cruise operators'
            ],
            [
                'name' => 'cruise_operators.create',
                'display_name' => 'Create cruise operators',
                'description' => 'User can create new cruise operators'
            ],
            [
                'name' => 'cruise_operators.edit_all',
                'display_name' => 'Edit all cruise operators',
                'description' => 'User can edit all cruise operators'
            ],
            [
                'name' => 'cruise_operators.delete_all',
                'display_name' => 'Delete all cruise operators',
                'description' => 'User can delete all cruise operators'
            ],

            // AGENTS
            [
                'name' => 'agents.list_all',
                'display_name' => 'List all agents',
                'description' => 'User can list all agents'
            ],
            [
                'name' => 'agents.show_all',
                'display_name' => 'Show all agents',
                'description' => 'User can show all agents'
            ],
            [
                'name' => 'agents.create',
                'display_name' => 'Create agents',
                'description' => 'User can create new agents'
            ],
            [
                'name' => 'agents.edit_all',
                'display_name' => 'Edit all agents',
                'description' => 'User can edit all agents'
            ],
            [
                'name' => 'agents.delete_all',
                'display_name' => 'Delete all agents',
                'description' => 'User can delete all agents'
            ],

            // USERS
            [
                'name' => 'users.list_all',
                'display_name' => 'List all users',
                'description' => 'User can list all users'
            ],
            [
                'name' => 'users.show_all',
                'display_name' => 'Show all users',
                'description' => 'User can show all users'
            ],
            [
                'name' => 'users.create',
                'display_name' => 'Create users',
                'description' => 'User can create new users'
            ],
            [
                'name' => 'users.edit_all',
                'display_name' => 'Edit all users',
                'description' => 'User can edit all users'
            ],
            [
                'name' => 'users.delete_all',
                'display_name' => 'Delete all operators',
                'description' => 'User can delete all users'
            ],

            // ROLES
            [
                'name' => 'roles.list_all',
                'display_name' => 'List all roles',
                'description' => 'User can list all roles'
            ],
            [
                'name' => 'roles.show_all',
                'display_name' => 'Show all roles',
                'description' => 'User can show all roles'
            ],
            [
                'name' => 'roles.create',
                'display_name' => 'Create roles',
                'description' => 'User can create new roles'
            ],
            [
                'name' => 'roles.edit_all',
                'display_name' => 'Edit all roles',
                'description' => 'User can edit all roles'
            ],
            [
                'name' => 'roles.delete_all',
                'display_name' => 'Delete all roles',
                'description' => 'User can delete all roles'
            ],

            // ACTIVITIES
            [
                'name' => 'activities.list_all',
                'display_name' => 'List all activities',
                'description' => 'User can list all activities'
            ],

            // COLLABORATIONS
            [
                'name' => 'collaborations.list_all',
                'display_name' => 'List all collaborations',
                'description' => 'User can list all collaborations'
            ],
            [
                'name' => 'collaborations.list_associated',
                'display_name' => 'List all collaborations',
                'description' => 'User can list all collaborations'
            ],
            [
                'name' => 'collaborations.show_all',
                'display_name' => 'Show all collaborations',
                'description' => 'User can show all collaborations'
            ],
            [
                'name' => 'collaborations.show_associated',
                'display_name' => 'Show associated collaborations',
                'description' => 'User can show only associated collaborations'
            ],
            [
                'name' => 'collaborations.create',
                'display_name' => 'Create collaborations',
                'description' => 'User can create collaborations'
            ],
            [
                'name' => 'collaborations.approve',
                'display_name' => 'Approve collaborations',
                'description' => 'User can approve collaborations'
            ],
            [
                'name' => 'collaborations.delete_all',
                'display_name' => 'Delete all collaborations',
                'description' => 'User can delete all collaborations'
            ],
            [
                'name' => 'collaborations.delete_associated',
                'display_name' => 'Delete associated collaborations',
                'description' => 'User can delete only associated collaborations'
            ],

            // RESERVATIONS
            [
                'name' => 'reservations.list_all',
                'display_name' => 'List all reservations',
                'description' => 'User can list all reservations'
            ],
            [
                'name' => 'reservations.list_associated',
                'display_name' => 'List all reservations',
                'description' => 'User can list all reservations'
            ],
            [
                'name' => 'reservations.show_all',
                'display_name' => 'Show all reservations',
                'description' => 'User can show all reservations'
            ],
            [
                'name' => 'reservations.show_associated',
                'display_name' => 'Show associated reservations',
                'description' => 'User can show only associated reservations'
            ],
            [
                'name' => 'reservations.create_all',
                'display_name' => 'Create all reservations',
                'description' => 'User can create reservations for all cruises'
            ],
            [
                'name' => 'reservations.create_associated',
                'display_name' => 'Create associated reservations',
                'description' => 'User can create reservations only for associated boats'
            ],
            [
                'name' => 'reservations.edit_all',
                'display_name' => 'Edit all reservations',
                'description' => 'User can edit reservations for all cruises'
            ],
            [
                'name' => 'reservations.edit_associated',
                'display_name' => 'Edit associated reservations',
                'description' => 'User can edit reservations only for associated boats'
            ],
            [
                'name' => 'reservations.delete_all',
                'display_name' => 'Delete all reservations',
                'description' => 'User can delete all reservations'
            ],
            [
                'name' => 'reservations.delete_associated',
                'display_name' => 'Delete associated reservations',
                'description' => 'User can delete only associated reservations'
            ],
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('permissions')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        foreach ($permissions as $permission) {
            Permission::create($permission);
        }

    }
}
