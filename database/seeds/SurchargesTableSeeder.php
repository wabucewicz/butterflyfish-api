<?php

use App\Models\Surcharge;
use Illuminate\Database\Seeder;

class SurchargesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $surcharges = [
            ['name' => 'Fuel surcharge I'],
            ['name' => 'Fuel surcharge II'],
            ['name' => 'Park fee'],
            ['name' => 'Port fee'],
            ['name' => 'Service and handing fee'],
            ['name' => 'SGL single surcharges'],
            ['name' => 'Flight'],
            ['name' => 'Ingala Card']
        ];

        foreach ($surcharges as $surcharge) {
            Surcharge::create($surcharge);
        }
    }
}
