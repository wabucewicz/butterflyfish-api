<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Super administrator',
            'email' => 'admin@bflyfish.com',
            'password' => bcrypt('ButterFlyFish231'),
            'active' => true
        ]);
        $superAdminRole = Role::where('name', 'super_admin')->first();
        $user->role()->associate($superAdminRole);
        $user->save();
    }
}
