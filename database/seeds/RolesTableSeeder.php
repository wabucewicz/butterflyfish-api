<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'data' => [
                    'name' => 'super_admin',
                    'display_name' => 'Super Administrator',
                    'description' => 'Master of the system',
                    'editable' => false
                ],
                'permissions' => Permission::all()->pluck('name')->toArray()
            ],
            [
                'data' => [
                    'name' => 'boat_operator',
                    'display_name' => 'Boat Operator',
                    'description' => 'Boat operator',
                    'editable' => false
                ],
                'permissions' => [
                    'boats.list_associated',
                    'boats.show_associated',
                    'boats.edit_associated',
                    'boats.delete_associated',
                    'decks.list_associated',
                    'decks.show_associated',
                    'decks.create_associated',
                    'decks.edit_associated',
                    'decks.delete_associated',
                    'cabins.list_associated',
                    'cabins.show_associated',
                    'cabins.create_associated',
                    'cabins.edit_associated',
                    'cabins.delete_associated',
                    'cruises.list_associated',
                    'cruises.show_associated',
                    'cruises.create_associated',
                    'cruises.edit_associated',
                    'cruises.set_associated_dates',
                    'cruises.set_associated_places',
                    'cruises.set_associated_confirm',
                    'cruises.set_associated_itinerary',
                    'cruises.set_associated_price',
                    'cruises.set_associated_currency',
                    'cruises.set_associated_pax',
                    'cruises.set_associated_surcharges',
                    'cruises.set_associated_promotions',
                    'cruises.delete_associated',
                    'cruises.assign_associated',
                    'cruises.unassign_associated',
                    'itineraries.list_associated',
                    'itineraries.show_associated',
                    'itineraries.create',
                    'itineraries.edit_associated',
                    'itineraries.delete_associated',
                    'collaborations.list_associated',
                    'collaborations.show_associated',
                    'collaborations.create',
                    'collaborations.approve',
                    'collaborations.delete_associated',
                    'reservations.list_associated',
                    'reservations.show_associated',
                    'reservations.create_associated',
                    'reservations.edit_associated',
                    'reservations.delete_associated'
                ]
            ],
            [
                'data' => [
                    'name' => 'cruise_operator',
                    'display_name' => 'Cruise Operator',
                    'description' => 'Cruise operator',
                    'editable' => false
                ],
                'permissions' => [
                    'boats.list_associated',
                    'boats.show_associated',
                    'decks.list_associated',
                    'decks.show_associated',
                    'cabins.list_associated',
                    'cruises.list_associated',
                    'cruises.show_associated',
                    'cruises.edit_associated',
                    'cruises.set_associated_confirm',
                    'cruises.set_associated_itinerary',
                    'cruises.set_associated_price',
                    'cruises.set_associated_currency',
                    'cruises.set_associated_pax',
                    'cruises.set_associated_promotions',
                    'itineraries.list_associated',
                    'itineraries.show_associated',
                    'itineraries.create',
                    'itineraries.edit_associated',
                    'itineraries.delete_associated',
                    'collaborations.list_associated',
                    'collaborations.show_associated',
                    'collaborations.create',
                    'collaborations.approve',
                    'collaborations.delete_associated',
                    'reservations.list_associated',
                    'reservations.show_associated',
                    'reservations.create_associated',
                    'reservations.edit_associated',
                    'reservations.delete_associated'
                ]
            ],
            [
                'data' => [
                    'name' => 'agent',
                    'display_name' => 'Agent',
                    'description' => 'Agent',
                    'editable' => false
                ],
                'permissions' => [
                    'boats.list_associated',
                    'boats.show_associated',
                    'cabins.list_associated',
                    'cruises.list_associated',
                    'cruises.show_associated',
                    'collaborations.list_associated',
                    'collaborations.show_associated',
                    'collaborations.create',
                    'collaborations.approve',
                    'collaborations.delete_associated',
                    'reservations.list_associated',
                    'reservations.show_associated',
                    'reservations.create_associated',
                    'reservations.edit_associated',
                    'reservations.delete_associated'
                ]
            ]
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('roles')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        foreach ($roles as $roleRow) {
            $role = Role::create($roleRow['data']);
            $role->permissions()->sync($this->getPermissionsIds($roleRow['permissions']));
        }

    }

    private function getPermissionsIds($names)
    {
        $ids = [];
        foreach ($names as $name) {
            $ids[] = Permission::where('name', $name)->first()->id;
        }
        return $ids;
    }
}
