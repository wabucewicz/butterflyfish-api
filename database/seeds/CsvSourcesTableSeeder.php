<?php

use Illuminate\Database\Seeder;

class CsvSourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("
            INSERT INTO `csv_sources` (`id`, `name`, `url`, `start_row`, `separator`, `column_boat_id_index`, `column_start_date_index`, `column_end_date_index`, `column_start_place_index`, `column_end_place_index`, `column_itinerary_index`, `column_free_pax_index`, `column_price_per_pax_index`, `column_currency_index`, `column_confirmed_index`, `column_confirmed_value`, `default_currency`, `id_map_arr`, `created_at`, `updated_at`) VALUES
            (1, 'Amira', 'http://www.amira-indonesien.de/termine-preise.csv', 0, '|', 0, 2, 3, 4, 5, 6, 7, NULL, NULL, NULL, '', NULL, 'a:1:{i:1373;i:8;}', NULL, NULL),
            (2, 'Carpediem', 'https://www.carpediemmaldives.com/pax_cd/export/termine.csv', 0, ',', 0, 4, 5, 6, 7, 9, 10, NULL, NULL, NULL, '', NULL, 'a:3:{i:1280;i:57;i:1426;i:58;i:1324;i:59;}', NULL, NULL),
            (3, 'Independence', 'http://my-independence1.de/termin-data-export', 1, ',', 0, 1, 2, 3, 4, 5, 6, NULL, NULL, NULL, '', NULL, 'a:1:{s:14:\"Independence I\";i:155;}', NULL, NULL),
            (4, 'LiveaboardFleet', 'http://www.liveaboardfleet.com/export/resellers.csv', 1, ',', 13, 2, 3, 11, 12, 8, 5, NULL, NULL, NULL, '', NULL, 'a:16:{i:27;i:25;i:1;i:27;i:16;i:386;i:28;i:28;i:9;i:176;i:30;i:115;i:32;i:156;i:34;i:292;i:40;i:361;i:29;i:297;i:11;i:260;i:38;i:286;i:7;i:121;i:3;i:62;i:31;i:253;i:33;i:375;}', NULL, NULL),
            (5, 'Naia', 'http://pax.naia.com.fj/public_website/l24_export.php', 0, '|', 0, 2, 3, 4, 5, 6, 7, NULL, NULL, NULL, '', NULL, 'a:1:{i:28;i:224;}', NULL, NULL),
            (6, 'Spoilsport', 'http://www.data.mikeball.com/schedules_scubadates.csv', 1, ',', 0, 2, 3, 5, 6, 7, 8, NULL, NULL, 4, 'CONFIRMED', NULL, 'a:1:{i:16;i:360;}', NULL, NULL),
            (7, 'StEgypt', 'http://www.emperordivers.com/schedules/st-egypt.php', 0, ',', 0, 1, 2, 3, 4, 5, 6, 7, NULL, NULL, '', 'EUR', 'a:5:{s:16:\"MV Emperor Elite\";i:100;s:25:\"MV Emperor Elite Hurghada\";i:100;s:19:\"MV Emperor Superior\";i:104;s:25:\"MV Emperor Asmaa Hurghada\";i:98;s:29:\"MV Emperor Asmaa Marsa Ghalib\";i:98;}', NULL, NULL),
            (8, 'StMaldives', 'http://www.emperordivers.com/schedules/st-maldives.php', 0, ',', 0, 1, 2, 3, 4, 5, 6, 7, NULL, NULL, '', 'EUR', 'a:6:{s:16:\"MV Emperor Atoll\";i:99;s:19:\"MV Emperor Serenity\";i:103;s:14:\"MV Emperor Leo\";i:101;s:16:\"MV Emperor Virgo\";i:105;s:18:\"MV Emperor Voyager\";i:106;s:16:\"MV Emperor Orion\";i:102;}', NULL, NULL),
            (9, 'UnderSeaHunter', 'http://www.underseahunter.com/export/termine.php', 0, '|', 0, 2, 3, 4, 5, 6, 7, NULL, NULL, NULL, '', NULL, 'a:2:{i:1228;i:14;i:145;i:316;}', NULL, NULL),
            (10, 'Whitemanta', 'http://whitemanta.com/freespaces.php', 4, ';', 0, 1, 2, 3, 4, 5, 6, NULL, NULL, NULL, '', NULL, 'a:3:{s:10:\"Blue Manta\";i:40;s:14:\"MV White Manta\";i:400;}', NULL, NULL);
        ");
    }
}
