<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHashCodeFieldsToOperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('boat_operators', function (Blueprint $table) {
            $table->string('hash_code')->after('id');
        });
        Schema::table('cruise_operators', function (Blueprint $table) {
            $table->string('hash_code')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('boat_operators', function (Blueprint $table) {
            $table->dropColumn('hash_code');
        });
        Schema::table('cruise_operators', function (Blueprint $table) {
            $table->dropColumn('hash_code');
        });
    }
}
