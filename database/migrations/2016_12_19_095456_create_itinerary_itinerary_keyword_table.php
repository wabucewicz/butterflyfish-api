<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItineraryItineraryKeywordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itinerary_itinerary_keyword', function (Blueprint $table) {

            $table->integer('itinerary_id')->unsigned()->nullable();
            $table->foreign('itinerary_id')->references('id')
                ->on('itineraries')->onDelete('set null');

            $table->integer('itinerary_keyword_id')->unsigned()->nullable();
            $table->foreign('itinerary_keyword_id')->references('id')
                ->on('itinerary_keywords')->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itinerary_itinerary_keyword');
    }
}
