<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('boat_website_url');
            $table->enum('data_source', ['', 'la24', 'csv', 'web', 'xls', 'email', 'dropbox']);
            $table->string('schedules_website_url');
            $table->string('csv_source_url');
            $table->text('cancellation_policy');
            $table->text('prepayment_policy');
            $table->text('notes');

            // Boat features
            $table->smallInteger('year_built')->nullable();
            $table->smallInteger('year_renovated')->nullable();
            $table->integer('beam')->nullable();
            $table->enum('beam_unit', ['m', 'ft']);
            $table->enum('build_from', ['steel', 'wood'])->nullable();
            $table->smallInteger('number_of_cabins');
            $table->smallInteger('number_of_crew_members');
            $table->smallInteger('number_of_dive_guides');
            $table->smallInteger('max_quests');
            $table->smallInteger('quantities_pax');

            $table->boolean('laundry_service');
            $table->boolean('leisure_deck');
            $table->boolean('spa');
            $table->boolean('massage');
            $table->boolean('daily_housekeeping');
            $table->boolean('audio_and_video_entertainment');
            $table->boolean('wifi');
            $table->boolean('free_internet');
            $table->boolean('air_conditioned_saloon');
            $table->boolean('aircon_in_cabins');
            $table->boolean('sun_deck');
            $table->boolean('snorkeler_friendly');
            $table->boolean('outdoor_dining');
            $table->boolean('hot_tub_jacuzzi');
            $table->boolean('onboard_kayaks');
            $table->boolean('surfing_board');
            $table->boolean('library');
            $table->boolean('indoor_saloon');
            $table->boolean('alcohol_mini_bar');
            $table->boolean('alcohol_bar');
            $table->boolean('freshwater_maker');

            // Safety facilities
            $table->boolean('gps_emergency_rafts');
            $table->boolean('life_vests');
            $table->boolean('fire_alarm_and_fire_extinguishers');
            $table->boolean('oxygen');
            $table->boolean('first_aid_kits');
            $table->boolean('satellite_and_mobile_phones');
            $table->boolean('crew_trained_in_first_aid');
            $table->boolean('emergency_flares');

            // Divers' facilities
            $table->boolean('nitrox');
            $table->boolean('twelve_l_tanks');
            $table->boolean('fifteen_l_tanks');
            $table->boolean('din_tanks');
            $table->boolean('int_tanks');
            $table->boolean('rebreather');
            $table->boolean('dive_equipment_rental');
            $table->boolean('hot_drinks_after_diving');
            $table->boolean('hot_towels_for_divers');
            $table->boolean('warm_water_showers_for_divers');
            $table->boolean('rinse_for_divers');
            $table->boolean('photography_station');
            $table->boolean('separate_rinse_for_uw_camera');
            $table->boolean('camera_room_and_charging_point');
            $table->boolean('diving_deck_at_water_level');
            $table->boolean('shaded_diving_deck');
            $table->boolean('safety_sausage');
            $table->boolean('nautilus_lifeline');
            $table->boolean('macro_sticks');

            $table->string('hash_code');

            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('boats');
    }
}
