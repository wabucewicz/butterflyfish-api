<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCruisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cruises', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('boat_id')->unsigned();
            $table->foreign('boat_id')->references('id')->on('boats')->onDelete('cascade');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('start_place')->nullable();
            $table->string('end_place')->nullable();
            $table->boolean('occupancy_per_cruise')->default(true);
            $table->integer('free_pax')->nullable();
            $table->integer('price_per_pax')->nullable();
            $table->string('currency')->nullable();
            $table->integer('itinerary_id')->unsigned()->nullable();
            $table->foreign('itinerary_id')->references('id')->on('itineraries')->onDelete('set null');
            $table->boolean('confirmed')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cruises');
    }
}
