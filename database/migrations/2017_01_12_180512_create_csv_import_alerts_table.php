<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCsvImportAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csv_import_alerts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('boat_id')->unsigned();
            $table->foreign('boat_id')->references('id')->on('boats')->onDelete('cascade');

            // Imported cruise data
            $table->date('start_date');
            $table->date('end_date');
            $table->string('start_place');
            $table->string('end_place');
            $table->boolean('occupancy_per_cruise')->default(true);
            $table->integer('free_pax')->nullable();
            $table->integer('price_per_pax')->nullable();
            $table->string('currency')->nullable();
            $table->string('itinerary');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csv_import_alerts');
    }
}
