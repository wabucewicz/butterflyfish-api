<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPromotionPriceToCruiseCabinOccupanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cruise_cabin_occupancies', function (Blueprint $table) {
            $table->integer('promotion_price')->nullable()->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cruise_cabin_occupancies', function (Blueprint $table) {
            $table->dropColumn('promotion_price');
        });
    }
}
