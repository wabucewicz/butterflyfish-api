<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCruiseOperatorIdToCruisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cruises', function (Blueprint $table) {
            $table->integer('cruise_operator_id')->unsigned()->nullable()->after('id');
            $table->foreign('cruise_operator_id')->references('id')->on('cruise_operators');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cruises', function (Blueprint $table) {
            $table->dropForeign('cruises_cruise_operator_id_foreign');
            $table->dropColumn('cruise_operator_id');
        });
    }
}
