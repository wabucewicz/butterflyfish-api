<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItinerariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itineraries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('operator_id')->unsigned()->nullable();
            $table->foreign('operator_id')->references('id')->on('operators')
                ->onDelete('cascade');
            $table->string('name');
            $table->enum('completed_training', ['owd', 'aowd', 'rescue', 'dm'])->nullable();
            $table->enum('number_of_dives', ['20', '30', '50', '100'])->nullable();
            $table->boolean('buoy')->default(false);
            $table->boolean('equipment')->default(false);
            $table->boolean('computer')->default(false);
            $table->string('picture_map');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('itineraries');
    }
}
