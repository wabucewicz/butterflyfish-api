<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCruiseOperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cruise_operators', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address')->nullable();
            $table->string('responsible_person')->nullable();
            $table->string('email');
            $table->string('email_2')->nullable();
            $table->string('phone');
            $table->string('phone_2')->nullable();
            $table->string('website');
            $table->string('beneficiary_eur')->nullable();
            $table->string('swift_eur')->nullable();
            $table->string('iban_account_eur')->nullable();
            $table->string('beneficiary_usd')->nullable();
            $table->string('swift_usd')->nullable();
            $table->string('iban_account_usd')->nullable();
            $table->string('local_currency')->nullable();
            $table->string('beneficiary_local')->nullable();
            $table->string('swift_local')->nullable();
            $table->string('iban_account_local')->nullable();
            $table->text('cancellation_policy')->nullable();
            $table->text('groups_policy')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cruise_operators');
    }
}
