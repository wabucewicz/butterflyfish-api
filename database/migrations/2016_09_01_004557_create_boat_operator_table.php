<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoatOperatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boat_operator', function (Blueprint $table) {
            $table->integer('boat_id')->unsigned();
            $table->integer('operator_id')->unsigned();

            $table->foreign('boat_id')->references('id')->on('boats')
                ->onDelete('cascade');
            $table->foreign('operator_id')->references('id')->on('operators')
                ->onDelete('cascade');

            $table->primary(['boat_id', 'operator_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('boat_operator');
    }
}
