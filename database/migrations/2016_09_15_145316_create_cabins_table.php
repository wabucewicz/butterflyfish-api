<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCabinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cabins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('boat_id')->unsigned();
            $table->foreign('boat_id')->references('id')->on('boats')->onDelete('cascade');
            $table->integer('deck_id')->unsigned()->nullable();
            $table->foreign('deck_id')->references('id')->on('decks')->onDelete('set null');
            $table->string('name');
            $table->integer('sng_beds');
            $table->integer('dbl_beds');
            $table->integer('max_pax');
            $table->integer('min_pax');
            $table->boolean('shared');
            $table->integer('max_shared');
            $table->text('description');

            $table->integer('size');
            $table->enum('size_unit', ['m2', 'ft2']);
            $table->boolean('ensuite');
            $table->boolean('aircon');
            $table->boolean('tv');
            $table->boolean('computer');
            $table->boolean('towels');
            $table->boolean('soap');
            $table->boolean('safe');
            $table->boolean('wifi');
            $table->boolean('wifi_free');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cabins');
    }
}
