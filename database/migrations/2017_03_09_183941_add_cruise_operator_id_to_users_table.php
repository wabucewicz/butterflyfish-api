<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCruiseOperatorIdToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('cruise_operator_id')->unsigned()->nullable()->after('boat_operator_id');
            $table->foreign('cruise_operator_id')->references('id')->on('cruise_operators');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_cruise_operator_id_foreign');
            $table->dropColumn('cruise_operator_id');
        });
    }
}
