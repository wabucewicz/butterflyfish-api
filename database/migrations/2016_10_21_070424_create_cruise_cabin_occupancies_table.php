<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCruiseCabinOccupanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cruise_cabin_occupancies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cruise_id')->unsigned();
            $table->foreign('cruise_id')->references('id')->on('cruises')->onDelete('cascade');
            $table->integer('cabin_id')->unsigned();
            $table->foreign('cabin_id')->references('id')->on('cabins')->onDelete('cascade');
            $table->integer('occupancy');
            $table->integer('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cruise_cabin_occupancies');
    }
}
