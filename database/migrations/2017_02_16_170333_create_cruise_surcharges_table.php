<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCruiseSurchargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cruise_surcharges', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cruise_id')->unsigned();
            $table->foreign('cruise_id')->references('id')->on('cruises')->onDelete('cascade');
            $table->integer('surcharge_id')->unsigned();
            $table->foreign('surcharge_id')->references('id')->on('surcharges')->onDelete('cascade');
            $table->integer('amount');
            $table->string('currency');
            $table->enum('payment', ['on_book', 'on_board']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cruise_surcharges');
    }
}
