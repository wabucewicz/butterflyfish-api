<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserIdForeignInOperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_boat_operator_id_foreign');
            $table->foreign('boat_operator_id')->references('id')->on('boat_operators')
                ->onDelete('set null');
            $table->dropForeign('users_cruise_operator_id_foreign');
            $table->foreign('cruise_operator_id')->references('id')->on('cruise_operators')
                ->onDelete('set null');
            $table->foreign('agent_id')->references('id')->on('agents')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_boat_operator_id');
            $table->foreign('boat_operator_id')->references('id')->on('boat_operators');
            $table->dropForeign('users_cruise_operator_id');
            $table->foreign('cruise_operator_id')->references('id')->on('cruise_operators');
            $table->dropForeign('users_agent_id');
            $table->foreign('agent_id')->references('id')->on('agents');
        });
    }
}
