<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameOperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('operators', 'boat_operators');

        Schema::rename('boat_operator', 'boat_boat_operator');

        Schema::table('boat_boat_operator', function (Blueprint $table) {
            $table->dropForeign('boat_operator_operator_id_foreign');
            $table->renameColumn('operator_id', 'boat_operator_id');
            $table->foreign('boat_operator_id')->references('id')->on('boat_operators')
                ->onDelete('cascade');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_operator_id_foreign');
            $table->renameColumn('operator_id', 'boat_operator_id');
            $table->foreign('boat_operator_id')->references('id')->on('boat_operators');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('boat_operators', 'operators');

        Schema::table('boat_boat_operator', function (Blueprint $table) {
            $table->dropForeign('boat_boat_operator_boat_operator_id_foreign');
            $table->renameColumn('boat_operator_id', 'operator_id');
            $table->foreign('operator_id')->references('id')->on('operators')
                ->onDelete('cascade');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_boat_operator_id_foreign');
            $table->renameColumn('boat_operator_id', 'operator_id');
            $table->foreign('operator_id')->references('id')->on('operators');
        });

        Schema::rename('boat_boat_operator', 'boat_operator');
    }
}
