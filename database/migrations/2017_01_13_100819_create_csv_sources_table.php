<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCsvSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csv_sources', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('url');
            $table->integer('start_row');
            $table->char('separator', 1);
            $table->integer('column_boat_id_index');
            $table->integer('column_start_date_index');
            $table->integer('column_end_date_index');
            $table->integer('column_start_place_index');
            $table->integer('column_end_place_index');
            $table->integer('column_itinerary_index')->nullable();
            $table->integer('column_free_pax_index')->nullable();
            $table->integer('column_price_per_pax_index')->nullable();
            $table->integer('column_currency_index')->nullable();
            $table->integer('column_confirmed_index')->nullable();
            $table->string('column_confirmed_value');
            $table->string('default_currency')->nullable();
            $table->text('id_map_arr');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csv_sources');
    }
}
