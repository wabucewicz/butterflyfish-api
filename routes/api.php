<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Admin butter API
 */
Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function() {

    // LOGIN AND PASSWORD RESET
    Route::post('login', 'AuthController@authenticate');
    Route::post('password-forgot-send', 'AuthController@sendPasswordResetLink');
    Route::post('password-forgot-check', 'AuthController@checkPasswordResetToken');
    Route::post('password-forgot-reset', 'AuthController@passwordReset');

    Route::group(['middleware' => 'jwt.auth'], function (){

        // RETRIEVE AUTHENTICATED USER
        Route::get('user', 'AuthController@getAuthenticatedUser');

        // BOATS
        Route::resource('boats', 'BoatController');

        // BOATS/DECKS
        Route::get('boats/{boatHashId}/decks', 'BoatController@indexDeck');
        Route::get('boats/{boatHashId}/decks/{deckHashId}', 'BoatController@showDeck');
        Route::post('boats/{boatHashId}/decks', 'BoatController@storeDeck');
        Route::put('boats/{boatHashId}/decks/{deckHashId}', 'BoatController@updateDeck');
        Route::delete('boats/{boatHashId}/decks/{deckHashId}', 'BoatController@destroyDeck');

        // BOATS/CABINS
        Route::get('boats/{boatHashId}/cabins', 'BoatController@indexCabin');
        Route::get('boats/{boatHashId}/cabins/{cabinHashId}', 'BoatController@showCabin');
        Route::post('boats/{boatHashId}/cabins', 'BoatController@storeCabin');
        Route::put('boats/{boatHashId}/cabins/{cabinHashId}', 'BoatController@updateCabin');
        Route::delete('boats/{boatHashId}/cabins/{cabinHashId}', 'BoatController@destroyCabin');

        // BOATS/CRUISES
        Route::get('boats/{boatHashId}/cruises', 'BoatController@indexCruise');
        Route::get('boats/{boatHashId}/cruises/{cruiseHashId}', 'BoatController@showCruise');
        Route::post('boats/{boatHashId}/cruises', 'BoatController@storeCruise');
        Route::put('boats/{boatHashId}/cruises/{cruiseHashId}', 'BoatController@updateCruise');
        Route::delete('boats/{boatHashId}/cruises/{cruiseHashId}', 'BoatController@destroyCruise');

        Route::post('boats/{boatHashId}/cruises/{cruiseHashId}/assign', 'BoatController@assignToCruiseOperator');
        Route::post('boats/{boatHashId}/cruises/{cruiseHashId}/unassign', 'BoatController@unassignFromCruiseOperator');

        // BOATS/ADD CRUISES PROMOTION
        Route::post('boats/{boatHashId}/promotion', 'BoatController@addPromotionToBoatCruises');

        // DECKS
        Route::resource('decks', 'DeckController');

        // CABINS
        Route::resource('cabins', 'CabinController');

        // ITINERARIES
        Route::get('itineraries/embarkations', 'ItineraryController@embarkations');
        Route::get('itineraries/disembarkations', 'ItineraryController@disembarkations');
        Route::resource('itineraries', 'ItineraryController');

        // ITINERARY KEYWORDS
        Route::resource('itinerary-keywords', 'ItineraryKeywordController');

        // BOAT OPERATORS
        Route::resource('boat-operators', 'BoatOperatorController');

        // CRUISE OPERATORS
        Route::resource('cruise-operators', 'CruiseOperatorController');

        // AGENTS
        Route::resource('agents', 'AgentController');

        // ROLES
        Route::resource('roles', 'RoleController');

        // PERMISSIONS
        Route::resource('permissions', 'PermissionController');

        // USERS
        Route::resource('users', 'UserController');

        // CURRENCIES
        Route::resource('currencies', 'CurrencyController');

        // CRUISES
        Route::resource('cruises', 'CruiseController');

        // SURCHARGES
        Route::resource('surcharges', 'SurchargeController');

        // CSV SOURCES
        Route::resource('csv-sources', 'CsvSourceController');
        Route::get('csv-sources/{hashId}/import', 'CsvSourceController@import');

        // CSV IMPORT ALERTS
        Route::resource('csv-import-alerts', 'CsvImportAlertController');

        // ACTIVITY LOG
        Route::get('activities', 'ActivityController@index');

        // COLLABORATIONS
        Route::put('collaborations/{hashId}', 'CollaborationController@approve');
        Route::resource('collaborations', 'CollaborationController', ['except' => ['update']]);

        // CUSTOMERS
        Route::resource('customers', 'CustomerController');

        // RESERVATIONS
        Route::resource('reservations', 'ReservationController');

    });

});

// OFFERS SEARCH
Route::post('/offers', 'OfferController@getOffers');
Route::post('/offers/{hashCode}', 'OfferController@getOffersByHashCode');