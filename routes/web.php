<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// SCHEDULE WIDGET
Route::get('/schedule-widget/{boatHashCode}', 'WidgetController@show');

// CSV EXPORT
Route::get('csv/export/all', 'Admin\DataExchangeController@csvExport');
Route::get('export/{boatHashCode}', 'Admin\DataExchangeController@csvExportByBoat');

Route::get('admin/photos/{model}/{hashId}', 'Admin\PhotoController@index');
Route::post('admin/photos/{model}/{hashId}', 'Admin\PhotoController@store');
Route::delete('admin/photos/{hashId}', 'Admin\PhotoController@destroy');

if (!App::environment('production')) {
    Route::get('id/decode/{hashId}', function ($hashId) {
        return App\Utils\ID::decode($hashId);
    });

    Route::get('id/encode/{id}', function ($id) {
        return App\Utils\ID::encode($id);
    });
}
