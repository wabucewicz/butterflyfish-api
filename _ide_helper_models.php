<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Activity
 *
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 * @property-read mixed $hash_id
 * @property-read mixed $hash_user_id
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Activity whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Activity whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Activity whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Activity whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Activity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Activity whereUserId($value)
 */
	class Activity extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Agent
 *
 * @property int $id
 * @property string $hash_code
 * @property string $name
 * @property string $address
 * @property string $responsible_person
 * @property string $email
 * @property string $email_2
 * @property string $phone
 * @property string $phone_2
 * @property string $website
 * @property string $notes
 * @property int $created_at
 * @property int $updated_at
 * @property-read mixed $hash_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agent whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agent whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agent whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agent whereEmail2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agent whereHashCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agent whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agent whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agent whereNotes($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agent wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agent wherePhone2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agent whereResponsiblePerson($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agent whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agent whereWebsite($value)
 */
	class Agent extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Boat
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $boat_website_url
 * @property string $data_source
 * @property string $schedules_website_url
 * @property string $csv_source_url
 * @property string $cancellation_policy
 * @property string $prepayment_policy
 * @property string $notes
 * @property int $year_built
 * @property int $year_renovated
 * @property int $beam
 * @property string $beam_unit
 * @property string $build_from
 * @property int $number_of_cabins
 * @property int $number_of_crew_members
 * @property int $number_of_dive_guides
 * @property int $max_quests
 * @property int $quantities_pax
 * @property bool $laundry_service
 * @property bool $leisure_deck
 * @property bool $spa
 * @property bool $massage
 * @property bool $daily_housekeeping
 * @property bool $audio_and_video_entertainment
 * @property bool $wifi
 * @property bool $free_internet
 * @property bool $air_conditioned_saloon
 * @property bool $aircon_in_cabins
 * @property bool $sun_deck
 * @property bool $snorkeler_friendly
 * @property bool $outdoor_dining
 * @property bool $hot_tub_jacuzzi
 * @property bool $onboard_kayaks
 * @property bool $surfing_board
 * @property bool $library
 * @property bool $indoor_saloon
 * @property bool $alcohol_mini_bar
 * @property bool $alcohol_bar
 * @property bool $freshwater_maker
 * @property bool $gps_emergency_rafts
 * @property bool $life_vests
 * @property bool $fire_alarm_and_fire_extinguishers
 * @property bool $oxygen
 * @property bool $first_aid_kits
 * @property bool $satellite_and_mobile_phones
 * @property bool $crew_trained_in_first_aid
 * @property bool $emergency_flares
 * @property bool $nitrox
 * @property bool $twelve_l_tanks
 * @property bool $fifteen_l_tanks
 * @property bool $din_tanks
 * @property bool $int_tanks
 * @property bool $rebreather
 * @property bool $dive_equipment_rental
 * @property bool $hot_drinks_after_diving
 * @property bool $hot_towels_for_divers
 * @property bool $warm_water_showers_for_divers
 * @property bool $rinse_for_divers
 * @property bool $photography_station
 * @property bool $separate_rinse_for_uw_camera
 * @property bool $camera_room_and_charging_point
 * @property bool $diving_deck_at_water_level
 * @property bool $shaded_diving_deck
 * @property bool $safety_sausage
 * @property bool $nautilus_lifeline
 * @property bool $macro_sticks
 * @property string $hash_code
 * @property int $created_at
 * @property int $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BoatOperator[] $boatOperators
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cabin[] $cabins
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cruise[] $cruises
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CsvImportAlert[] $csvImportAlerts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Deck[] $decks
 * @property-read mixed $hash_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Photo[] $photos
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereAirConditionedSaloon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereAirconInCabins($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereAlcoholBar($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereAlcoholMiniBar($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereAudioAndVideoEntertainment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereBeam($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereBeamUnit($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereBoatWebsiteUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereBuildFrom($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereCameraRoomAndChargingPoint($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereCancellationPolicy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereCrewTrainedInFirstAid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereCsvSourceUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereDailyHousekeeping($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereDataSource($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereDinTanks($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereDiveEquipmentRental($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereDivingDeckAtWaterLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereEmergencyFlares($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereFifteenLTanks($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereFireAlarmAndFireExtinguishers($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereFirstAidKits($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereFreeInternet($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereFreshwaterMaker($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereGpsEmergencyRafts($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereHashCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereHotDrinksAfterDiving($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereHotTowelsForDivers($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereHotTubJacuzzi($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereIndoorSaloon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereIntTanks($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereLaundryService($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereLeisureDeck($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereLibrary($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereLifeVests($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereMacroSticks($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereMassage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereMaxQuests($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereNautilusLifeline($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereNitrox($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereNotes($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereNumberOfCabins($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereNumberOfCrewMembers($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereNumberOfDiveGuides($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereOnboardKayaks($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereOutdoorDining($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereOxygen($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat wherePhotographyStation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat wherePrepaymentPolicy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereQuantitiesPax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereRebreather($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereRinseForDivers($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereSafetySausage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereSatelliteAndMobilePhones($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereSchedulesWebsiteUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereSeparateRinseForUwCamera($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereShadedDivingDeck($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereSnorkelerFriendly($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereSpa($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereSunDeck($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereSurfingBoard($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereTwelveLTanks($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereWarmWaterShowersForDivers($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereWifi($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereYearBuilt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Boat whereYearRenovated($value)
 */
	class Boat extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\BoatOperator
 *
 * @property int $id
 * @property string $hash_code
 * @property string $name
 * @property string $address
 * @property string $responsible_person
 * @property string $email
 * @property string $email_2
 * @property string $phone
 * @property string $phone_2
 * @property string $website
 * @property string $beneficiary_eur
 * @property string $swift_eur
 * @property string $iban_account_eur
 * @property string $beneficiary_usd
 * @property string $swift_usd
 * @property string $iban_account_usd
 * @property string $local_currency
 * @property string $beneficiary_local
 * @property string $swift_local
 * @property string $iban_account_local
 * @property string $cancellation_policy
 * @property string $groups_policy
 * @property string $notes
 * @property int $created_at
 * @property int $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Boat[] $boats
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Collaboration[] $collaborations
 * @property-read mixed $hash_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Itinerary[] $itineraries
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereBeneficiaryEur($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereBeneficiaryLocal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereBeneficiaryUsd($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereCancellationPolicy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereEmail2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereGroupsPolicy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereHashCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereIbanAccountEur($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereIbanAccountLocal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereIbanAccountUsd($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereLocalCurrency($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereNotes($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator wherePhone2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereResponsiblePerson($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereSwiftEur($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereSwiftLocal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereSwiftUsd($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BoatOperator whereWebsite($value)
 */
	class BoatOperator extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Cabin
 *
 * @property int $id
 * @property int $boat_id
 * @property int $deck_id
 * @property string $name
 * @property int $sng_beds
 * @property int $dbl_beds
 * @property int $max_pax
 * @property int $min_pax
 * @property bool $shared
 * @property int $max_shared
 * @property string $description
 * @property int $size
 * @property string $size_unit
 * @property bool $ensuite
 * @property bool $aircon
 * @property bool $tv
 * @property bool $computer
 * @property bool $towels
 * @property bool $soap
 * @property bool $safe
 * @property bool $wifi
 * @property bool $wifi_free
 * @property int $created_at
 * @property int $updated_at
 * @property-read \App\Models\Boat $boat
 * @property-read \App\Models\Deck $deck
 * @property-read mixed $hash_boat_id
 * @property-read mixed $hash_deck_id
 * @property-read mixed $hash_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Photo[] $photos
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereAircon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereBoatId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereComputer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereDblBeds($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereDeckId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereEnsuite($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereMaxPax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereMaxShared($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereMinPax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereSafe($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereShared($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereSize($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereSizeUnit($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereSngBeds($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereSoap($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereTowels($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereTv($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereWifi($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cabin whereWifiFree($value)
 */
	class Cabin extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Collaboration
 *
 * @property int $id
 * @property int $operator_id
 * @property string $operator_type
 * @property int $agent_id
 * @property bool $operator_approved
 * @property bool $agent_approved
 * @property int $created_at
 * @property int $updated_at
 * @property-read \App\Models\Agent $agent
 * @property-read mixed $hash_agent_id
 * @property-read mixed $hash_id
 * @property-read mixed $hash_operator_id
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $operator
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Collaboration whereAgentApproved($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Collaboration whereAgentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Collaboration whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Collaboration whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Collaboration whereOperatorApproved($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Collaboration whereOperatorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Collaboration whereOperatorType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Collaboration whereUpdatedAt($value)
 */
	class Collaboration extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Cruise
 *
 * @property int $id
 * @property int $cruise_operator_id
 * @property int $boat_id
 * @property string $start_date
 * @property string $end_date
 * @property string $start_place
 * @property string $end_place
 * @property bool $occupancy_per_cruise
 * @property int $free_pax
 * @property int $price_per_pax
 * @property bool $promotion_active
 * @property int $promotion_price_per_pax
 * @property string $currency
 * @property int $itinerary_id
 * @property bool $confirmed
 * @property int $created_at
 * @property int $updated_at
 * @property-read \App\Models\Boat $boat
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CruiseCabinOccupancy[] $cruiseCabinOccupancies
 * @property-read \App\Models\CruiseOperator $cruiseOperator
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CruisePromotion[] $cruisePromotions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CruiseSurcharge[] $cruiseSurcharges
 * @property-read mixed $hash_boat_id
 * @property-read mixed $hash_cruise_operator_id
 * @property-read mixed $hash_id
 * @property-read mixed $hash_itinerary_id
 * @property-read \App\Models\Itinerary $itinerary
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cruise whereBoatId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cruise whereConfirmed($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cruise whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cruise whereCruiseOperatorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cruise whereCurrency($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cruise whereEndDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cruise whereEndPlace($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cruise whereFreePax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cruise whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cruise whereItineraryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cruise whereOccupancyPerCruise($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cruise wherePricePerPax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cruise wherePromotionActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cruise wherePromotionPricePerPax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cruise whereStartDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cruise whereStartPlace($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cruise whereUpdatedAt($value)
 */
	class Cruise extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CruiseCabinOccupancy
 *
 * @property int $id
 * @property int $cruise_id
 * @property int $cabin_id
 * @property int $occupancy
 * @property int $price
 * @property int $promotion_price
 * @property int $created_at
 * @property int $updated_at
 * @property-read \App\Models\Cabin $cabin
 * @property-read \App\Models\Cruise $cruise
 * @property-read mixed $hash_cabin_id
 * @property-read mixed $hash_cruise_id
 * @property-read mixed $hash_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseCabinOccupancy whereCabinId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseCabinOccupancy whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseCabinOccupancy whereCruiseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseCabinOccupancy whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseCabinOccupancy whereOccupancy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseCabinOccupancy wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseCabinOccupancy wherePromotionPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseCabinOccupancy whereUpdatedAt($value)
 */
	class CruiseCabinOccupancy extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CruiseOperator
 *
 * @property int $id
 * @property string $hash_code
 * @property string $name
 * @property string $address
 * @property string $responsible_person
 * @property string $email
 * @property string $email_2
 * @property string $phone
 * @property string $phone_2
 * @property string $website
 * @property string $beneficiary_eur
 * @property string $swift_eur
 * @property string $iban_account_eur
 * @property string $beneficiary_usd
 * @property string $swift_usd
 * @property string $iban_account_usd
 * @property string $local_currency
 * @property string $beneficiary_local
 * @property string $swift_local
 * @property string $iban_account_local
 * @property string $cancellation_policy
 * @property string $groups_policy
 * @property string $notes
 * @property int $created_at
 * @property int $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Collaboration[] $collaborations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cruise[] $cruises
 * @property-read mixed $hash_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereBeneficiaryEur($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereBeneficiaryLocal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereBeneficiaryUsd($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereCancellationPolicy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereEmail2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereGroupsPolicy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereHashCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereIbanAccountEur($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereIbanAccountLocal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereIbanAccountUsd($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereLocalCurrency($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereNotes($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator wherePhone2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereResponsiblePerson($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereSwiftEur($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereSwiftLocal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereSwiftUsd($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseOperator whereWebsite($value)
 */
	class CruiseOperator extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CruisePromotion
 *
 * @property int $id
 * @property int $cruise_id
 * @property string $start_date
 * @property string $end_date
 * @property string $type
 * @property int $rate
 * @property int $created_at
 * @property int $updated_at
 * @property-read \App\Models\Cruise $cruise
 * @property-read mixed $hash_cruise_id
 * @property-read mixed $hash_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruisePromotion whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruisePromotion whereCruiseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruisePromotion whereEndDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruisePromotion whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruisePromotion whereRate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruisePromotion whereStartDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruisePromotion whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruisePromotion whereUpdatedAt($value)
 */
	class CruisePromotion extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CruiseSurcharge
 *
 * @property int $id
 * @property int $cruise_id
 * @property int $surcharge_id
 * @property int $amount
 * @property string $currency
 * @property string $payment
 * @property int $created_at
 * @property int $updated_at
 * @property-read \App\Models\Cruise $cruise
 * @property-read mixed $hash_cruise_id
 * @property-read mixed $hash_id
 * @property-read mixed $hash_surcharge_id
 * @property-read \App\Models\Surcharge $surcharge
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseSurcharge whereAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseSurcharge whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseSurcharge whereCruiseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseSurcharge whereCurrency($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseSurcharge whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseSurcharge wherePayment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseSurcharge whereSurchargeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CruiseSurcharge whereUpdatedAt($value)
 */
	class CruiseSurcharge extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CsvImportAlert
 *
 * @property int $id
 * @property int $boat_id
 * @property \Carbon\Carbon $start_date
 * @property \Carbon\Carbon $end_date
 * @property string $start_place
 * @property string $end_place
 * @property bool $occupancy_per_cruise
 * @property int $free_pax
 * @property int $price_per_pax
 * @property string $currency
 * @property string $itinerary
 * @property int $created_at
 * @property int $updated_at
 * @property-read \App\Models\Boat $boat
 * @property-read mixed $hash_cruise_id
 * @property-read mixed $hash_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvImportAlert whereBoatId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvImportAlert whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvImportAlert whereCurrency($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvImportAlert whereEndDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvImportAlert whereEndPlace($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvImportAlert whereFreePax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvImportAlert whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvImportAlert whereItinerary($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvImportAlert whereOccupancyPerCruise($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvImportAlert wherePricePerPax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvImportAlert whereStartDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvImportAlert whereStartPlace($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvImportAlert whereUpdatedAt($value)
 */
	class CsvImportAlert extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CsvSource
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property int $start_row
 * @property string $separator
 * @property int $column_boat_id_index
 * @property int $column_start_date_index
 * @property int $column_end_date_index
 * @property int $column_start_place_index
 * @property int $column_end_place_index
 * @property int $column_itinerary_index
 * @property int $column_free_pax_index
 * @property int $column_price_per_pax_index
 * @property int $column_currency_index
 * @property int $column_confirmed_index
 * @property string $column_confirmed_value
 * @property string $default_currency
 * @property string $date_format
 * @property string $id_map_arr
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read mixed $hash_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereColumnBoatIdIndex($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereColumnConfirmedIndex($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereColumnConfirmedValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereColumnCurrencyIndex($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereColumnEndDateIndex($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereColumnEndPlaceIndex($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereColumnFreePaxIndex($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereColumnItineraryIndex($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereColumnPricePerPaxIndex($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereColumnStartDateIndex($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereColumnStartPlaceIndex($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereDateFormat($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereDefaultCurrency($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereIdMapArr($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereSeparator($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereStartRow($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CsvSource whereUrl($value)
 */
	class CsvSource extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Currency
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property bool $active
 * @property int $order
 * @property int $created_at
 * @property int $updated_at
 * @property-read mixed $hash_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Currency whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Currency whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Currency whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Currency whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Currency whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Currency whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Currency whereUpdatedAt($value)
 */
	class Currency extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Deck
 *
 * @property int $id
 * @property int $boat_id
 * @property string $name
 * @property int $order
 * @property int $created_at
 * @property int $updated_at
 * @property-read \App\Models\Boat $boat
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cabin[] $cabins
 * @property-read mixed $hash_boat_id
 * @property-read mixed $hash_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Photo[] $photos
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Deck whereBoatId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Deck whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Deck whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Deck whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Deck whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Deck whereUpdatedAt($value)
 */
	class Deck extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Itinerary
 *
 * @property int $id
 * @property int $operator_id
 * @property string $name
 * @property string $disembarkation
 * @property string $embarkation
 * @property string $completed_training
 * @property string $number_of_dives
 * @property bool $buoy
 * @property bool $equipment
 * @property bool $computer
 * @property string $picture_map
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 * @property-read \App\Models\BoatOperator $boatOperator
 * @property-read mixed $hash_id
 * @property-read mixed $hash_operator_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ItineraryKeyword[] $keywords
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Itinerary whereBuoy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Itinerary whereCompletedTraining($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Itinerary whereComputer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Itinerary whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Itinerary whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Itinerary whereDisembarkation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Itinerary whereEmbarkation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Itinerary whereEquipment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Itinerary whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Itinerary whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Itinerary whereNumberOfDives($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Itinerary whereOperatorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Itinerary wherePictureMap($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Itinerary whereUpdatedAt($value)
 */
	class Itinerary extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ItineraryKeyword
 *
 * @property int $id
 * @property string $name
 * @property bool $approved
 * @property int $created_at
 * @property int $updated_at
 * @property-read mixed $hash_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Itinerary[] $itineraries
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ItineraryKeyword whereApproved($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ItineraryKeyword whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ItineraryKeyword whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ItineraryKeyword whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ItineraryKeyword whereUpdatedAt($value)
 */
	class ItineraryKeyword extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Permission
 *
 * @property int $id
 * @property string $name
 * @property string $display_name
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 * @property-read mixed $hash_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permission whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permission whereDisplayName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permission whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permission whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permission whereUpdatedAt($value)
 */
	class Permission extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Photo
 *
 * @property int $id
 * @property string $filename
 * @property string $path
 * @property int $file_size
 * @property int $order
 * @property int $model_id
 * @property string $model_type
 * @property int $created_at
 * @property int $updated_at
 * @property-read mixed $hash_id
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $model
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Photo whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Photo whereFileSize($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Photo whereFilename($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Photo whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Photo whereModelId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Photo whereModelType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Photo whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Photo wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Photo whereUpdatedAt($value)
 */
	class Photo extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Role
 *
 * @property int $id
 * @property string $name
 * @property string $display_name
 * @property string $description
 * @property bool $editable
 * @property int $created_at
 * @property int $updated_at
 * @property-read mixed $hash_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permission[] $permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role whereDisplayName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role whereEditable($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role whereUpdatedAt($value)
 */
	class Role extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Surcharge
 *
 * @property int $id
 * @property string $name
 * @property int $created_at
 * @property int $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CruiseSurcharge[] $cruiseSurcharges
 * @property-read mixed $hash_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Surcharge whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Surcharge whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Surcharge whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Surcharge whereUpdatedAt($value)
 */
	class Surcharge extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property int $cruise_operator_id
 * @property int $agent_id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property bool $active
 * @property string $remember_token
 * @property int $role_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $boat_operator_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Activity[] $activities
 * @property-read \App\Models\Agent $agent
 * @property-read \App\Models\BoatOperator $boatOperator
 * @property-read \App\Models\CruiseOperator $cruiseOperator
 * @property-read mixed $hash_agent_id
 * @property-read mixed $hash_boat_operator_id
 * @property-read mixed $hash_cruise_operator_id
 * @property-read mixed $hash_id
 * @property-read mixed $hash_role_id
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \App\Models\Role $role
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereAgentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereBoatOperatorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCruiseOperatorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRoleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

