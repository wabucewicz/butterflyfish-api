<?php

function logActivity(string $text, App\Models\User $user = null, string $type = 'info')
{
    $data = [
        'type' => $type,
        'description' => $text
    ];
    if (!empty($user)) {
        $user->activities()->create($data);
    } else {
        \App\Models\Activity::create($data);
    }
}