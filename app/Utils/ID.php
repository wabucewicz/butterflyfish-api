<?php

namespace App\Utils;

use Hashids;

class ID
{
    /**
     * Encodes an ID value to its hash value
     *
     * @param int    $value
     * @param string $config
     *
     * @return string
     */
    public static function encode($value, $config = 'main')
    {
        if (!is_null($value)) {
            if (is_array($value)) {
                $result = [];
                foreach ($value as $val) {
                    $result[] = Hashids::connection($config)->encode($val);
                }
                return $result;
            }
            return Hashids::connection($config)->encode($value);
        }
        return null;
    }

    /**
     * Decodes an ID value to the numeric string value
     *
     * @param string $value
     * @param string $config
     *
     * @return array
     */
    public static function decode($value, $config = 'main')
    {
        if (!is_null($value)) {
            if (is_array($value)) {
                $result = [];
                foreach ($value as $val) {
                    $id = Hashids::connection($config)->decode($val);
                    $result[] = isset($id[0]) ? $id[0] : null;
                }
                return $result;
            }
            $id = Hashids::connection($config)->decode($value);
            return isset($id[0]) ? $id[0] : null;
        }
        return null;

    }
}