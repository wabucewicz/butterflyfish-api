<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Utils\ID;

class Surcharge extends Model
{
    protected $table = 'surcharges';

    protected $hidden = [
        'id'
    ];

    protected $fillable = [
        'name'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'name' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function cruiseSurcharges()
    {
        return $this->hasMany('App\Models\CruiseSurcharge');
    }
}
