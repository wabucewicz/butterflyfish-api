<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Utils\ID;

class CsvSource extends Model
{
    protected $table = 'csv_sources';

    protected $hidden = [
        'id'
    ];

    protected $fillable = [
        'name',
        'url',
        'start_row',
        'separator',
        'column_boat_id_index',
        'column_start_date_index',
        'column_end_date_index',
        'column_start_place_index',
        'column_end_place_index',
        'column_itinerary_index',
        'column_free_pax_index',
        'column_price_per_pax_index',
        'column_currency_index',
        'column_confirmed_index',
        'column_confirmed_value',
        'default_currency',
        'date_format',
        'id_map_arr'
    ];

    protected $cast = [
        'hash_id' => 'string',
        'name' => 'string',
        'url' => 'string',
        'start_row' => 'integer',
        'separator' => 'string',
        'column_boat_id_index' => 'integer',
        'column_start_date_index' => 'integer',
        'column_end_date_index' => 'integer',
        'column_start_place_index' => 'integer',
        'column_end_place_index' => 'integer',
        'column_itinerary_index' => 'integer',
        'column_free_pax_index' => 'integer',
        'column_price_per_pax_index' => 'integer',
        'column_currency_index' => 'integer',
        'column_confirmed_index' => 'integer',
        'column_confirmed_value' => 'integer',
        'default_currency' => 'string',
        'date_format' => 'string',
        'id_map_arr' => 'array',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

}
