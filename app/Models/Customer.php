<?php

namespace App\Models;

use App\Utils\ID;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';

    protected $hidden = [
        'id'
    ];

    protected $fillable = [
        'title',
        'first_name',
        'last_name',
        'contact_email',
        'born_date'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'title' => 'string',
        'first_name' => 'string',
        'last_name' => 'string',
        'contact_email' => 'string',
        'born_date' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function reservations()
    {
        return $this->hasMany('App\Models\Reservation');
    }



}
