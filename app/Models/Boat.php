<?php

namespace App\Models;

use App\Utils\ID;
use Illuminate\Database\Eloquent\Model;

class Boat extends Model
{
    protected $table = 'boats';

    protected $hidden = [
        'id',
        'pivot'
    ];

    protected $fillable = [
        'name',
        'description',
        'boat_website_url',
        'data_source',
        'schedules_website_url',
        'csv_source_url',
        'cancellation_policy',
        'prepayment_policy',
        'notes',
        'year_built',
        'year_renovated',
        'beam',
        'beam_unit',
        'build_from',
        'number_of_cabins',
        'number_of_crew_members',
        'number_of_dive_guides',
        'max_quests',
        'quantities_pax',
        'laundry_service',
        'leisure_deck',
        'spa',
        'massage',
        'daily_housekeeping',
        'audio_and_video_entertainment',
        'wifi',
        'free_internet',
        'air_conditioned_saloon',
        'aircon_in_cabins',
        'sun_deck',
        'snorkeler_friendly',
        'outdoor_dining',
        'hot_tub_jacuzzi',
        'onboard_kayaks',
        'surfing_board',
        'library',
        'indoor_saloon',
        'alcohol_mini_bar',
        'alcohol_bar',
        'freshwater_maker',
        'gps_emergency_rafts',
        'life_vests',
        'fire_alarm_and_fire_extinguishers',
        'oxygen',
        'first_aid_kits',
        'satellite_and_mobile_phones',
        'crew_trained_in_first_aid',
        'emergency_flares',
        'nitrox',
        'twelve_l_tanks',
        'fifteen_l_tanks',
        'din_tanks',
        'int_tanks',
        'rebreather',
        'dive_equipment_rental',
        'hot_drinks_after_diving',
        'hot_towels_for_divers',
        'warm_water_showers_for_divers',
        'rinse_for_divers',
        'photography_station',
        'separate_rinse_for_uw_camera',
        'camera_room_and_charging_point',
        'diving_deck_at_water_level',
        'shaded_diving_deck',
        'safety_sausage',
        'nautilus_lifeline',
        'macro_sticks'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'name' => 'string',
        'description' => 'string',
        'boat_website_url' => 'string',
        'data_source' => 'string',
        'schedules_website_url' => 'string',
        'csv_source_url' => 'string',
        'cancellation_policy' => 'string',
        'prepayment_policy' => 'string',
        'notes' => 'string',
        'year_built' => 'integer',
        'year_renovated' => 'integer',
        'beam' => 'integer',
        'beam_unit' => 'string',
        'build_from' => 'string',
        'number_of_cabins' => 'integer',
        'number_of_crew_members' => 'integer',
        'number_of_dive_guides' => 'integer',
        'max_quests' => 'integer',
        'quantities_pax' => 'integer',
        'laundry_service' => 'boolean',
        'leisure_deck' => 'boolean',
        'spa' => 'boolean',
        'massage' => 'boolean',
        'daily_housekeeping' => 'boolean',
        'audio_and_video_entertainment' => 'boolean',
        'wifi' => 'boolean',
        'free_internet' => 'boolean',
        'air_conditioned_saloon' => 'boolean',
        'aircon_in_cabins' => 'boolean',
        'sun_deck' => 'boolean',
        'snorkeler_friendly' => 'boolean',
        'outdoor_dining' => 'boolean',
        'hot_tub_jacuzzi' => 'boolean',
        'onboard_kayaks' => 'boolean',
        'surfing_board' => 'boolean',
        'library' => 'boolean',
        'indoor_saloon' => 'boolean',
        'alcohol_mini_bar' => 'boolean',
        'alcohol_bar' => 'boolean',
        'freshwater_maker' => 'boolean',
        'gps_emergency_rafts' => 'boolean',
        'life_vests' => 'boolean',
        'fire_alarm_and_fire_extinguishers' => 'boolean',
        'oxygen' => 'boolean',
        'first_aid_kits' => 'boolean',
        'satellite_and_mobile_phones' => 'boolean',
        'crew_trained_in_first_aid' => 'boolean',
        'emergency_flares' => 'boolean',
        'nitrox' => 'boolean',
        'twelve_l_tanks' => 'boolean',
        'fifteen_l_tanks' => 'boolean',
        'din_tanks' => 'boolean',
        'int_tanks' => 'boolean',
        'rebreather' => 'boolean',
        'dive_equipment_rental' => 'boolean',
        'hot_drinks_after_diving' => 'boolean',
        'hot_towels_for_divers' => 'boolean',
        'warm_water_showers_for_divers' => 'boolean',
        'rinse_for_divers' => 'boolean',
        'photography_station' => 'boolean',
        'separate_rinse_for_uw_camera' => 'boolean',
        'camera_room_and_charging_point' => 'boolean',
        'diving_deck_at_water_level' => 'boolean',
        'shaded_diving_deck' => 'boolean',
        'safety_sausage' => 'boolean',
        'nautilus_lifeline' => 'boolean',
        'macro_sticks' => 'boolean',
        'hash_code' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public static function create(array $attributes = [])
    {
        $boat = parent::create(array_merge($attributes));
        $hashCodes = Boat::all()->pluck('hash_code');
        do {
            $hashCode = md5(uniqid());
        } while ($hashCodes->contains($hashCode));
        $boat->hash_code = $hashCode;
        $boat->save();
        return $boat;
    }

    public function boatOperators()
    {
        return $this->belongsToMany('App\Models\BoatOperator');
    }

    public function decks()
    {
        return $this->hasMany('App\Models\Deck');
    }

    public function cabins()
    {
        return $this->hasMany('App\Models\Cabin');
    }

    public function photos()
    {
        return $this->morphMany('App\Models\Photo', 'model');
    }

    public function cruises()
    {
        return $this->hasMany('App\Models\Cruise');
    }

    public function csvImportAlerts()
    {
        return $this->hasMany('App\Models\CsvImportAlert');
    }

    /* Other methods */

    public function photosPath()
    {
        return Photo::STORAGE_LOCATION . 'boats/' . $this->hashId . '/';
    }

}

