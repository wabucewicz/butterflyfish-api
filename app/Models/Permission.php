<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Utils\ID;

class Permission extends Model
{
    protected $table = 'permissions';

    protected $hidden = [
        'id'
    ];

    protected $fillable =  [];

    protected $casts = [
        'hash_id' => 'string',
        'name' => 'string',
        'display_name' => 'string',
        'description' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'permission_role', 'permission_id', 'role_id');
    }
}