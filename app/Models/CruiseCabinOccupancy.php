<?php

namespace App\Models;

use App\Utils\ID;
use Illuminate\Database\Eloquent\Model;

class CruiseCabinOccupancy extends Model
{
    protected $table = 'cruise_cabin_occupancies';

    protected $hidden = [
        'id',
        'cruise_id',
        'cabin_id'
    ];

    protected $fillable = [
        'occupancy',
        'price',
        'cruise_id',
        'cabin_id'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'hash_cruise_id' => 'string',
        'hash_cabin_id' => 'string',
        'occupancy' => 'integer',
        'price' => 'integer',
        'promotion_price' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id',
        'hash_cruise_id',
        'hash_cabin_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function getHashCruiseIdAttribute()
    {
        return ID::encode($this->cruise_id);
    }

    public function getHashCabinIdAttribute()
    {
        return ID::encode($this->cabin_id);
    }

    public function cruise()
    {
        return $this->belongsTo('App\Models\Cruise');
    }

    public function cabin()
    {
        return $this->belongsTo('App\Models\Cabin');
    }
}
