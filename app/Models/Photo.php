<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Utils\ID;

class Photo extends Model
{
    const STORAGE_LOCATION = 'storage/photos/';
    const WATERMARK_PATH = 'gfx/watermark.png';

    protected $table = 'photos';

    protected $hidden = [
        'id'
    ];

    protected $fillable = [
        'filename',
        'path',
        'file_size',
        'order'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'filename' => 'string',
        'path' => 'string',
        'file_size' => 'integer',
        'order' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function model()
    {
        return $this->morphTo();
    }

}
