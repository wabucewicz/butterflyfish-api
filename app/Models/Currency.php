<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Utils\ID;

class Currency extends Model
{
    protected $table = 'currencies';

    protected $hidden = [
        'id'
    ];

    protected $fillable = [
        'code',
        'name'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'code' => 'string',
        'name' => 'string',
        'active' => 'boolean',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }
}
