<?php

namespace App\Models;

use App\Utils\ID;
use Illuminate\Database\Eloquent\Model;

class Cabin extends Model
{
    protected $table = 'cabins';

    protected $hidden = [
        'id',
        'boat_id',
        'deck_id'
    ];

    protected $fillable = [
        'name',
        'sng_beds',
        'dbl_beds',
        'max_pax',
        'min_pax',
        'shared',
        'max_shared',
        'description',
        'size',
        'size_unit',
        'ensuite',
        'aircon',
        'tv',
        'computer',
        'towels',
        'soap',
        'safe',
        'wifi',
        'wifi_free'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'hash_deck_id' => 'string',
        'name' => 'string',
        'sng_beds' => 'integer',
        'dbl_beds' => 'integer',
        'max_pax' => 'integer',
        'min_pax' => 'integer',
        'shared' => 'boolean',
        'max_shared' => 'integer',
        'description' => 'string',
        'size' => 'integer',
        'size_unit' => 'string',
        'ensuite' => 'boolean',
        'aircon' => 'boolean',
        'tv' => 'boolean',
        'computer' => 'boolean',
        'towels' => 'boolean',
        'soap' => 'boolean',
        'safe' => 'boolean',
        'wifi' => 'boolean',
        'wifi_free' => 'boolean',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id',
        'hash_deck_id',
        'hash_boat_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function getHashDeckIdAttribute()
    {
        return ID::encode($this->deck_id);
    }

    public function getHashBoatIdAttribute()
    {
        return ID::encode($this->boat_id);
    }

    public function boat()
    {
        return $this->belongsTo('App\Models\Boat');
    }

    public function deck()
    {
        return $this->belongsTo('App\Models\Deck');
    }

    public function photos()
    {
        return $this->morphMany('App\Models\Photo', 'model');
    }

    /* Other methods */

    public function photosPath()
    {
        return Photo::STORAGE_LOCATION . 'boats/' . $this->boat->hashId . '/cabins/' . $this->hashId . '/';
    }
}
