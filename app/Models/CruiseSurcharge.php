<?php

namespace App\Models;

use App\Utils\ID;
use Illuminate\Database\Eloquent\Model;

class CruiseSurcharge extends Model
{
    protected $table = 'cruise_surcharges';

    protected $hidden = [
        'id',
        'cruise_id',
        'surcharge_id'
    ];

    protected $fillable = [
        'amount',
        'currency',
        'payment'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'amount' => 'integer',
        'currency' => 'string',
        'payment' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id',
        'hash_cruise_id',
        'hash_surcharge_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function getHashCruiseIdAttribute()
    {
        return ID::encode($this->cruise_id);
    }

    public function getHashSurchargeIdAttribute()
    {
        return ID::encode($this->surcharge_id);
    }

    public function cruise()
    {
        return $this->belongsTo('App\Models\Cruise');
    }

    public function surcharge()
    {
        return $this->belongsTo('App\Models\Surcharge');
    }
}
