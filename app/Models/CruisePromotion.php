<?php

namespace App\Models;

use App\Utils\ID;
use Illuminate\Database\Eloquent\Model;

class CruisePromotion extends Model
{
    protected $table = 'cruise_promotions';

    protected $hidden = [
        'id',
        'cruise_id'
    ];

    protected $fillable = [
        'start_date',
        'end_date',
        'type',
        'rate'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'hash_cruise_id' => 'string',
        'start_date' => 'string',
        'end_date' => 'string',
        'type' => 'string',
        'rate' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id',
        'hash_cruise_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function getHashCruiseIdAttribute()
    {
        return ID::encode($this->cruise_id);
    }

    public function cruise()
    {
        return $this->belongsTo('App\Models\Cruise');
    }
}
