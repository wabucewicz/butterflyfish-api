<?php

namespace App\Models;

use App\Utils\ID;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'activities';

    protected $hidden = [
        'id',
        'user_id'
    ];

    protected $fillable = [
        'type',
        'description'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'hash_user_id' => 'string',
        'type' => 'string',
        'description' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id',
        'hash_user_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function getHashUserIdAttribute()
    {
        return ID::encode($this->user_id);
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
