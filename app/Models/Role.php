<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Utils\ID;

class Role extends Model
{
    protected $table = 'roles';

    protected $hidden = [
        'id'
    ];

    protected $fillable =  [
        'name',
        'display_name',
        'description'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'name' => 'string',
        'display_name' => 'string',
        'description' => 'string',
        'editable' => 'boolean',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permission', 'permission_role', 'role_id', 'permission_id');
    }

    public function users()
    {
        return $this->hasMany('App\Models\User', 'role_id', 'id');
    }
}