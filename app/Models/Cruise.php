<?php

namespace App\Models;

use App\Utils\ID;
use Illuminate\Database\Eloquent\Model;

class Cruise extends Model
{
    protected $table = 'cruises';

    protected $hidden = [
        'id',
        'boat_id',
        'itinerary_id',
        'cruise_operator_id'
    ];

    protected $fillable = [
        'start_date',
        'end_date',
        'start_place',
        'end_place',
        'occupancy_per_cruise',
        'free_pax',
        'price_per_pax',
        'currency',
        'confirmed'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'hash_boat_id' => 'string',
        'hash_itinerary_id' => 'string',
        'start_date' => 'string',
        'end_date' => 'string',
        'start_place' => 'string',
        'end_place' => 'string',
        'occupancy_per_cruise' => 'boolean',
        'free_pax' => 'integer',
        'price_per_pax' => 'integer',
        'promotion_price_per_pax' => 'integer',
        'currency' => 'string',
        'confirmed' => 'boolean',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    protected $appends = [
        'hash_id',
        'hash_boat_id',
        'hash_itinerary_id',
        'hash_cruise_operator_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function getHashBoatIdAttribute()
    {
        return ID::encode($this->boat_id);
    }

    public function getHashItineraryIdAttribute()
    {
        return ID::encode($this->itinerary_id);
    }

    public function getHashCruiseOperatorIdAttribute()
    {
        return ID::encode($this->cruise_operator_id);
    }

    public function boat()
    {
        return $this->belongsTo('App\Models\Boat', 'boat_id', 'id');
    }

    public function itinerary()
    {
        return $this->belongsTo('App\Models\Itinerary');
    }

    public function cruiseCabinOccupancies()
    {
        return $this->hasMany('App\Models\CruiseCabinOccupancy');
    }

    public function cruiseSurcharges()
    {
        return $this->hasMany('App\Models\CruiseSurcharge');
    }

    public function cruisePromotions()
    {
        return $this->hasMany('App\Models\CruisePromotion');
    }

    public function cruiseOperator()
    {
        return $this->belongsTo('App\Models\CruiseOperator');
    }

    public function reservations()
    {
        return $this->hasMany('App\Models\Reservation');
    }

}
