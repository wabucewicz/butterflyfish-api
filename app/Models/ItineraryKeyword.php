<?php

namespace App\Models;

use App\Utils\ID;
use Illuminate\Database\Eloquent\Model;

class ItineraryKeyword extends Model
{
    protected $table = 'itinerary_keywords';

    protected $hidden =  [
        'id'
    ];

    protected $fillable = [
        'name',
        'approved'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'name' => 'string',
        'approved' => 'boolean',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function itineraries()
    {
        return $this->belongsToMany('App\Models\Itinerary', 'itinerary_itinerary_keyword', 'itinerary_keyword_id', 'itinerary_id');
    }
}
