<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Utils\ID;

class Deck extends Model
{
    protected $table = 'decks';

    protected $hidden = [
        'id',
        'boat_id'
    ];

    protected $fillable = [
        'name',
        'order'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'hash_boat_id' => 'string',
        'name' => 'string',
        'order' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id',
        'hash_boat_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function getHashBoatIdAttribute()
    {
        return ID::encode($this->boat_id);
    }

    public function boat()
    {
        return $this->belongsTo('App\Models\Boat');
    }

    public function cabins()
    {
        return $this->hasMany('App\Models\Cabin');
    }

    public function photos()
    {
        return $this->morphMany('App\Models\Photo', 'model');
    }

    /* Other methods */

    public function photosPath()
    {
        return Photo::STORAGE_LOCATION . 'boats/' . $this->boat->hashId . '/decks/' . $this->hashId . '/';
    }
}
