<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Utils\ID;

class CsvImportAlert extends Model
{
    protected $table = 'csv_import_alerts';

    protected $hidden = [
        'id'
    ];

    protected $fillable = [
        'start_date',
        'end_date',
        'start_place',
        'end_place',
        'occupancy_per_cruise',
        'free_pax',
        'price_per_pax',
        'currency',
        'itinerary'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'hash_cruise_id' => 'string',
        'start_date' => 'date',
        'end_date' => 'date',
        'start_place' => 'string',
        'end_place' => 'string',
        'occupancy_per_cruise' => 'boolean',
        'free_pax' => 'integer',
        'price_per_pax' => 'integer',
        'currency' => 'string',
        'itinerary' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id',
        'hash_cruise_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function getHashCruiseIdAttribute()
    {
        return ID::encode($this->cruise_id);
    }

    public function boat()
    {
        return $this->belongsTo('App\Models\Boat', 'boat_id', 'id');
    }
}
