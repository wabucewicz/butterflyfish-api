<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Utils\ID;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    protected $hidden = [
        'id',
        'password',
        'remember_token',
        'role_id',
        'boat_operator_id',
        'cruise_operator_id',
        'agent_id'
    ];

    protected $fillable = [
        'name',
        'email',
        'password',
        'active'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'hash_role_id' => 'string',
        'hash_boat_operator_id' => 'string',
        'hash_cruise_operator_id' => 'string',
        'name' => 'string',
        'email' => 'string',
        'active' => 'boolean',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id',
        'hash_role_id',
        'hash_boat_operator_id',
        'hash_cruise_operator_id',
        'hash_agent_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function getHashRoleIdAttribute()
    {
        return ID::encode($this->role_id);
    }

    public function getHashBoatOperatorIdAttribute()
    {
        return ID::encode($this->boat_operator_id);
    }

    public function getHashCruiseOperatorIdAttribute()
    {
        return ID::encode($this->cruise_operator_id);
    }

    public function getHashAgentIdAttribute()
    {
        return ID::encode($this->agent_id);
    }

    public function boatOperator()
    {
        return $this->belongsTo('App\Models\BoatOperator');
    }

    public function cruiseOperator()
    {
        return $this->belongsTo('App\Models\CruiseOperator');
    }

    public function agent()
    {
        return $this->belongsTo('App\Models\Agent');
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role', 'role_id', 'id');
    }

    public function isAllowed($permissionName)
    {
        return (bool) $this->role->permissions()->where('name', $permissionName)->count();
    }

    public function activities()
    {
        return $this->hasMany('App\Models\Activity', 'user_id', 'id');
    }


}
