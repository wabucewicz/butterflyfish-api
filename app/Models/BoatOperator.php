<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Utils\ID;

class BoatOperator extends Model
{
    protected $table = 'boat_operators';

    protected $hidden = [
        'id',
        'pivot'
    ];

    protected $fillable = [
        'name',
        'address',
        'responsible_person',
        'email',
        'email_2',
        'phone',
        'phone_2',
        'website',
        'beneficiary_eur',
        'swift_eur',
        'iban_eur',
        'beneficiary_usd',
        'swift_usd',
        'iban_usd',
        'local_currency',
        'beneficiary_local',
        'swift_local',
        'iban_local',
        'default_cancellation_policy',
        'groups_policy',
        'notes'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'hash_code' => 'string',
        'name' => 'string',
        'address' => 'string',
        'responsible_person' => 'string',
        'email' => 'string',
        'email_2' => 'string',
        'phone' => 'string',
        'phone_2' => 'string',
        'website' => 'string',
        'beneficiary_eur' => 'string',
        'swift_eur' => 'string',
        'iban_account_eur' => 'string',
        'beneficiary_usd' => 'string',
        'swift_usd' => 'string',
        'iban_account_usd' => 'string',
        'local_currency' => 'string',
        'beneficiary_local' => 'string',
        'swift_local' => 'string',
        'iban_account_local' => 'string',
        'cancellation_policy' => 'string',
        'groups_policy' => 'string',
        'notes' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public static function create(array $attributes = [])
    {
        $boatOperator = parent::create(array_merge($attributes));
        $hashCodes = BoatOperator::all()->pluck('hash_code');
        do {
            $hashCode = md5(uniqid());
        } while ($hashCodes->contains($hashCode));
        $boatOperator->hash_code = $hashCode;
        $boatOperator->save();
        return $boatOperator;
    }

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    public function boats()
    {
        return $this->belongsToMany('App\Models\Boat');
    }

    public function itineraries()
    {
        return $this->hasMany('App\Models\Itinerary');
    }

    public function collaborations()
    {
        return $this->morphMany('App\Models\Collaboration', 'operator');
    }
}
