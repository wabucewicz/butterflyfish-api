<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Utils\ID;

class Itinerary extends Model
{
    protected $table = 'itineraries';

    protected $hidden = [
        'id',
        'operator_id'
    ];

    protected $fillable = [
        'name',
        'embarkation',
        'disembarkation',
        'completed_training',
        'number_of_dives',
        'buoy',
        'equipment',
        'computer',
        'picture_map',
        'description'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'hash_operator_id' =>  'string',
        'name' => 'string',
        'embarkation' => 'string',
        'disembarkation' => 'string',
        'completed_training' => 'string',
        'number_of_dives' => 'string',
        'buoy' => 'boolean',
        'equipment' => 'boolean',
        'computer' => 'boolean',
        'picture_map' => 'string',
        'description' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id',
        'hash_operator_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function getHashOperatorIdAttribute()
    {
        return ID::encode($this->operator_id);
    }

    public function keywords()
    {
        return $this->belongsToMany('App\Models\ItineraryKeyword', 'itinerary_itinerary_keyword', 'itinerary_id', 'itinerary_keyword_id');
    }

    public function boatOperator()
    {
        return $this->belongsTo('App\Models\BoatOperator');
    }
}
