<?php

namespace App\Models;

use App\Utils\ID;
use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $table = 'agents';

    protected $hidden = [
        'id'
    ];

    protected $fillable = [
        'name',
        'address',
        'responsible_person',
        'email',
        'email_2',
        'phone',
        'phone_2',
        'website',
        'notes'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'hash_code' => 'string',
        'name' => 'string',
        'address' => 'string',
        'responsible_person' => 'string',
        'email' => 'string',
        'email_2' => 'string',
        'phone' => 'string',
        'phone_2' => 'string',
        'website' => 'string',
        'notes' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public static function create(array $attributes = [])
    {
        $agent = parent::create(array_merge($attributes));
        $hashCodes = Agent::all()->pluck('hash_code');
        do {
            $hashCode = md5(uniqid());
        } while ($hashCodes->contains($hashCode));
        $agent->hash_code = $hashCode;
        $agent->save();
        return $agent;
    }

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    public function collaborations()
    {
        return $this->hasMany('App\Models\Collaboration');
    }

    public function reservations()
    {
        return $this->hasMany('App\Models\Reservation');
    }

}
