<?php

namespace App\Models;

use App\Utils\ID;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $table = 'reservations';

    protected $hidden = [
        'id',
        'cruise_id',
        'user_id',
        'customer_id',
        'agent_id'
    ];

    protected $fillable = [
        'cruise_id',
        'user_id',
        'customer_id',
        'agent_id',
        'passengers_data_arr',
        'surcharges_data_arr',
        'currency'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'hash_cruise_id' => 'string',
        'hash_user_id' => 'string',
        'hash_customer_id' => 'string',
        'passengers_data_arr' => 'array',
        'surcharges_data_arr' => 'array',
        'currency' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id',
        'hash_cruise_id',
        'hash_user_id',
        'hash_customer_id',
        'hash_agent_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function getHashCruiseIdAttribute()
    {
        return ID::encode($this->cruise_id);
    }

    public function getHashUserIdAttribute()
    {
        return ID::encode($this->user_id);
    }

    public function getHashCustomerIdAttribute()
    {
        return ID::encode($this->customer_id);
    }

    public function getHashAgentIdAttribute()
    {
        return ID::encode($this->agent_id);
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function cruise()
    {
        return $this->belongsTo('App\Models\Cruise');
    }

    public function agent()
    {
        return $this->belongsTo('App\Models\Agent');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
