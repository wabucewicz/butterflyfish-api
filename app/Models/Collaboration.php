<?php

namespace App\Models;

use App\Utils\ID;
use Illuminate\Database\Eloquent\Model;

class Collaboration extends Model
{
    protected $table = 'collaborations';

    protected $hidden = [
        'id',
        'operator_id',
        'agent_id'
    ];

    protected $fillable = [
        'operator_approved',
        'agent_approved',
        'agent_id'
    ];

    protected $casts = [
        'hash_id' => 'string',
        'hash_operator_id' => 'string',
        'operator_type' => 'string',
        'hash_agent_id' => 'string',
        'operator_approved' => 'boolean',
        'agent_approved' => 'boolean',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    protected $appends = [
        'hash_id',
        'hash_operator_id',
        'hash_agent_id'
    ];

    public function getHashIdAttribute()
    {
        return ID::encode($this->id);
    }

    public function getHashOperatorIdAttribute()
    {
        return ID::encode($this->operator_id);
    }

    public function getHashAgentIdAttribute()
    {
        return ID::encode($this->agent_id);
    }

    public function operator()
    {
        return $this->morphTo();
    }

    public function agent()
    {
        return $this->belongsTo('App\Models\Agent');
    }
}
