<?php

namespace App\Policies;

use App\Models\Boat;
use App\Models\Cruise;
use App\Models\BoatOperator;
use App\Models\CruiseOperator;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CruisePolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function listAll(User $user)
    {
        if ($user->isAllowed('cruises.list_all')) {
            return true;
        }
        return false;
    }

    public function listAssociated(User $user)
    {
        if ($user->isAllowed('cruises.list_associated')) {
            return true;
        }
        return false;
    }

    public function view(User $user, Cruise $cruise)
    {
        if ($user->isAllowed('cruises.show_all') || ($user->isAllowed('cruises.show_associated') && ($this->isBoatOperator($user, $cruise->boat_id) || $this->isCruiseOperator($user, $cruise) || $this->isCruiseAgent($user, $cruise)))) {
            return true;
        }
        return false;
    }

    public function create(User $user, Boat $boat)
    {
        if ($user->isAllowed('cruises.create_all') || ($user->isAllowed('cruises.create_associated') && $this->isBoatOperator($user, $boat->id))) {
            return true;
        }
        return false;
    }

    public function update(User $user, Cruise $cruise)
    {
        if ($user->isAllowed('cruises.edit_all') || ($user->isAllowed('cruises.edit_associated') && ($this->isBoatOperator($user, $cruise->boat_id) || $this->isCruiseOperator($user, $cruise)))) {
            return true;
        }
        return false;
    }

    public function setDates(User $user, Cruise $cruise)
    {
        if ($user->isAllowed('cruises.set_all_dates') || ($user->isAllowed('cruises.set_associated_dates') && $this->isBoatOperator($user, $cruise->boat_id))) {
            return true;
        }
        return false;
    }

    public function setPlaces(User $user, Cruise $cruise)
    {
        if ($user->isAllowed('cruises.set_all_places') || ($user->isAllowed('cruises.set_associated_places') && $this->isBoatOperator($user, $cruise->boat_id))) {
            return true;
        }
        return false;
    }

    public function setConfirmation(User $user, Cruise $cruise)
    {
        if ($user->isAllowed('cruises.set_all_confirm') || ($user->isAllowed('cruises.set_associated_confirm') && ($this->isBoatOperator($user, $cruise->boat_id) || $this->isCruiseOperator($user, $cruise)))) {
            return true;
        }
        return false;
    }

    public function setItinerary(User $user, Cruise $cruise)
    {
        if ($user->isAllowed('cruises.set_all_itinerary') || ($user->isAllowed('cruises.set_associated_itinerary') && ($this->isBoatOperator($user, $cruise->boat_id) || $this->isCruiseOperator($user, $cruise)))) {
            return true;
        }
        return false;
    }

    public function setPrice(User $user, Cruise $cruise)
    {
        if ($user->isAllowed('cruises.set_all_price') || ($user->isAllowed('cruises.set_associated_price') && ($this->isBoatOperator($user, $cruise->boat_id) || $this->isCruiseOperator($user, $cruise)))) {
            return true;
        }
        return false;
    }

    public function setCurrency(User $user, Cruise $cruise)
    {
        if ($user->isAllowed('cruises.set_all_currency') || ($user->isAllowed('cruises.set_associated_currency') && ($this->isBoatOperator($user, $cruise->boat_id) || $this->isCruiseOperator($user, $cruise)))) {
            return true;
        }
        return false;
    }

    public function setPax(User $user, Cruise $cruise)
    {
        if ($user->isAllowed('cruises.set_all_pax') || ($user->isAllowed('cruises.set_associated_pax') && ($this->isBoatOperator($user, $cruise->boat_id) || $this->isCruiseOperator($user, $cruise)))) {
            return true;
        }
        return false;
    }

    public function setSurcharges(User $user, Cruise $cruise)
    {
        if ($user->isAllowed('cruises.set_all_surcharges') || ($user->isAllowed('cruises.set_associated_surcharges') && ($this->isBoatOperator($user, $cruise->boat_id) || $this->isCruiseOperator($user, $cruise)))) {
            return true;
        }
        return false;
    }

    public function setPromotions(User $user, Cruise $cruise)
    {
        if ($user->isAllowed('cruises.set_all_promotions') || ($user->isAllowed('cruises.set_associated_promotions') && ($this->isBoatOperator($user, $cruise->boat_id) || $this->isCruiseOperator($user, $cruise)))) {
            return true;
        }
        return false;
    }

    public function delete(User $user, Cruise $cruise)
    {
        if ($user->isAllowed('cruises.delete_all') || ($user->isAllowed('cruises.delete_associated') && $this->isBoatOperator($user, $cruise->boat_id))) {
            return true;
        }
        return false;
    }

    public function assign(User $user, Cruise $cruise)
    {
        if ($user->isAllowed('cruises.assign_all') || ($user->isAllowed('cruises.assign_associated') && $this->isBoatOperator($user, $cruise->boat_id))) {
            return true;
        }
        return false;
    }

    public function unassign(User $user, Cruise $cruise)
    {
        if ($user->isAllowed('cruises.unassign_all') || ($user->isAllowed('cruises.unassign_associated') && $this->isBoatOperator($user, $cruise->boat_id))) {
            return true;
        }
        return false;
    }
}
