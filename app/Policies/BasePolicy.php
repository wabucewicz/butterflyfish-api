<?php

namespace App\Policies;

use App\Models\Agent;
use App\Models\Boat;
use App\Models\BoatOperator;
use App\Models\Cruise;
use App\Models\CruiseOperator;
use App\Models\User;

class BasePolicy
{
    protected function isBoatOperator(User $user, $boatId)
    {
        return $user->boatOperator instanceof BoatOperator && $user->boatOperator->boats->contains($boatId);
    }

    protected function isCruiseOperator(User $user, Cruise $cruise)
    {
        return $user->cruiseOperator instanceof CruiseOperator && $user->cruiseOperator->id == $cruise->cruise_operator_id;
    }

    protected function isCruiseAgent(User $user, Cruise $cruise)
    {
        if ($user->agent instanceof Agent) {
            $agentId = $user->agent->id;
            $cruise = Cruise::where(function ($query) use ($agentId) {
                $query->whereHas('boat', function ($query) use ($agentId) {
                    $query->whereHas('boatOperators', function ($query) use ($agentId) {
                        $query->whereHas('collaborations', function ($query) use ($agentId) {
                            $query->whereHas('agent', function ($query) use ($agentId) {
                                $query->where('id', $agentId);
                            });
                        });
                    });
                })->orWhereHas('cruiseOperator', function ($query) use ($agentId) {
                    $query->whereHas('collaborations', function ($query) use ($agentId) {
                        $query->whereHas('agent', function ($query) use ($agentId) {
                            $query->where('id', $agentId);
                        });
                    });
                });
            })->where('id', $cruise->id)->first();
            if ($cruise instanceof Cruise) {
                return true;
            }
        }
        return false;
    }

    protected function isRelatedWithBoat(User $user, $boatId)
    {
        if ($this->isBoatOperator($user, $boatId)) {
            return true;
        } elseif ($user->cruiseOperator instanceof CruiseOperator) {
            foreach ($user->cruiseOperator->cruises as $cruise) {
                if ($cruise->boat_id == $boatId) {
                    return true;
                }
            }
        } elseif ($user->agent instanceof Agent) {
            $agentId = $user->agent->id;
            $boat = Boat::where(function ($query) use ($agentId) {
                $query->whereHas('boatOperators', function ($query) use ($agentId) {
                    $query->whereHas('collaborations', function ($query) use ($agentId) {
                        $query->whereHas('agent', function ($query) use ($agentId) {
                            $query->where('id', $agentId);
                        });
                    });
                });
                $query->orWhereHas('cruises', function ($query) use ($agentId) {
                    $query->whereHas('cruiseOperator', function ($query) use ($agentId) {
                        $query->whereHas('collaborations', function ($query) use ($agentId) {
                            $query->whereHas('agent', function ($query) use ($agentId) {
                                $query->where('id', $agentId);
                            });
                        });
                    });
                });
            })->where('id', $boatId)->first();
            if ($boat instanceof Boat) {
                return true;
            }
        }
        return false;
    }
}