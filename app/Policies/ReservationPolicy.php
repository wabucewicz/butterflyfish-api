<?php

namespace App\Policies;

use App\Models\Agent;
use App\Models\Boat;
use App\Models\Cabin;
use App\Models\BoatOperator;
use App\Models\Cruise;
use App\Models\Reservation;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReservationPolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function listAll(User $user)
    {
        if ($user->isAllowed('reservations.list_all')) {
            return true;
        }
        return false;
    }

    public function listAssociated(User $user)
    {
        if ($user->isAllowed('reservations.list_associated')) {
            return true;
        }
        return false;
    }

    public function view(User $user, Reservation $reservation)
    {
        if ($user->isAllowed('reservations.show_all') || ($user->isAllowed('reservations.show_associated') && $this->canSellCruise($user, $reservation->cruise))) {
            return true;
        }
        return false;
    }

    public function create(User $user, Cruise $cruise)
    {
        if ($user->isAllowed('reservations.create_all') || ($user->isAllowed('reservations.create_associated') && $this->canSellCruise($user, $cruise))) {
            return true;
        }
        return false;
    }

    public function update(User $user, Reservation $reservation)
    {
        if ($user->isAllowed('reservations.edit_all') || ($user->isAllowed('reservations.edit_associated') && $this->canSellCruise($user, $reservation->cruise))) {
            return true;
        }
        return false;
    }

    public function delete(User $user, Reservation $reservation)
    {
        if ($user->isAllowed('reservations.delete_all') || ($user->isAllowed('reservations.delete_associated') && $this->canSellCruise($user, $reservation->cruise))) {
            return true;
        }
        return false;
    }

    protected function canSellCruise(User $user, Cruise $cruise)
    {
        if ($this->isCruiseOperator($user, $cruise) || ($cruise->cruise_operator_id == null && $this->isBoatOperator($user, $cruise->boat_id)) || $this->isCruiseAgent($user, $cruise)) {
            return true;
        }
        return false;
    }
}
