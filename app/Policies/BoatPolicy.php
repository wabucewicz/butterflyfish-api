<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Boat;
use Illuminate\Auth\Access\HandlesAuthorization;

class BoatPolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function listAll(User $user)
    {
        if ($user->isAllowed('boats.list_all')) {
            return true;
        }
        return false;
    }

    public function listAssociated(User $user)
    {
        if ($user->isAllowed('boats.list_associated')) {
            return true;
        }
        return false;
    }

    public function view(User $user, Boat $boat)
    {
        if ($user->isAllowed('boats.show_all') || ($user->isAllowed('boats.show_associated') && $this->isRelatedWithBoat($user, $boat->id))) {
            return true;
        }
        return false;
    }

    public function create(User $user)
    {
        if ($user->isAllowed('boats.create')) {
            return true;
        }
        return false;
    }

    public function update(User $user, Boat $boat)
    {
        if ($user->isAllowed('boats.edit_all') || ($user->isAllowed('boats.edit_associated') && $this->isRelatedWithBoat($user, $boat->id))) {
            return true;
        }
        return false;
    }

    public function delete(User $user, Boat $boat)
    {
        if ($user->isAllowed('boats.delete_all') || ($user->isAllowed('boats.delete_associated') && $this->isRelatedWithBoat($user, $boat->id))) {
            return true;
        }
        return false;
    }
}
