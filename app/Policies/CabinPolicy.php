<?php

namespace App\Policies;

use App\Models\Boat;
use App\Models\Cabin;
use App\Models\BoatOperator;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CabinPolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function list(User $user)
    {
        if ($user->isAllowed('cabins.list')) {
            return true;
        }
        return false;
    }

    public function listAssociated(User $user)
    {
        if ($user->isAllowed('cabins.list_associated')) {
            return true;
        }
        return false;
    }

    public function view(User $user, Cabin $cabin)
    {
        if ($user->isAllowed('cabins.show_all') || ($user->isAllowed('cabins.show_associated') && $user->boatOperator instanceof BoatOperator && $user->boatOperator->boats->contains($cabin->boat_id))) {
            return true;
        }
        return false;
    }

    public function create(User $user, Boat $boat)
    {
        if ($user->isAllowed('cabins.create_all') || $user->isAllowed('cabins.create_associated') && $user->boatOperator instanceof BoatOperator && $user->boatOperator->boats->contains($boat->id)) {
            return true;
        }
        return false;
    }

    public function update(User $user, Cabin $cabin)
    {
        if ($user->isAllowed('cabins.edit_all') || $user->isAllowed('cabins.edit_associated') && $user->boatOperator instanceof BoatOperator && $user->boatOperator->boats->contains($cabin->boat_id)) {
            return true;
        }
        return false;
    }

    public function delete(User $user, Cabin $cabin)
    {
        if ($user->isAllowed('cabins.delete_all') || $user->isAllowed('cabins.delete_associated') && $user->boatOperator instanceof BoatOperator && $user->boatOperator->boats->contains($cabin->boat_id)) {
            return true;
        }
        return false;
    }
}
