<?php

namespace App\Policies;

use App\Models\Agent;
use App\Models\BoatOperator;
use App\Models\Collaboration;
use App\Models\CruiseOperator;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CollaborationPolicy
{
    use HandlesAuthorization;

    public function listAll(User $user)
    {
        if ($user->isAllowed('collaborations.list_all')) {
            return true;
        }
        return false;
    }

    public function listAssociated(User $user)
    {
        if ($user->isAllowed('collaborations.list_associated')) {
            return true;
        }
        return false;
    }

    public function view(User $user, Collaboration $collaboration)
    {
        if ($user->isAllowed('collaborations.show_all')) {
            return true;
        } elseif ($user->isAllowed('collaborations.show_associated') && ($this->relatedAsBoatOperator($user, $collaboration) || $this->relatedAsCruiseOperator($user, $collaboration) || $this->relatedAsAgent($user, $collaboration))) {
            return true;
        }
        return false;
    }

    public function create(User $user)
    {
        if ($user->isAllowed('collaborations.create')) {
            return true;
        }
        return false;
    }

    public function approveAsOperator(User $user, Collaboration $collaboration)
    {
        if ($user->isAllowed('collaborations.approve') && ($this->relatedAsBoatOperator($user, $collaboration) || $this->relatedAsCruiseOperator($user, $collaboration))) {
            return true;
        }
        return false;
    }

    public function approveAsAgent(User $user, Collaboration $collaboration)
    {
        if ($user->isAllowed('collaborations.approve') && $this->relatedAsAgent($user, $collaboration)) {
            return true;
        }
        return false;
    }

    public function delete(User $user, Collaboration $collaboration)
    {
        if ($user->isAllowed('collaborations.delete_all')) {
            return true;
        } elseif ($user->isAllowed('collaborations.delete_associated') && ($this->relatedAsBoatOperator($user, $collaboration) || $this->relatedAsCruiseOperator($user, $collaboration) || $this->relatedAsAgent($user, $collaboration))) {
            return true;
        }
        return false;
    }

    private function relatedAsBoatOperator(User $user, Collaboration $collaboration)
    {
        return $user->boatOperator instanceof BoatOperator && $collaboration->operator instanceof BoatOperator && $user->boatOperator->id == $collaboration->operator->id;
    }

    private function relatedAsCruiseOperator(User $user, Collaboration $collaboration)
    {
        return $user->cruiseOperator instanceof CruiseOperator && $collaboration->operator instanceof CruiseOperator && $user->cruiseOperator->id == $collaboration->operator->id;
    }

    private function relatedAsAgent(User $user, Collaboration $collaboration)
    {
        return $user->agent instanceof Agent && $collaboration->agent instanceof Agent && $user->agent->id == $collaboration->agent->id;
    }
}
