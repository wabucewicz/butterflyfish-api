<?php

namespace App\Policies;

use App\Models\Boat;
use App\Models\Deck;
use App\Models\BoatOperator;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DeckPolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function list(User $user)
    {
        if ($user->isAllowed('decks.list')) {
            return true;
        }
        return false;
    }

    public function listAssociated(User $user)
    {
        if ($user->isAllowed('decks.list_associated')) {
            return true;
        }
        return false;
    }

    public function view(User $user, Deck $deck)
    {
        if ($user->isAllowed('decks.show_all') || ($user->isAllowed('decks.show_associated') && $user->boatOperator instanceof BoatOperator && $user->boatOperator->boats->contains($deck->boat_id))) {
            return true;
        }
        return false;
    }

    public function create(User $user, Boat $boat)
    {
        if ($user->isAllowed('decks.create_all') || $user->isAllowed('decks.create_associated') && $user->boatOperator instanceof BoatOperator && $user->boatOperator->boats->contains($boat->id)) {
            return true;
        }
        return false;
    }

    public function update(User $user, Deck $deck)
    {
        if ($user->isAllowed('decks.edit_all') || $user->isAllowed('decks.edit_associated') && $user->boatOperator instanceof BoatOperator && $user->boatOperator->boats->contains($deck->boat_id)) {
            return true;
        }
        return false;
    }

    public function delete(User $user, Deck $deck)
    {
        if ($user->isAllowed('decks.delete_all') || $user->isAllowed('decks.delete_associated') && $user->boatOperator instanceof BoatOperator && $user->boatOperator->boats->contains($deck->boat_id)) {
            return true;
        }
        return false;
    }
}
