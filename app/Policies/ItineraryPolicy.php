<?php

namespace App\Policies;

use App\Models\Itinerary;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ItineraryPolicy extends BasePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view(User $user, Itinerary $itinerary)
    {

    }

    public function create(User $user)
    {
        //
    }

    public function update(User $user, Itinerary $itinerary)
    {
        //
    }

    public function delete(User $user, Itinerary $itinerary)
    {
        //
    }

    public function listAll(User $user)
    {
        if ($user->isAllowed('itineraries.list_all')) {
            return true;
        }
        return false;
    }

    public function listAssociated(User $user)
    {
        if ($user->isAllowed('itineraries.list_associated')) {
            return true;
        }
        return false;
    }
}
