<?php

namespace App\Jobs;

use App\Models\CsvImportAlert;
use App\Models\CsvSource;
use App\Repositories\CruiseRepository;
use App\Repositories\CsvImportAlertRepository;
use App\Repositories\ItineraryRepository;
use App\Services\CsvService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ImportFromCsvSource implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $csvSource;

    /**
     * Create a new job instance.
     *
     * @param CsvSource $csvSource
     */
    public function __construct(CsvSource $csvSource)
    {
        $this->csvSource = $csvSource;
    }

    /**
     * Execute the job.
     *
     * @param CsvService $csvService
     * @return void
     */
    public function handle(CsvService $csvService)
    {
        $cruises = $csvService->getData($this->csvSource);
        foreach ($cruises as $cruiseData) {
            if ($csvService->validateCruise($cruiseData)) {
                $csvService->saveCruise($cruiseData);
            }
        }
    }

}
