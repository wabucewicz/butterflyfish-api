<?php

namespace App\Jobs;

use App\Models\CsvImportAlert;
use App\Models\CsvSource;
use App\Repositories\CruiseRepository;
use App\Repositories\CsvImportAlertRepository;
use App\Repositories\CsvSourceRepository;
use App\Repositories\ItineraryRepository;
use App\Services\CsvService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ImportFromAllCsvSources implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @param CsvSourceRepository $csvSourceRepository
     * @param CsvService $csvService
     * @return void
     */
    public function handle(CsvSourceRepository $csvSourceRepository, CsvService $csvService)
    {
        $csvSources = $csvSourceRepository->all();
        foreach ($csvSources as $csvSource) {
            $cruises = $csvService->getData($csvSource);
            foreach ($cruises as $cruiseData) {
                if ($csvService->validateCruise($cruiseData)) {
                    $csvService->saveCruise($cruiseData);
                }
            }
        }
    }

}
