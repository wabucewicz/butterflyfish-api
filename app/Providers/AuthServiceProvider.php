<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\Boat' => 'App\Policies\BoatPolicy',
        'App\Models\Deck' => 'App\Policies\DeckPolicy',
        'App\Models\Cabin' => 'App\Policies\CabinPolicy',
        'App\Models\Cruise' => 'App\Policies\CruisePolicy',
        'App\Models\Itinerary' => 'App\Policies\ItineraryPolicy',
        'App\Models\Collaboration' => 'App\Policies\CollaborationPolicy',
        'App\Models\Reservation' => 'App\Policies\ReservationPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
