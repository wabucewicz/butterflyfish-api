<?php

namespace App\Providers;

use App\Utils\ID;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('exists_hash_id', function ($attribute, $value, $parameters, $validator) {
            if (count($parameters) < 1) {
                throw new \InvalidArgumentException('Validation rule requires at least 1 parameter.');
            }
            $table = $parameters[0];
            if (!empty($parameters[1])) {
                $column = $parameters[1];
            } else {
                $column = 'id';
            }
            $result = \DB::table($table)->where($column, ID::decode($value))->count();
            return $result > 0;
        });

        Validator::extend('required_if_occupancy_per_cabin', function ($attribute, $value, $parameters, $validator) {
            $cruiseHashId = $validator->getData()['hash_cruise_id'];
            $cruise = \DB::table('cruises')->select(['id', 'occupancy_per_cruise'])->where('id', ID::decode($cruiseHashId))->first();
            return $cruise != null && $cruise->occupancy_per_cruise == false;
        });

        Validator::extend('available_space_in_cabin', function ($attribute, $value, $parameters, $validator) {
            $cruiseHashId = $validator->getData()['hash_cruise_id'];
            $cabinOccupancy = \DB::table('cruise_cabin_occupancies')->select('occupancy')->where('cruise_id', ID::decode($cruiseHashId))->where('cabin_id', ID::decode($value))->first();
            return $cabinOccupancy->occupancy >= 1;
        });

        Relation::morphMap([
            'boat_operator' => 'App\Models\BoatOperator',
            'cruise_operator' => 'App\Models\CruiseOperator',
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
