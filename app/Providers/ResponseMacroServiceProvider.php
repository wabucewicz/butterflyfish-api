<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Response;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('success', function ($message, $data) {
            return Response::json([
                'success' => true,
                'data'    => $data,
                'message' => $message,
            ], 200);
        });

        Response::macro('error', function ($message, $data) {
            return Response::json([
                'success' => false,
                'data'    => $data,
                'message' => $message,
            ], 400);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
