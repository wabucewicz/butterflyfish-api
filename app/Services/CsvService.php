<?php

namespace App\Services;

use App\Models\Cruise;
use App\Models\CsvSource;
use App\Models\Itinerary;
use App\Repositories\BoatRepository;
use App\Repositories\CruiseRepository;

use App\Repositories\CsvImportAlertRepository;
use App\Repositories\CsvSourceRepository;
use App\Repositories\ItineraryRepository;
use App\Utils\ID;
use DB;
use Exception;

class CsvService
{
    private $cruiseRepository;
    private $csvSourceRepository;
    private $csvImportAlertRepository;
    private $boatRepository;
    private $itineraryRepository;

    private $dir = 'public/schedule/';
    private $tmpDir = 'storage/app/csv/temp/';

    public function __construct(CruiseRepository $cruiseRepository, CsvSourceRepository $csvSourceRepository, CsvImportAlertRepository $csvImportAlertRepository, BoatRepository $boatRepository, ItineraryRepository $itineraryRepository)
    {
        $this->cruiseRepository = $cruiseRepository;
        $this->csvSourceRepository = $csvSourceRepository;
        $this->csvImportAlertRepository = $csvImportAlertRepository;
        $this->boatRepository = $boatRepository;
        $this->itineraryRepository = $itineraryRepository;
    }

    public function getExportFileName($boatId = null)
    {
        if (!is_null($boatId)) {
            return str_slug($this->boatRepository->getById($boatId)->name) . '_' . date('Ymd') . '.csv';
        } else {
            return 'all_cruises_' . date('Ymd') . '.csv';
        }
    }

    private function getCsvFilePath($fileName)
    {
        if (!is_dir(base_path($this->dir))) {
            mkdir(base_path($this->dir), 0755, true);
        }
        return base_path($this->dir . $fileName);
    }

    public function saveCruise(array $cruiseData)
    {
        $boat = $this->boatRepository->getById($cruiseData['boat_id']);
        $attributes = [
            'start_date' => $cruiseData['start_date'],
            'end_date' => $cruiseData['end_date']
        ];
        $cruise = $boat->cruises()->updateOrCreate($attributes, $cruiseData);
        if (isset($cruiseData['itinerary'])) {
            $itinerary = Itinerary::firstOrCreate(['name' => $cruiseData['itinerary']]);
            $cruise->itinerary()->associate($itinerary);
            $cruise->save();
        }
    }

    public function export($boatId = null)
    {
        $fileName = $this->getExportFileName($boatId);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="' . $fileName . '";');
        $fileHandle = fopen('php://output', 'w');

        fputcsv($fileHandle, [
            'Boat ID',
            'Boat name',
            'Start date',
            'End date',
            'Start place',
            'End place',
            'Itinerary',
            'Free spaces',
            'Price',
            'Currency',
        ], ';');

        if (!is_null($boatId)) {
            $cruises = $this->boatRepository->getById($boatId)->cruises;
        } else {
            $cruises = Cruise::all();
        }

        foreach ($cruises as $cruise) {
            if ($cruise->promotion_active) {
                if ($cruise->occupancy_per_cruise) {
                    $price = $cruise->price_per_pax;
                } elseif ($cruise->cruiseCabinOccupancies()->count()) {
                    $min = $cruise->cruiseCabinOccupancies->min('price');
                    $max = $cruise->cruiseCabinOccupancies->max('price');
                    if ($min && $max && $min == $max) {
                        $price = $min;
                    } elseif ($min && $max && $min < $max) {
                        $price = $min . ' to ' . $max;
                    } else {
                        $price = '';
                    }
                } else {
                    $price = '';
                }
            } else {
                if ($cruise->occupancy_per_cruise) {
                    $price = $cruise->promotion_price_per_pax;
                } elseif ($cruise->cruiseCabinOccupancies()->count()) {
                    $min = $cruise->cruiseCabinOccupancies->min('promotion_price');
                    $max = $cruise->cruiseCabinOccupancies->max('promotion_price');
                    if ($min && $max && $min == $max) {
                        $price = $min;
                    } elseif ($min && $max && $min < $max) {
                        $price = $min . ' to ' . $max;
                    } else {
                        $price = '';
                    }
                } else {
                    $price = '';
                }
            }

            fputcsv($fileHandle, [
                $cruise->boat->hashId,
                $cruise->boat->name,
                $cruise->start_date,
                $cruise->end_date,
                $cruise->start_place,
                $cruise->end_place,
                $cruise->itinerary ? $cruise->itinerary->name : '',
                $cruise->free_pax,
                $price,
                $cruise->currency,
            ], ';');
        }

        fclose($fileHandle);

    }

    private function getImportCsvTmpFilePath($fileName)
    {
        if (!is_dir(base_path($this->tmpDir))) {
            mkdir(base_path($this->tmpDir), 0755, true);
        }
        return base_path($this->tmpDir . $fileName);
    }

    private function downloadAndGetFile($sourceUrl)
    {
        $fileName = $this->getImportCsvTmpFilePath(md5(time()) . '.csv');
        try {
            $data = file_get_contents($sourceUrl);
        } catch (\Exception $e) {
            $data = false;
        }
        if (!$data) {
            return false;
        }
        $handle = fopen($fileName, 'w');
        fwrite($handle, $data);
        fclose($handle);
        return $fileName;
    }

    public function getData(CsvSource $csvSource)
    {
        $fileName = $this->downloadAndGetFile($csvSource->url);
        if (!$fileName) {
            return false;
        }
        $fileHandle = fopen($fileName, "r");
        $data = [];
        $n = 0;
        $mapId = unserialize($csvSource->id_map_arr);

        for ($i = 0; $i < $csvSource->start_row; $i++) {
            fgetcsv($fileHandle, 1, $csvSource->separator);
        }

        while (($row = fgetcsv($fileHandle, 0, $csvSource->separator)) !== FALSE) {
            if (array_key_exists($row[$csvSource->column_boat_id_index], $mapId)) {
                $data[$n]['boat_id'] = $mapId[$row[$csvSource->column_boat_id_index]];
                $data[$n]['start_date'] = date_create_from_format($csvSource->date_format, $row[$csvSource->column_start_date_index])->format('Y-m-d');
                $data[$n]['end_date'] = date_create_from_format($csvSource->date_format, $row[$csvSource->column_end_date_index])->format('Y-m-d');
                $data[$n]['start_place'] = $row[$csvSource->column_start_place_index];
                $data[$n]['end_place'] = $row[$csvSource->column_end_place_index];
                $data[$n]['itinerary'] = $csvSource->column_itinerary_index ? $row[$csvSource->column_itinerary_index] : null;
                $data[$n]['free_pax'] = $csvSource->column_free_pax_index ? $row[$csvSource->column_free_pax_index] : null;
                $data[$n]['price_per_pax'] = $csvSource->column_price_per_pax_index ? $row[$csvSource->column_price_per_pax_index] : null;
                $data[$n]['currency'] = $csvSource->column_currency_index ? $row[$csvSource->column_currency_index] : $csvSource->default_currency ? $csvSource->default_currency : null;
                $data[$n]['confirmed'] = $csvSource->column_confirmed_index && $csvSource->column_confirmed_value == $row[$csvSource->column_confirmed_index] ? true : false;
                $n++;
            }
        }

        fclose($fileHandle);
        unlink($fileName);

        return $data;
    }

    public function validateCruise(array $cruiseData)
    {
        $cruiseExist = (boolean) $this->cruiseRepository->where('boat_id', '=', $cruiseData['boat_id'])->where('start_date', '=', $cruiseData['start_date'])->where('end_date', '=', $cruiseData['end_date'])->count();
        $boat = $this->boatRepository->getById($cruiseData['boat_id']);
        $datesAvailable = (boolean) $this->cruiseRepository->checkIfDatesAvailable($boat, $cruiseData['start_date'], $cruiseData['end_date']);
        if (!$cruiseExist && !$datesAvailable) {
            $boat->csvImportAlerts()->updateOrCreate(['start_date' => $cruiseData['start_date'], 'end_date' => $cruiseData['end_date']], $cruiseData);
            return false;
        } elseif ($cruiseExist) {
            $cruise = $this->cruiseRepository->where('boat_id', '=', $cruiseData['boat_id'])->where('start_date', '=', $cruiseData['start_date'])->where('end_date', '=', $cruiseData['end_date'])->first();

            $startPlace = $cruiseData['start_place'] == $cruise->start_place;
            $endPlace = $cruiseData['end_place'] == $cruise->end_place;
            $itinerary = $cruiseData['itinerary'] == ($cruise->itinerary ? $cruise->itinerary->name : null);

            if (!$startPlace || !$endPlace || !$itinerary) {
                $cruiseData['cruise_id'] = $cruise->id;
                $boat->csvImportAlerts()->updateOrCreate(['start_date' => $cruiseData['start_date'], 'end_date' => $cruiseData['end_date']], $cruiseData);
                return false;
            }
        }
        return true;
    }

}