<?php
/**
 * Created by PhpStorm.
 * User: Wojtek
 * Date: 28.03.2017
 * Time: 07:28
 */

namespace App\Services;


use App\Models\Reservation;
use App\Repositories\CruiseRepository;
use App\Repositories\ReservationRepository;

class ReservationService
{
    private $reservationRepository;
    private $cruiseRepository;

    public function __construct(ReservationRepository $reservationRepository, CruiseRepository $cruiseRepository)
    {
        $this->reservationRepository = $reservationRepository;
        $this->cruiseRepository = $cruiseRepository;
    }

    public function create(array $data)
    {
        $cruise = $this->cruiseRepository->getById($data['cruise_id']);
        $data['currency'] = $cruise->currency;
        foreach ($data['passengers_data_arr'] as $key => $passenger) {
            if (isset($passenger['cabin_id'])) {
                $data['passengers_data_arr'][$key]['price'] = $cruise->cruiseCabinOccupancies()->where('cabin_id', $passenger['cabin_id'])->first()->price;
            }
        }
        foreach ($cruise->cruiseSurcharges as $cruiseSurcharge) {
            $data['surcharges_data_arr'][] = [
                'name' => $cruiseSurcharge->surcharge->name,
                'amount' => $cruiseSurcharge->amount,
                'currency' => $cruiseSurcharge->currency,
                'payment' => $cruiseSurcharge->payment,
            ];
        }
        $reservation = $this->reservationRepository->create($data);

        $cruise->free_pax -= count($data['passengers_data_arr']);
        $cruise->save();
//        if (!$cruise->occupancy_per_cruise) {
//            foreach ($data['passengers_data_arr'] as $passenger) {
//                $cabinOccupancy = $cruise->cruiseCabinOccupancies()->where('cabin_id', $passenger['cabin_id'])->first();
//                $cabinOccupancy->occupancy++;
//                $cabinOccupancy->save();
//            }
//        }
        return $reservation;
    }

    public function update(Reservation $reservation, array $data)
    {
        foreach ($data['passengers_data_arr'] as $key => $passenger) {
            if (isset($passenger['cabin_id'])) {
                $data['passengers_data_arr'][$key]['price'] = $reservation->cruise->cruiseCabinOccupancies()->where('cabin_id', $passenger['cabin_id'])->first()->price;
            }
        }
        $paxChange = count($reservation->passengers_data_arr) - count($data['passengers_data_arr']);
        $reservation = $this->reservationRepository->updateById($reservation->id, $data);
        $reservation->cruise->free_pax += $paxChange;
        $reservation->cruise->save();
        return $reservation;
    }

    public function delete(Reservation $reservation)
    {
        $pax = count($reservation->passengers_data_arr);
        $cruise = $reservation->cruise;
        $reservation->delete();
        $cruise->free_pax += $pax;
        $cruise->save();
    }

}