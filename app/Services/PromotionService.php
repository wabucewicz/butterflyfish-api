<?php

namespace App\Services;

use App\Models\Cruise;
use Carbon\Carbon;

class PromotionService
{
    public function checkCruisePromotions(Cruise $cruise)
    {
        $promotions = $cruise->cruisePromotions;
        if ($cruise->occupancy_per_cruise) {
            $lowestPrice = $cruise->price_per_pax;
            foreach ($promotions as $promotion) {
                if ($this->isPromotionActive($promotion->start_date, $promotion->end_date)) {
                    $discountPrice = $this->getPriceAfterDiscount($promotion->type, $cruise->price_per_pax, $promotion->rate);
                    $lowestPrice = $discountPrice < $lowestPrice ? $discountPrice : $lowestPrice;
                }
            }
            if ($lowestPrice != $cruise->price_per_pax && $lowestPrice > 0) {
                $cruise->promotion_price_per_pax = $lowestPrice;
                $cruise->promotion_active = true;
            } else {
                $cruise->promotion_active = false;
                $cruise->promotion_price_per_pax = null;
            }
            $cruise->save();
        } else {
            foreach ($cruise->cruiseCabinOccupancies as $cruiseCabinOccupancy) {
                $lowestPrice = $cruiseCabinOccupancy->price;
                foreach ($promotions as $promotion) {
                    if ($this->isPromotionActive($promotion->start_date, $promotion->end_date)) {
                        $discountPrice = $this->getPriceAfterDiscount($promotion->type, $cruiseCabinOccupancy->price, $promotion->rate);
                        $lowestPrice = $discountPrice < $lowestPrice ? $discountPrice : $lowestPrice;
                    }
                }
                if ($lowestPrice != $cruiseCabinOccupancy->price && $lowestPrice > 0) {
                    $cruise->promotion_active = true;
                    $cruiseCabinOccupancy->promotion_price = $lowestPrice;
                } else {
                    $cruise->promotion_active = false;
                    $cruiseCabinOccupancy->promotion_price = null;
                }
                $cruiseCabinOccupancy->save();
            }
        }
    }

    public function checkAllCruisesPromotions()
    {
        foreach (Cruise::all() as $cruise) {
            $this->checkCruisePromotions($cruise);
        }
    }

    private function isPromotionActive($startDate, $endDate)
    {
        return (bool) Carbon::parse($startDate)->lte(Carbon::now()) && Carbon::parse($endDate)->gte(Carbon::now());
    }

    private function getPriceAfterDiscount($discountType, $price, $discountRate)
    {
        switch ($discountType) {
            case 'amount':
                return $price - $discountRate;
            case 'percentage':
                return $price - round($price * $discountRate/100);
        }
    }
}