<?php
namespace App\Repositories;

use App\Models\Itinerary;
use App\Models\ItineraryKeyword;
use App\Models\BoatOperator;

class ItineraryRepository extends BaseRepository
{
    public function __construct(Itinerary $itinerary)
    {
        $this->model = $itinerary;
    }

    public function updateKeywords(Itinerary $itinerary, array $keywords)
    {
        $itinerary->keywords()->detach();
        foreach ($keywords as $keywordString) {
            $keyword = ItineraryKeyword::where('name', $keywordString)->first();
            if (!$keyword) {
                $keyword = ItineraryKeyword::create(['name' => $keywordString]);
            }
            $itinerary->keywords()->attach($keyword);
        }
    }

    public function allForOperator($operator)
    {
        if ($operator instanceof BoatOperator) {
            return $operator->itineraries;
        } else {
            return [];
        }
    }

    public function updateOperator(Itinerary $itinerary, $operator)
    {
        if ($operator instanceof BoatOperator) {
            $itinerary->boatOerator()->associate($operator);
            $itinerary->save();
        }
        return $itinerary;
    }

    public function getEmbarkations()
    {
        $data = Itinerary::select(['embarkation'])->orderBy('embarkation','asc')->groupBy('embarkation')->get();
        return $data->pluck('embarkation');
    }

    public function getDisembarkations()
    {
        $data = Itinerary::select(['disembarkation'])->orderBy('disembarkation','asc')->groupBy('embarkation')->get();
        return $data->pluck('disembarkation');
    }
}