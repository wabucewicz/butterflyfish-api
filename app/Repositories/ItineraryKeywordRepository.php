<?php
namespace App\Repositories;

use App\Models\ItineraryKeyword;
use Vinkla\Hashids\Facades\Hashids;

class ItineraryKeywordRepository extends BaseRepository
{
    public function __construct(ItineraryKeyword $itineraryKeyword)
    {
        $this->model = $itineraryKeyword;
    }
}