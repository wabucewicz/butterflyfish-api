<?php
namespace App\Repositories;

use App\Models\CruiseOperator;

class CruiseOperatorRepository extends BaseRepository
{
    public function __construct(CruiseOperator $operator)
    {
        $this->model = $operator;
    }
}