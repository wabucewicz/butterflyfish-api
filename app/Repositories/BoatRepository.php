<?php
namespace App\Repositories;

use App\Models\Agent;
use App\Models\Boat;
use App\Models\BoatOperator;
use App\Models\CruiseOperator;
use App\Utils\ID;

class BoatRepository extends BaseRepository
{

    public function __construct(Boat $boat)
    {
        $this->model = $boat;
    }

    public function updateById($id, array $data)
    {
        $boat = $this->getById($id);

        if ($boat->cabins()->count()) {
            $maxQuests = 0;
            foreach ($boat->cabins as $cabin) {
                $maxQuests += $cabin->max_pax;
            }
        } else {
            $maxQuests = $data['max_quests'];
        }

        if (isset($data['number_of_crew_members'])) {
            $crewMembers = $data['number_of_crew_members'];
        } else {
            $crewMembers = 0;
        }
        $data['max_quests'] = $maxQuests;
        $data['quantities_pax'] = $crewMembers + $maxQuests;

        $boat->update($data);
        return $boat;
    }

    // OPERATORS

    public function allForBoatOperator(BoatOperator $boatOperator)
    {
        return $boatOperator->boats;
    }

    public function allForCruiseOperator(CruiseOperator $cruiseOperator)
    {
        $boats = collect();
        foreach ($cruiseOperator->cruises as $cruise) {
            $boats->push($cruise->boat);
        }
        return $boats->unique();
    }

    public function syncBoatOperators(Boat $boat, array $data)
    {
        $boat->boatOperators()->sync($data);
    }

    // AGENTS

    public function allForAgent(Agent $agent)
    {
        $agentId = $agent->id;
        return Boat::where(function ($query) use ($agentId) {
            $query->whereHas('boatOperators', function ($query) use ($agentId) {
                $query->whereHas('collaborations', function ($query) use ($agentId) {
                    $query->whereHas('agent', function ($query) use ($agentId) {
                        $query->where('id', $agentId);
                    });
                });
            })->orWhereHas('cruises', function ($query) use ($agentId) {
                $query->whereHas('cruiseOperator', function ($query) use ($agentId) {
                    $query->whereHas('collaborations', function ($query) use ($agentId) {
                        $query->whereHas('agent', function ($query) use ($agentId) {
                            $query->where('id', $agentId);
                        });
                    });
                });
            });
        })->get();
    }


    // DECKS

    public function getByIdDeck(Boat $boat, $id)
    {
        return $boat->decks()->findOrFail($id);
    }

    public function allDeck(Boat $boat)
    {
        return $boat->decks()->get();
    }

    public function storeDeck(Boat $boat, array $data)
    {
        return $boat->decks()->create($data);
    }

    public function updateDeck(Boat $boat, $id, array $data)
    {
        $deck = $boat->decks()->findOrFail($id);
        $deck->update($data);
        return $deck;
    }

    public function destroyDeck(Boat $boat, $id)
    {
        return $boat->decks()->findOrFail($id)->delete();
    }


    // CABINS

    public function getByIdCabin(Boat $boat, $id)
    {
        return $boat->cabins()->findOrFail($id);
    }

    public function allCabin(Boat $boat)
    {
        return $boat->cabins()->get();
    }

    public function storeCabin(Boat $boat, array $data)
    {
        $data['max_pax'] = $data['sng_beds'] + $data['dbl_beds'] * 2;
        $cabin = $boat->cabins()->create($data);
        if (isset($data['hash_deck_id']) && $deck = $this->getByIdDeck($boat, ID::decode($data['hash_deck_id']))) {
            $cabin->deck()->associate($deck);
            $cabin->save();
        }
        $maxQuests = 0;
        foreach ($boat->cabins as $cabin) {
            $maxQuests += $cabin->max_pax;
        }
        $boat->max_quests = $maxQuests;
        $boat->save();
        return $cabin;
    }

    public function updateCabin(Boat $boat, $id, array $data)
    {
        $cabin = $boat->cabins()->findOrFail($id);
        $data['max_pax'] = $data['sng_beds'] + $data['dbl_beds'] * 2;
        if (isset($data['hash_deck_id']) && $deck = $this->getByIdDeck($boat, ID::decode($data['hash_deck_id']))) {
            $cabin->deck()->associate($deck);
            $cabin->save();
        }
        $cabin->update($data);
        $maxQuests = 0;
        foreach ($boat->cabins as $cabin) {
            $maxQuests += $cabin->max_pax;
        }
        $boat->max_quests = $maxQuests;
        $boat->save();
        return $cabin;
    }

    public function destroyCabin(Boat $boat, $id)
    {
        return $boat->cabins()->findOrFail($id)->delete();
    }
}