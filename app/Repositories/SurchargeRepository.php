<?php
namespace App\Repositories;

use App\Models\Surcharge;

class SurchargeRepository extends BaseRepository
{
    public function __construct(Surcharge $surcharge)
    {
        $this->model = $surcharge;
    }
}