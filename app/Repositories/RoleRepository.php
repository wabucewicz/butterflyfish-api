<?php
namespace App\Repositories;

use App\Models\Permission;
use App\Models\Role;
use Hashids;

class RoleRepository extends BaseRepository
{
    public function __construct(Role $role)
    {
        $this->model = $role;
    }
}