<?php
namespace App\Repositories;

use App\Models\CsvSource;
use Vinkla\Hashids\Facades\Hashids;

class CsvSourceRepository extends BaseRepository
{
    public function __construct(CsvSource $csvSource)
    {
        $this->model = $csvSource;
    }
}