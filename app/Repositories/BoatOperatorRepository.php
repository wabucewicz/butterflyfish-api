<?php
namespace App\Repositories;

use App\Models\BoatOperator;

class BoatOperatorRepository extends BaseRepository
{
    public function __construct(BoatOperator $operator)
    {
        $this->model = $operator;
    }
}