<?php
namespace App\Repositories;

use App\Models\Agent;
use App\Models\BoatOperator;
use App\Models\Collaboration;
use App\Models\CruiseOperator;

class CollaborationRepository extends BaseRepository
{
    public function __construct(Collaboration $collaboration)
    {
        $this->model = $collaboration;
    }

    public function createFromBoatOperatorToAgent(BoatOperator $boatOperator, Agent $agent)
    {
        return $boatOperator->collaborations()->create([
            'agent_id' => $agent->id,
            'operator_approved' => true
        ]);
    }

    public function createFromCruiseOperatorToAgent(CruiseOperator $cruiseOperator, Agent $agent)
    {
        return $cruiseOperator->collaborations()->create([
            'agent_id' => $agent->id,
            'operator_approved' => true
        ]);
    }

    public function createFromAgentToBoatOperator(Agent $agent, BoatOperator $boatOperator)
    {
        return $boatOperator->collaborations()->create([
            'agent_id' => $agent->id,
            'agent_approved' => true
        ]);
    }

    public function createFromAgentToCruiseOperator(Agent $agent, CruiseOperator $cruiseOperator)
    {
        return $cruiseOperator->collaborations()->create([
            'agent_id' => $agent->id,
            'agent_approved' => true
        ]);
    }

    public function approveAsOperator(Collaboration $collaboration)
    {
        $collaboration->update([
            'operator_approved' => true
        ]);
        return $collaboration;
    }

    public function approveAsAgent(Collaboration $collaboration)
    {
        $collaboration->update([
            'agent_approved' => true
        ]);
        return $collaboration;
    }

    public function allForBoatOperator(BoatOperator $boatOperator)
    {
        return $boatOperator->collaborations;
    }

    public function allForCruiseOperator(CruiseOperator $cruiseOperator)
    {
        return $cruiseOperator->collaborations;
    }

    public function allForAgent(Agent $agent)
    {
        return $agent->collaborations;
    }


}