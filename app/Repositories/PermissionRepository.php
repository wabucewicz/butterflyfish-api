<?php
namespace App\Repositories;

use App\Models\Permission;
use App\Models\Role;
use Hashids;

class PermissionRepository extends BaseRepository
{
    public function __construct(Permission $permission)
    {
        $this->model = $permission;
    }
}