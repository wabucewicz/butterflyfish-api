<?php
namespace App\Repositories;

use App\Models\Agent;
use App\Models\Boat;
use App\Models\Cruise;
use App\Models\CruiseCabinOccupancy;
use App\Models\CruiseOperator;
use App\Models\CruiseSurcharge;
use App\Models\Surcharge;
use App\Utils\ID;
use Carbon\Carbon;

class CruiseRepository extends BaseRepository
{
    public function __construct(Cruise $cruise)
    {
        $this->model = $cruise;
    }

    public function allByBoat(Boat $boat)
    {
        if (request()->input('with')) {
            return $boat->cruises()->with(explode(',', request()->input('with')))->get();
        }
        return $boat->cruises()->get();
    }

    public function allByBoatForCruiseOperator(Boat $boat, CruiseOperator $cruiseOperator)
    {
        if (request()->input('with')) {
            return $boat->cruises()->where('cruise_operator_id', $cruiseOperator->id)->with(explode(',', request()->input('with')))->get();
        }
        return $boat->cruises()->where('cruise_operator_id', $cruiseOperator->id)->get();
    }

    public function allByBoatForAgent(Boat $boat, Agent $agent)
    {
        $boatId = $boat->id;
        $agentId = $agent->id;
        return Cruise::where(function ($query) use ($boatId, $agentId) {
            $query->whereHas('boat', function ($query) use ($boatId, $agentId) {
                $query->where('id', $boatId);
                $query->whereHas('boatOperators', function ($query) use ($agentId) {
                    $query->whereHas('collaborations', function ($query) use ($agentId) {
                        $query->whereHas('agent', function ($query) use ($agentId) {
                            $query->where('id', $agentId);
                        });
                    });
                });
            });
        })->orWhere(function ($query) use ($boatId, $agentId) {
            $query->whereHas('boat', function ($query) use ($boatId, $agentId) {
                $query->where('id', $boatId);
            });
            $query->whereHas('cruiseOperator', function ($query) use ($agentId) {
                $query->whereHas('collaborations', function ($query) use ($agentId) {
                    $query->whereHas('agent', function ($query) use ($agentId) {
                        $query->where('id', $agentId);
                    });
                });
            });
        })->get();
    }

    public function getByBoat(Boat $boat, $id)
    {
        if (request()->input('with')) {
            return $boat->cruises()->with(explode(',', request()->input('with')))->findOrFail($id);
        }
        return $boat->cruises()->findOrFail($id);
    }

    public function createByBoat(Boat $boat, array $data)
    {
        $cruise = $boat->cruises()->create($data);
        $cruise->currency = 'USD';
        $cruise->save();
        return $cruise;
    }

    public function updateByBoat(Boat $boat, int $id, array $data)
    {
        $cruise = $boat->cruises()->findOrFail($id);
        $cruise->update($data);
        return $cruise;
    }

    public function deleteByBoat(Boat $boat, int $id)
    {
        return $boat->cruises()->findOrFail($id)->delete();
    }

    public function getCruisesByBoatFromDateRange(Boat $boat, string $dateFrom, string $dateTo)
    {
        return $boat->cruises()->where('start_date', '>=', $dateFrom)->where('end_date', '<=', $dateTo)->get();
    }

    public function checkIfDatesAvailable(Boat $boat, string $start, string $end, int $excludeCruiseId = null)
    {
        $count = $boat->cruises();
        if ($excludeCruiseId != null) {
            $count->where('id', '<>', $excludeCruiseId);
        }
        $count->where(function ($query) use ($start, $end) {
            $query->where(function ($query) use ($start, $end) {
                $query->where('start_date', '>', $start)
                    ->where('start_date', '<', $end);
            })
                ->orWhere(function ($query) use ($start, $end) {
                    $query->where('end_date', '>', $start)
                        ->where('end_date', '<', $end);
                })
                ->orWhere(function ($query) use ($start, $end) {
                    $query->where('start_date', '<=', $start)
                        ->where('end_date', '>=', $end);
                });
        });
        return !boolval($count->count());
    }

    public function setDates(Cruise $cruise, $startDate, $endDate)
    {
        $cruise->start_date = $startDate;
        $cruise->end_date = $endDate;
        return $cruise;
    }

    public function setPlaces(Cruise $cruise, $startPlace, $endPlace)
    {
        $cruise->start_place = $startPlace;
        $cruise->end_place = $endPlace;
        return $cruise;
    }

    public function setConfirmation(Cruise $cruise, $value)
    {
        $cruise->confirmed = $value;
        return $cruise;
    }

    public function setItinerary(Cruise $cruise, $itineraryId)
    {
        if (empty($itineraryId)) {
            $cruise->itinerary()->dissociate();
        } else {
            $cruise->itinerary()->associate($itineraryId);
        }
        return $cruise;
    }

    public function setCurrency(Cruise $cruise, $currency)
    {
        $cruise->currency = $currency;
        return $cruise;
    }

    public function setOccupancyPerCruisePax(Cruise $cruise, int $freePax)
    {
        $cruise->occupancy_per_cruise = true;
        $cruise->free_pax = !empty($freePax) ? $freePax : 0;
        return $cruise;
    }

    public function setOccupancyPerCruisePrice(Cruise $cruise, int $pricePerPax)
    {
        $cruise->occupancy_per_cruise = true;
        $cruise->price_per_pax = !empty($pricePerPax) ? $pricePerPax : 0;
        return $cruise;
    }

    public function setOccupancyPerCabinPax(Cruise $cruise, array $cabinsFreePax)
    {
        if ($cruise->boat->cabins()->count()) {
            $cruise->occupancy_per_cruise = false;
            $availablePax = 0;
            foreach ($cruise->boat->cabins as $cabin) {
                CruiseCabinOccupancy::updateOrCreate([
                    'cruise_id' => $cruise->id,
                    'cabin_id' => $cabin->id
                ],
                [
                    'cruise_id' => $cruise->id,
                    'cabin_id' => $cabin->id,
                    'occupancy' => $cabin->max_pax - (isset($cabinsFreePax[$cabin->hash_id]) ? $cabinsFreePax[$cabin->hash_id] : 0)
                ]);
                $availablePax += isset($cabinsFreePax[$cabin->hash_id]) ? $cabinsFreePax[$cabin->hash_id] : 0;
            }
            $cruise->free_pax = $availablePax;
            $cruise->price_per_pax = 0;
        }
        return $cruise;
    }

    public function setOccupancyPerCabinPrice(Cruise $cruise, array $cabinsPricePerPax)
    {
        if ($cruise->boat->cabins()->count()) {
            $cruise->occupancy_per_cruise = false;
            $availablePax = 0;
            foreach ($cruise->boat->cabins as $cabin) {
                CruiseCabinOccupancy::updateOrCreate([
                    'cruise_id' => $cruise->id,
                    'cabin_id' => $cabin->id
                ],
                [
                    'cruise_id' => $cruise->id,
                    'cabin_id' => $cabin->id,
                    'price' => isset($cabinsPricePerPax[$cabin->hash_id]) ? $cabinsPricePerPax[$cabin->hash_id] : 0
                ]);
            }
            $cruise->price_per_pax = 0;
        }
        return $cruise;
    }

    public function setSurcharges(Cruise $cruise, array $surcharges)
    {
        $cruise->cruiseSurcharges()->delete();
        if (isset($surcharges) && count($surcharges)) {
            foreach ($surcharges as $surchargeData) {
                $surcharge = new CruiseSurcharge($surchargeData);
                $surcharge->surcharge()->associate(Surcharge::find(ID::decode($surchargeData['hash_surcharge_id'])));
                $cruise->cruiseSurcharges()->save($surcharge);
            }
        }
        return $cruise;
    }

    public function setPromotions(Cruise $cruise, array $promotions)
    {
        $cruise->cruisePromotions()->delete();
        if (isset($promotions) && count($promotions)) {
            foreach ($promotions as $promotionData) {
                $cruise->cruisePromotions()->create($promotionData);
            }
        }
        return $cruise;
    }

    public function addPromotion(Cruise $cruise, $data)
    {
        $cruise->cruisePromotions()->create($data);
        return $cruise;
    }

    public function assignToCruiseOperator(Cruise $cruise, CruiseOperator $cruiseOperator)
    {
        $cruise->cruiseOperator()->associate($cruiseOperator);
        $cruise->save();
        return $cruise;
    }

    public function unassignFromCruiseOperator(Cruise $cruise)
    {
        $cruise->cruiseOperator()->dissociate();
        $cruise->save();
        return $cruise;
    }


}