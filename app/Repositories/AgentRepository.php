<?php
namespace App\Repositories;

use App\Models\Agent;

class AgentRepository extends BaseRepository
{
    public function __construct(Agent $agent)
    {
        $this->model = $agent;
    }


}