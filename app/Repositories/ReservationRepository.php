<?php
namespace App\Repositories;

use App\Models\Agent;
use App\Models\BoatOperator;
use App\Models\CruiseOperator;
use App\Models\Reservation;
use App\Models\User;

class ReservationRepository extends BaseRepository
{
    public function __construct(Reservation $reservation)
    {
        $this->model = $reservation;
    }

    public function allForUser(User $user)
    {
        $this->newQuery()->eagerLoad();
        if ($user->agent instanceof Agent) {
            $userId = $user->id;
//            return $user->agent->reservations;
            return $this->query->whereHas('agent', function ($query) use ($userId) {
                $query->whereHas('users', function ($query) use ($userId) {
                    $query->where('id', $userId);
                });
            })->get();
        }
        if ($user->boatOperator instanceof BoatOperator) {
            $boatOperatorId = $user->boatOperator->id;
            return $this->query->whereHas('cruise', function ($query) use ($boatOperatorId) {
                $query->whereHas('boat', function ($query) use ($boatOperatorId) {
                    $query->whereHas('boatOperators', function ($query) use ($boatOperatorId) {
                        $query->where('id', $boatOperatorId);
                    });
                });
            })->get();
        }
        if ($user->cruiseOperator instanceof CruiseOperator) {
            $cruiseOperatorId = $user->cruiseOperator->id;
            return $this->query->whereHas('cruise', function ($query) use ($cruiseOperatorId) {
                $query->whereHas('cruiseOperator', function ($query) use ($cruiseOperatorId) {
                    $query->where('id', $cruiseOperatorId);
                });
            })->get();
        }
    }


}