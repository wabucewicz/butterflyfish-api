<?php
namespace App\Repositories;

use App\Models\Agent;
use App\Models\BoatOperator;
use App\Models\CruiseOperator;
use App\Models\Role;
use App\Models\User;

class UserRepository extends BaseRepository
{
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function updateBoatOperator(User $user, $operatorId)
    {
        $user->boatOperator()->associate(BoatOperator::find($operatorId));
        $user->save();
    }

    public function updateCruiseOperator(User $user, $operatorId)
    {
        $user->cruiseOperator()->associate(CruiseOperator::find($operatorId));
        $user->save();
    }

    public function updateAgent(User $user, $agentId)
    {
        $user->agent()->associate(Agent::find($agentId));
        $user->save();
    }

    public function updateRole(User $user, $roleId)
    {
        $user->role()->associate(Role::find($roleId));
        $user->save();
    }

}