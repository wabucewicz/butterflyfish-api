<?php
namespace App\Repositories;

use App\Models\Photo;
use File;
use stdClass;
use Vinkla\Hashids\Facades\Hashids;
use Image;

class PhotoRepository extends BaseRepository
{


    /**
     * @param $model
     * @return string
     */
    public function getModelPhotos($model)
    {
        $files = [];
        foreach ($model->photos()->orderBy('order', 'asc')->get() as $photo) {
            $success = new stdClass();
            $success->name = $photo->filename . '.jpg';
            $success->size = $photo->file_size;
            $success->url = asset($photo->path . $photo->filename . '.jpg');
            $success->thumbnailUrl = asset($photo->path . $photo->filename . '_thumb.jpg');
            $success->deleteUrl = action('Admin\PhotoController@deletePhoto', $photo->hashId);
            $success->deleteType = 'DELETE';
            $success->fileID = $photo->hashId;
            $files[] = $success;
        }
        return json_encode(['files'=> $files]);
    }

    /**
     * @param $data
     * @return stdClass
     * @internal param $image
     */
    public function store(array $data)
    {
        $model = $data['model'];
        $inputPhoto = $data['photo'];

        $filename  = str_slug($model->name) . '-butterflyfish-' . substr(str_shuffle(md5(time())),0,8);
        $path = $model->photosPath();

        if(!File::exists($path)) {
            File::makeDirectory($path, 0775, true);
        }

        $photoFile = Image::make($inputPhoto->getRealPath())
            ->resize(null, 600, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->insert(public_path(Photo::WATERMARK_PATH), 'bottom-right', 30, 30)
            ->save(public_path($path . $filename . '.jpg'));

        Image::make($inputPhoto->getRealPath())
            ->fit(350, 200)
            ->save(public_path($path . $filename . '_thumb.jpg'));

        $photoModel = new Photo([
            'filename' => $filename,
            'path' => $path,
            'file_size' => $photoFile->filesize(),
            'order' => 0
        ]);

        $model->photos()->save($photoModel);

        $success = new stdClass();
        $success->name = $filename . '.jpg';
        $success->size = $photoFile->filesize();
        $success->url = asset($path . $filename . '.jpg');
        $success->thumbnailUrl = asset($path . $filename . '_thumb.jpg');;
        $success->deleteUrl = route('admin.photos-delete', $photoModel->hashId);
        $success->deleteType = 'DELETE';
        $success->fileID = $photoModel->hashId;

        return $success;
    }

    /**
     * @param $hashId
     * @param $data
     */
    public function update($hashId, $data)
    {

    }

    /**
     * @param $hashId
     */
    public function destroy($hashId)
    {
        $photo = $this->find($hashId);
        File::delete($photo->path . $photo->filename);
        $photo->delete();
    }

    /**
     * @param $data
     */
    public function updateOrder($data)
    {
        foreach ($data['photo'] as $order => $photoId) {
            $this->find($photoId)->update([
                'order' => $order
            ]);
        }
    }
}