<?php
/**
 * Created by PhpStorm.
 * User: Wojtek
 * Date: 18.01.2017
 * Time: 09:16
 */

namespace App\Repositories;


use App\Models\CsvImportAlert;

class CsvImportAlertRepository extends BaseRepository
{
    public function __construct(CsvImportAlert $csvImportAlert)
    {
        $this->model = $csvImportAlert;
    }
}