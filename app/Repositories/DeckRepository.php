<?php
namespace App\Repositories;

use App\Models\Deck;
use Vinkla\Hashids\Facades\Hashids;

class DeckRepository extends BaseRepository
{
    public function __construct(Deck $deck)
    {
        $this->model = $deck;
    }


}