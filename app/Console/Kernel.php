<?php

namespace App\Console;

use App\Console\Commands\ImportFromCsvSources;
use App\Console\Commands\RebuildPermissionsAndRoles;
use App\Services\PromotionService;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ImportFromCsvSources::class,
        RebuildPermissionsAndRoles::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('import:csv')->daily()->at(env('CRON_IMPORT_CSV', '08:00'));

        $schedule->command('backup:run --only-db')->daily()->at(env('CRON_BACKUP', '08:00'));

        $schedule->call(function (PromotionService $promotionService) {
            $promotionService->checkAllCruisesPromotions();
        })->daily()->at(env('CRON_CHECK_PROMOTIONS', '00:00'));

    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
