<?php

namespace App\Console\Commands;

use Artisan;
use Illuminate\Console\Command;

class RebuildPermissionsAndRoles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'perms_roles_rebuild';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Permissions and roles rebuild';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Permissions and roles rebuild in database');
        $bar = $this->output->createProgressBar(2);
        Artisan::call('db:seed', [
            '--class'     => 'PermissionsTableSeeder'
        ]);
        $bar->advance();
        $this->info(' Permissions seeded');
        Artisan::call('db:seed', [
            '--class'     => 'RolesTableSeeder'
        ]);
        $bar->advance();
        $bar->finish();
        $this->info(' Roles seeded');
    }
}
