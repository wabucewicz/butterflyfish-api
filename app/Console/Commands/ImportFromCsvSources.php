<?php

namespace App\Console\Commands;

use App\Repositories\CsvSourceRepository;
use App\Services\CsvService;
use App\Utils\ID;
use Illuminate\Console\Command;

class ImportFromCsvSources extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:csv {--source=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import cruises data from CSV sources';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param CsvSourceRepository $csvSourceRepository
     * @param CsvService $csvService
     * @return mixed
     */
    public function handle(CsvSourceRepository $csvSourceRepository, CsvService $csvService)
    {
        $sourceId = $this->option('source');

        if (empty($sourceId)) {
            $this->line('Import from all CSV sources');
            $csvSources = $csvSourceRepository->all();
            foreach ($csvSources as $csvSource) {
                $this->line($csvSource->name . ' (' . $csvSource->hash_id . ') ');
                $cruises = $csvService->getData($csvSource);
                if ($cruises) {
                    $bar = $this->output->createProgressBar(count($cruises));
                    foreach ($cruises as $cruiseData) {
                        if ($csvService->validateCruise($cruiseData)) {
                            $csvService->saveCruise($cruiseData);
                        }
                        $bar->advance();
                    }
                    $bar->finish();
                    $this->info(' Done');
                } else {
                    $this->error('Unable to download and get data!');
                }
            }
        } else {
            $this->line('Import from selected CSV source');
            $csvSource = $csvSourceRepository->getById(ID::decode($sourceId));
            if (empty($csvSource)) {
                $this->error('CSV source ' . $sourceId . ' not found!');
                return;
            }
            $this->line($csvSource->name . ' (' . $csvSource->hash_id . ') ');
            $cruises = $csvService->getData($csvSource);
            if ($cruises) {
                $bar = $this->output->createProgressBar(count($cruises));
                foreach ($cruises as $cruiseData) {
                    if ($csvService->validateCruise($cruiseData)) {
                        $csvService->saveCruise($cruiseData);
                    }
                    $bar->advance();
                }
                $bar->finish();
                $this->info(' Done');
            } else {
                $this->error('Unable to download and get data!');
            }
        }
        $this->info('Operation successful!');
    }
}
