<?php

namespace App\Http\Controllers;

use App\Models\Boat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WidgetController extends Controller
{
    public function show($boatHashCode)
    {
        $boat = Boat::with('cruises.itinerary')->where('hash_code', $boatHashCode)->first();
        return view('widget', ['boat' => $boat]);
    }
}
