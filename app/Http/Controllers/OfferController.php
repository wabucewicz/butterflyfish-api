<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\GetOffersRequest;
use App\Models\Agent;
use App\Models\Cruise;
use App\Services\OfferService;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    private $offerService;

    public function __construct(OfferService $offerService)
    {
        $this->offerService = $offerService;
    }

    public function getOffers(GetOffersRequest $request)
    {
        $data = $request->all();
        if (!empty($data['query']) && !empty($data['date_from']) && !empty($data['date_to']) && !empty($data['passengers'])) {
            $cruises = Cruise::where(function ($query) use ($data) {
                    $query->whereHas('itinerary', function ($query) use ($data) {
                        $query->where('name', 'like', '%' . $data['query'] . '%');
                        $query->orWhereHas('keywords', function ($query) use ($data) {
                            $query->where('name', 'like', '%' . $data['query'] . '%');
                        });
                    })
                    ->orWhereHas('boat', function ($query) use ($data) {
                        $query->where('name', 'like', '%' . $data['query'] . '%');
                    })
                    ->orWhere('start_place', 'like', '%' . $data['query'] . '%')
                    ->orWhere('end_place', 'like', '%' . $data['query'] . '%');
                })
                ->where('free_pax', '>=', $data['passengers'])
                ->where('start_date', '>=', $data['date_from'])
                ->where('end_date', '<=', $data['date_to'])
                ->with(['boat'=> function ($query) {
                    $query->select('name', 'id');
                    $query->with(['photos' => function ($query) {
                        $query->first();
                    }]);
                }])
                ->get();
            foreach ($cruises as $key => $cruise) {
                if ($cruise->free_pax != null && $cruise->free_pax >= 5) {
                    $cruises[$key]->free_pax = 5;
                }
            }
            return $this->sendResponse($cruises, 'Offers retrieved successfully');
        }
        return $this->sendError('Invalid request');
    }

    public function getOffersByHashCode(GetOffersRequest $request, $hashCode)
    {
        $data = $request->all();
        if (!empty($data['query']) && !empty($data['date_from']) && !empty($data['date_to']) && !empty($data['passengers'])) {
            $hashCodeData = explode('-', $hashCode);
            switch ($hashCodeData[0]) {
                case '0': // Boat Operator
                    $cruises = Cruise::whereHas('boat', function ($query) use ($hashCodeData) {
                        $query->whereHas('boatOperators', function ($query) use ($hashCodeData) {
                            $query->where('hash_code', $hashCodeData[1]);
                        });
                    });
                    break;
                case '1': // Cruise Operator
                    $cruises = Cruise::whereHas('cruiseOperator', function ($query) use ($hashCodeData) {
                        $query->where('hash_code', $hashCodeData[1]);
                    });
                    break;
                case 2: // Agent
                    $cruises = Cruise::where(function ($query) use ($hashCodeData) {
                        $query->whereHas('boat', function ($query) use ($hashCodeData) {
                            $query->whereHas('boatOperators', function ($query) use ($hashCodeData) {
                                $query->whereHas('collaborations', function ($query) use ($hashCodeData) {
                                    $query->whereHas('agent', function ($query) use ($hashCodeData) {
                                        $query->where('hash_code', $hashCodeData[1]);
                                    });
                                });
                            });
                        })->orWhereHas('cruiseOperator', function ($query) use ($hashCodeData) {
                            $query->whereHas('collaborations', function ($query) use ($hashCodeData) {
                                $query->whereHas('agent', function ($query) use ($hashCodeData) {
                                    $query->where('hash_code', $hashCodeData[1]);
                                });
                            });
                        });
                    });
                    break;
                default:
                    abort(404);
                    die;
            }
            $cruises = $cruises->where(function ($query) use ($data) {
                    $query->whereHas('itinerary', function ($query) use ($data) {
                        $query->where('name', 'like', '%' . $data['query'] . '%');
                        $query->orWhereHas('keywords', function ($query) use ($data) {
                            $query->where('name', 'like', '%' . $data['query'] . '%');
                        });
                    })
                    ->orWhereHas('boat', function ($query) use ($data) {
                        $query->where('name', 'like', '%' . $data['query'] . '%');
                    })
                    ->orWhere('start_place', 'like', '%' . $data['query'] . '%')
                    ->orWhere('end_place', 'like', '%' . $data['query'] . '%');
                })
                ->where('free_pax', '>=', $data['passengers'])
                ->where('start_date', '>=', $data['date_from'])
                ->where('end_date', '<=', $data['date_to'])
                ->with(['boat'=> function ($query) {
                    $query->select('name', 'id');
//                    $query->with(['photos' => function ($query) {
//                        $query->take(1);
//                    }]);
                }])
                ->get();
            foreach ($cruises as $key => $cruise) {
                if ($cruise->free_pax != null && $cruise->free_pax >= 5) {
                    $cruises[$key]->free_pax = 5;
                }
            }
            return $this->sendResponse($cruises, 'Offers retrieved successfully');
        }
        return $this->sendError('Invalid request');
    }

}
