<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\CabinRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CabinController extends Controller
{
    private $cabinRepository;

    public function __construct(CabinRepository $cabinRepository)
    {
        $this->cabinRepository = $cabinRepository;
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $hashId)
    {
        //
    }

    public function destroy($hashId)
    {
        //
    }
}
