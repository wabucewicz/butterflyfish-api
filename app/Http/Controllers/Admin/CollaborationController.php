<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CollaborationRequest;
use App\Models\Agent;
use App\Models\BoatOperator;
use App\Models\Collaboration;
use App\Models\CruiseOperator;
use App\Repositories\AgentRepository;
use App\Repositories\BoatOperatorRepository;
use App\Repositories\CollaborationRepository;
use App\Repositories\CruiseOperatorRepository;
use App\Utils\ID;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CollaborationController extends Controller
{
    private $collaborationRepository;
    private $agentRepository;
    private $boatOperatorRepository;
    private $cruiseOperatorRepository;

    public function __construct(CollaborationRepository $collaborationRepository, AgentRepository $agentRepository, BoatOperatorRepository $boatOperatorRepository, CruiseOperatorRepository $cruiseOperatorRepository)
    {
        $this->collaborationRepository = $collaborationRepository;
        $this->agentRepository = $agentRepository;
        $this->boatOperatorRepository = $boatOperatorRepository;
        $this->cruiseOperatorRepository = $cruiseOperatorRepository;
    }

    public function index()
    {
        $user = Auth::user();
        if ($user->can('listAll', Collaboration::class)) {
            return $this->sendResponse($this->collaborationRepository->all(), 'Collaborations retrieved successfully');
        } elseif ($user->can('listAssociated', Collaboration::class) && $user->boatOperator instanceof BoatOperator) {
            return $this->sendResponse($this->collaborationRepository->allForBoatOperator($user->boatOperator), 'Collaborations retrieved successfully');
        } elseif ($user->can('listAssociated', Collaboration::class) && $user->cruiseOperator instanceof CruiseOperator) {
            return $this->sendResponse($this->collaborationRepository->allForCruiseOperator($user->cruiseOperator), 'Collaborations retrieved successfully');
        } elseif ($user->can('listAssociated', Collaboration::class) && $user->agent instanceof Agent) {
            return $this->sendResponse($this->collaborationRepository->allForAgent($user->agent), 'Collaborations retrieved successfully');
        }
        return abort(403, 'Unauthorized');
    }

    public function show($hashId)
    {
        $collaboration = $this->collaborationRepository->getById(ID::decode($hashId));
        $this->authorize('view', $collaboration);
        return $this->sendResponse($collaboration, 'Collaboration retrieved successfully');
    }

    public function store(CollaborationRequest $request)
    {
        $data = $request->all();
        $user = Auth::user();
        if ($this->collaborationRepository->where('operator_type', $data['operator_type'])->where('operator_id', ID::decode($data['hash_operator_id']))->where('agent_id', ID::decode($data['hash_agent_id']))->count()) {
            return $this->sendError('Collaboration already exists');
        }
        if ($user->can('create', Collaboration::class) && $user->boatOperator instanceof BoatOperator && $data['operator_type'] == 'boat_operator' && ID::decode($data['hash_operator_id']) == $user->boatOperator->id) {
            $agent = $this->agentRepository->getById(ID::decode($data['hash_agent_id']));
            $collaboration = $this->collaborationRepository->createFromBoatOperatorToAgent($user->boatOperator, $agent);
            return $this->sendResponse($collaboration, 'Collaboration stored successfully');
        } elseif ($user->can('create', Collaboration::class) && $user->cruiseOperator instanceof CruiseOperator && $data['operator_type'] == 'cruise_operator' && ID::decode($data['hash_operator_id']) == $user->cruiseOperator->id) {
            $agent = $this->agentRepository->getById(ID::decode($data['hash_agent_id']));
            $collaboration = $this->collaborationRepository->createFromCruiseOperatorToAgent($user->cruiseOperator, $agent);
            return $this->sendResponse($collaboration, 'Collaboration stored successfully');
        } elseif ($user->can('create', Collaboration::class) && $user->agent instanceof Agent && ID::decode($data['hash_agent_id']) == $user->agent->id) {
            switch ($data['operator_type']) {
                case 'boat_operator':
                    $boatOperator = $this->boatOperatorRepository->getById(ID::decode($data['hash_operator_id']));
                    $collaboration = $this->collaborationRepository->createFromAgentToBoatOperator($user->agent, $boatOperator);
                    return $this->sendResponse($collaboration, 'Collaboration stored successfully');
                case 'cruise_operator':
                    $cruiseOperator = $this->cruiseOperatorRepository->getById(ID::decode($data['hash_operator_id']));
                    $collaboration = $this->collaborationRepository->createFromAgentToCruiseOperator($user->agent, $cruiseOperator);
                    return $this->sendResponse($collaboration, 'Collaboration stored successfully');
            }
        } else {
            return $this->sendError('Invalid collaboration request');
        }
    }

    public function approve(CollaborationRequest $request, $hashId)
    {
        $user = Auth::user();
        $collaboration = $this->collaborationRepository->getById(ID::decode($hashId));
        if ($user->can('approveAsOperator', $collaboration)) {
            $collaboration = $this->collaborationRepository->approveAsOperator($collaboration);
            return $this->sendResponse($collaboration, 'Collaboration approved successfully');
        } elseif ($user->can('approveAsAgent', $collaboration)) {
            $collaboration = $this->collaborationRepository->approveAsAgent($collaboration);
            return $this->sendResponse($collaboration, 'Collaboration approved successfully');
        }
        return $this->sendError('Can not approve collaboration');
    }

    public function destroy($hashId)
    {
        $collaboration = $this->collaborationRepository->getById(ID::decode($hashId));
        $this->authorize('view', $collaboration);
        $this->collaborationRepository->deleteById($collaboration->id);
        return $this->sendResponse(null, 'Collaboration deleted successfully');
    }

}
