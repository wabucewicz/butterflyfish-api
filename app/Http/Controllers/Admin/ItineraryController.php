<?php

namespace App\Http\Controllers\Admin;

use App\Models\Itinerary;
use App\Repositories\ItineraryRepository;
use App\Utils\ID;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItineraryController extends Controller
{
    private $itineraryRepository;

    public function __construct(ItineraryRepository $itineraryRepository)
    {
        $this->itineraryRepository = $itineraryRepository;
    }

    public function index()
    {
//        $user = Auth::user();
//        if ($user->can('listAll', Itinerary::class)) $itineraries = $this->itineraryRepository->all();
//        elseif ($user->can('listAssociated', Itinerary::class)) $itineraries = $this->itineraryRepository->allForOperator($user->operator);
//        else return abort(403, 'Unauthorized');
        $itineraries = $this->itineraryRepository->all();
        return $this->sendResponse($itineraries, 'Itineraries retrieved successfully');
    }

    public function show($hashId)
    {
        $itinerary = $this->itineraryRepository->getById(ID::decode($hashId));
        return $this->sendResponse($itinerary, 'Itinerary retrieved successfully');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $itinerary = $this->itineraryRepository->create($data);
        if (isset($data['keywords'])) $this->itineraryRepository->updateKeywords($itinerary, $data['keywords']);
        $itinerary = $this->itineraryRepository->updateOperator($itinerary, Auth::user()->operator);
        logActivity('Stored itinerary: ' . $itinerary->hash_id, Auth::user());
        return $this->sendResponse($itinerary, 'Itinerary stored successfully');
    }

    public function update(Request $request, $hashId)
    {
        $data = $request->all();
        $itinerary = $this->itineraryRepository->updateById(ID::decode($hashId), $data);
        if (isset($data['keywords'])) $this->itineraryRepository->updateKeywords($itinerary, $data['keywords']);
        logActivity('Updated itinerary: ' . $itinerary->hash_id, Auth::user());
        return $this->sendResponse($itinerary, 'Itinerary updated successfully');
    }

    public function destroy($hashId)
    {
        $this->itineraryRepository->deleteById(ID::decode($hashId));
        logActivity('Deleted itinerary: ' . $hashId, Auth::user());
        return $this->sendResponse(null, 'Itinerary deleted successfully');
    }

    public function embarkations()
    {
        $embarkations = $this->itineraryRepository->getEmbarkations();
        return $this->sendResponse($embarkations, 'Embarkations retrieved successfully');
    }

    public function disembarkations()
    {
        $disembarkations = $this->itineraryRepository->getDisembarkations();
        return $this->sendResponse($disembarkations, 'Disembarkations retrieved successfully');
    }


}
