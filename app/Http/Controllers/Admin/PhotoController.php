<?php

namespace App\Http\Controllers\Admin;

use App\Models\Photo;
use App\Repositories\PhotoRepository;
use App\Utils\ID;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
use Response;
use stdClass;

class PhotoController extends Controller
{
    private $photo;

    const MODEL_REPOSITORIES = [
        'boat' => 'App\Repositories\BoatRepository',
        'cabin' => 'App\Repositories\CabinRepository',
        'deck' => 'App\Repositories\DeckRepository'
    ];

    public function __construct(PhotoRepository $photo)
    {
        $this->photo = $photo;
    }

    public function index($model, $hashId)
    {
        if (!array_key_exists($model, self::MODEL_REPOSITORIES)) {
            return Response::json(false, 400);
        }
        $repo = app(self::MODEL_REPOSITORIES[$model]);
        $object = $repo->getById(ID::decode($hashId));
        $files = [];
        foreach ($object->photos()->orderBy('order', 'asc')->get() as $photo) {
            $success = new stdClass();
            $success->name = $photo->filename . '.jpg';
            $success->size = $photo->file_size;
            $success->url = asset($photo->path . $photo->filename . '.jpg');
            $success->thumbnailUrl = asset($photo->path . $photo->filename . '_thumb.jpg');
            $success->deleteUrl = action('Admin\PhotoController@destroy', $photo->hash_id);
            $success->deleteType = 'DELETE';
            $success->fileID = $photo->hash_id;
            $files[] = $success;
        }
        return Response::json(['files'=> $files], 200);
    }

    public function store(Request $request, $model, $hashId)
    {
        if (!array_key_exists($model, self::MODEL_REPOSITORIES)) {
            return Response::json(false, 400);
        }
        $repo = app(self::MODEL_REPOSITORIES[$model]);
        $object = $repo->getById(ID::decode($hashId));
        $data = $request->all();
        $files = [];
        foreach ($data['files'] as $inputPhoto) {
            $filename  = str_slug($object->name) . '-butterflyfish-' . substr(str_shuffle(md5(time())),0,8);
            $path = Photo::STORAGE_LOCATION . $model . '/' . $object->hash_id . '/';
            if(!File::exists($path)) {
                File::makeDirectory($path, 0775, true);
            }
            $photoFile = Image::make($inputPhoto->getRealPath())
                ->resize(null, 600, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->insert(public_path(Photo::WATERMARK_PATH), 'bottom-right', 30, 30)
                ->save(public_path($path . $filename . '.jpg'));
            Image::make($inputPhoto->getRealPath())
                ->fit(350, 200)
                ->save(public_path($path . $filename . '_thumb.jpg'));
            $photoModel = new Photo([
                'filename' => $filename,
                'path' => $path,
                'file_size' => $photoFile->filesize(),
                'order' => 0
            ]);
            $object->photos()->save($photoModel);
            $success = new stdClass();
            $success->name = $filename . '.jpg';
            $success->size = $photoFile->filesize();
            $success->url = asset($path . $filename . '.jpg');
            $success->thumbnailUrl = asset($path . $filename . '_thumb.jpg');;
            $success->deleteUrl = action('Admin\PhotoController@destroy', $photoModel->hash_id);
            $success->deleteType = 'DELETE';
            $success->fileID = $photoModel->hash_id;
            $files[] = $success;
        }
        return Response::json(['files'=> $files], 200);
    }

    public function destroy($hashId)
    {
        $photo = Photo::find(ID::decode($hashId));
        File::delete($photo->path . $photo->filename);
        $photo->delete();
        $success = new stdClass();
        $success->filename = true;
        return Response::json(['files'=> [$success]], 200);
    }

    public function updatePhotosOrder(Request $request)
    {
        $this->photo->updateOrder($request->all());
        return Response::json(['success'=> true], 200);
    }
}
