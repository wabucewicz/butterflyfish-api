<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\ItineraryKeywordRepository;
use App\Utils\ID;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItineraryKeywordController extends Controller
{
    private $itineraryKeywordRepository;

    public function __construct(ItineraryKeywordRepository $itineraryKeywordRepository)
    {
        $this->itineraryKeywordRepository = $itineraryKeywordRepository;
    }

    public function index()
    {
        $itineraryKeywords = $this->itineraryKeywordRepository->all();
        return $this->sendResponse($itineraryKeywords, 'Itinerary keywords retrieved successfully');
    }
    public function show($hashId)
    {
        $itineraryKeyword = $this->itineraryKeywordRepository->getById(ID::decode($hashId));
        return $this->sendResponse($itineraryKeyword, 'Itinerary keyword retrieved successfully');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $itineraryKeyword = $this->itineraryKeywordRepository->create($data);
        return $this->sendResponse($itineraryKeyword, 'Itinerary keyword stored successfully');
    }

    public function update(Request $request, $hashId)
    {
        $data = $request->all();
        $itineraryKeyword = $this->itineraryKeywordRepository->updateById(ID::decode($hashId), $data);
        return $this->sendResponse($itineraryKeyword, 'Itinerary keyword updated successfully');
    }

    public function destroy($hashId)
    {
        $this->itineraryKeywordRepository->deleteById(ID::decode($hashId));
        return $this->sendResponse(null, 'Itinerary keyword deleted successfully');
    }


}
