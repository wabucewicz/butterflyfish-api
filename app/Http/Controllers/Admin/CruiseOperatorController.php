<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Repositories\CruiseOperatorRepository;
use App\Utils\ID;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CruiseOperatorController extends Controller
{
    private $cruiseOperatorRepository;

    public function __construct(CruiseOperatorRepository $cruiseOperatorRepository)
    {
        $this->cruiseOperatorRepository = $cruiseOperatorRepository;

    }

    public function index()
    {
        $operators = $this->cruiseOperatorRepository->all();
        return $this->sendResponse($operators, 'Operators retrieved successfully');

    }

    public function show($hashId)
    {
        $operator = $this->cruiseOperatorRepository->getById(ID::decode($hashId));
        return $this->sendResponse($operator, 'Operator retrieved successfully');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $operator = $this->cruiseOperatorRepository->create($data);
        if (isset($data['users'])) {
            foreach ($data['users'] as $userHashId) {
                $user = User::find(ID::decode($userHashId));
                if ($user) {
                    $operator->users()->save($user);
                }
            }
        }
        return $this->sendResponse($operator, 'Operator stored successfully');
    }

    public function update(Request $request, $hashId)
    {
        $data = $request->all();
        $operator = $this->cruiseOperatorRepository->updateById(ID::decode($hashId), $data);
        if (isset($data['users'])) {
            foreach ($operator->users as $user) {
                $user->cruiseOperator()->dissociate();
                $user->save();
            }
            foreach ($data['users'] as $userHashId) {
                $user = User::find(ID::decode($userHashId));
                if ($user) {
                    $operator->users()->save($user);
                }
            }
        }
        return $this->sendResponse($operator, 'Operator updated successfully');
    }

    public function destroy($hashId)
    {
        $this->cruiseOperatorRepository->deleteById(ID::decode($hashId));
        return $this->sendResponse(null, 'Operator deleted successfully');
    }
}
