<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\ImportFromCsvSource;
use App\Repositories\CsvSourceRepository;
use App\Utils\ID;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CsvSourceController extends Controller
{
    private $csvSourceRepository;

    public function __construct(CsvSourceRepository $csvSourceRepository)
    {
        $this->csvSourceRepository = $csvSourceRepository;
    }

    public function index()
    {
        $csvSources = $this->csvSourceRepository->all();
        return $this->sendResponse($csvSources, 'Csv sources retrieved successfully');
    }
    public function show($hashId)
    {
        $csvSource = $this->csvSourceRepository->getById(ID::decode($hashId));
        return $this->sendResponse($csvSource, 'Csv source retrieved successfully');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $csvSource = $this->csvSourceRepository->create($data);
        return $this->sendResponse($csvSource, 'Csv source stored successfully');
    }

    public function update(Request $request, $hashId)
    {
        $data = $request->all();
        $csvSource = $this->csvSourceRepository->updateById(ID::decode($hashId), $data);
        return $this->sendResponse($csvSource, 'Csv source updated successfully');
    }

    public function destroy($hashId)
    {
        $this->csvSourceRepository->deleteById(ID::decode($hashId));
        return $this->sendResponse(null, 'Csv source deleted successfully');
    }

    public function import($id)
    {
        $csvSource = $this->csvSourceRepository->getById(ID::decode($id));

        if (empty($csvSource)) {
            return $this->sendError('Csv source not found');
        }

        $jobId = $this->dispatch(new ImportFromCsvSource($csvSource));

        return $this->sendResponse(['job' => ID::encode($jobId)], 'Csv import job started');
    }


}
