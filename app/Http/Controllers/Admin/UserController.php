<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserRequest;
use App\Repositories\UserRepository;
use App\Utils\ID;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $users = $this->userRepository->all();
        return $this->sendResponse($users, 'Users retrieved successfully');

    }

    public function show($hashId)
    {
        $user = $this->userRepository->getById(ID::decode($hashId));
        return $this->sendResponse($user, 'User retrieved successfully');
    }

    public function store(UserRequest $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $user = $this->userRepository->create($data);
        if (isset($data['hash_boat_operator_id'])) $this->userRepository->updateBoatOperator($user, ID::decode($data['hash_boat_operator_id']));
        if (isset($data['hash_cruise_operator_id'])) $this->userRepository->updateCruiseOperator($user, ID::decode($data['hash_cruise_operator_id']));
        if (isset($data['hash_agent_id'])) $this->userRepository->updateAgent($user, ID::decode($data['hash_agent_id']));
        if (isset($data['hash_role_id'])) $this->userRepository->updateRole($user, ID::decode($data['hash_role_id']));
        return $this->sendResponse($user, 'User stored successfully');
    }

    public function update(UserRequest $request, $hashId)
    {
        $data = $request->all();
        if (isset($data['password']) && $data['password'] != '') $data['password'] = Hash::make($data['password']); else unset($data['password']);
        $user = $this->userRepository->updateById(ID::decode($hashId), $data);
        if (isset($data['hash_boat_operator_id'])) $this->userRepository->updateBoatOperator($user, ID::decode($data['hash_boat_operator_id']));
        if (isset($data['hash_cruise_operator_id'])) $this->userRepository->updateCruiseOperator($user, ID::decode($data['hash_cruise_operator_id']));
        if (isset($data['hash_agent_id'])) $this->userRepository->updateAgent($user, ID::decode($data['hash_agent_id']));
        if (isset($data['hash_role_id'])) $this->userRepository->updateRole($user, ID::decode($data['hash_role_id']));
        return $this->sendResponse($user, 'User updated successfully');
    }

    public function destroy($hashId)
    {
        $this->userRepository->deleteById(ID::decode($hashId));
        return $this->sendResponse(null, 'User deleted successfully');
    }
}
