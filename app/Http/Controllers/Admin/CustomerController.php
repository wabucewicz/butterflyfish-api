<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CustomerRequest;
use App\Repositories\CustomerRepository;
use App\Utils\ID;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    private $customerRepository;

    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    public function index()
    {
        $customers = $this->customerRepository->all();
        return $this->sendResponse($customers, 'Customers retrieved successfully');
    }

    public function show($hashId)
    {
        $customer = $this->customerRepository->getById(ID::decode($hashId));
        return $this->sendResponse($customer, 'Customer retrieved successfully');
    }

    public function store(CustomerRequest $request)
    {
        $data = $request->all();
        $customer = $this->customerRepository->create($data);
        return $this->sendResponse($customer, 'Customer stored successfully');
    }

    public function update(CustomerRequest $request, $hashId)
    {
        $data = $request->all();
        $customer = $this->customerRepository->updateById(ID::decode($hashId), $data);
        return $this->sendResponse($customer, 'Customer updated successfully');
    }

    public function destroy($hashId)
    {
        $this->customerRepository->deleteById(ID::decode($hashId));
        return $this->sendResponse(null, 'Customer deleted successfully');
    }
}
