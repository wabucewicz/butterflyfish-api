<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\PermissionRepository;
use App\Utils\ID;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    private $permissionRepository;

    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    public function index()
    {
        $permissions = $this->permissionRepository->all();
        return $this->sendResponse($permissions, 'Permissions retrieved successfully');

    }

    public function show($hashId)
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function update(Request $request, $hashId)
    {
        //
    }

    public function destroy($hashId)
    {
        //
    }

}
