<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\DeckRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeckController extends Controller
{
    private $deckRepository;

    public function __construct(DeckRepository $deckRepository)
    {
        $this->deckRepository = $deckRepository;
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $hashId)
    {
        //
    }

    public function destroy($hashId)
    {
        //
    }
}
