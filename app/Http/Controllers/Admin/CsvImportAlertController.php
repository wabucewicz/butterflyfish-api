<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\ImportFromCsv;
use App\Repositories\CsvImportAlertRepository;
use App\Repositories\CsvSourceRepository;
use App\Utils\ID;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CsvImportAlertController extends Controller
{
    private $csvImportAlertRepository;

    public function __construct(CsvImportAlertRepository $csvImportAlertRepository)
    {
        $this->csvImportAlertRepository = $csvImportAlertRepository;
    }

    public function index()
    {
        $csvImportAlerts = $this->csvImportAlertRepository->all();
        return $this->sendResponse($csvImportAlerts, 'Csv import alerts retrieved successfully');
    }
    public function show($hashId)
    {
        $csvImportAlert = $this->csvImportAlertRepository->getById(ID::decode($hashId));
        return $this->sendResponse($csvImportAlert, 'Csv import alert retrieved successfully');
    }

    public function store(Request $request)
    {
        //
    }

    public function update(Request $request, $hashId)
    {
        //
    }

    public function destroy($hashId)
    {
        $this->csvImportAlertRepository->deleteById(ID::decode($hashId));
        return $this->sendResponse(null, 'Csv import alert deleted successfully');
    }


}
