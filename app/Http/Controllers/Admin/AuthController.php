<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\PasswordResetCheckTokenRequest;
use App\Http\Requests\PasswordResetRequest;
use App\Http\Requests\PasswordResetSendLinkRequest;
use App\Models\User;
use App\Notifications\PasswordReset;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    use ResetsPasswords;

    public function authenticate(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token'));
    }

    public function getAuthenticatedUser()
    {
        $user = Auth::user();
        $user->load(['role', 'boatOperator', 'cruiseOperator']);
        $user->permissions = isset($user->role) ? $user->role->permissions->pluck('name') : [];
        return $this->sendResponse($user, 'User retrieved successfully');
    }

    public function sendPasswordResetLink(PasswordResetSendLinkRequest $request)
    {
        $email = $request->email;
        $user = User::where('email', $email)->first();
        if (!empty($user)) {
            $token = str_random(30);
            $pwdResetTable = DB::table(config('auth.passwords.users.table'));
            if ($pwdResetTable->where('email', $email)->count()) {
                $pwdResetTable->where('email', $email)->update(['token' => $token, 'created_at' => Carbon::now()->toDateTimeString()]);
            } else {
                $pwdResetTable->insert([
                    'email' => $user->email,
                    'token' => $token
                ]);
            }
            $user->notify(new PasswordReset($token));
        }
        return $this->sendResponse(null, 'Success');
    }

    public function checkPasswordResetToken(PasswordResetCheckTokenRequest $request)
    {
        if (!empty($request->token) && DB::table(config('auth.passwords.users.table'))->where('token', $request->token)->where('created_at', '>', Carbon::now()->subMinutes(60)->toDateTimeString())->count()) {
            return $this->sendResponse(null);
        }
        return $this->sendError(null);
    }

    public function passwordReset(PasswordResetRequest $request)
    {
        if (!empty($request->token) && $row = DB::table(config('auth.passwords.users.table'))->where('token', $request->token)->where('created_at', '>', Carbon::now()->subMinutes(60)->toDateTimeString())->first()) {
            $user = User::where('email', $row->email)->first();
            $user->password = bcrypt($request->password);
            $user->save();
            return $this->sendResponse(null);
        }
        return $this->sendError(null);
    }
}
