<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\BoatRepository;
use App\Repositories\CruiseRepository;
use App\Repositories\ItineraryRepository;
use App\Utils\ID;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CruiseController extends Controller
{
    private $cruiseRepository;

    public function __construct(CruiseRepository $cruiseRepository)
    {
        $this->cruiseRepository = $cruiseRepository;
    }

    public function index()
    {
        $cruises = $this->cruiseRepository->all();
        return $this->sendResponse($cruises, 'Cruises retrieved successfully');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($hashId)
    {
        $cruise = $this->cruiseRepository->getById(ID::decode($hashId));
        return $this->sendResponse($cruise, 'Cruise retrieved successfully');
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $hashId)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function getBoatData(Request $request)
    {
//        try {
            return response()->json([
                'boat' => $this->boat->find($request->id, ['cabins']),
                'cruises' => $this->cruiseRepository->getCruisesEvents($request->id)
            ], 200);
//        } catch (\Exception $e) {
//            return response()->json(false, 200);
//        }
    }


}
