<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BoatRequest;
use App\Http\Requests\CabinRequest;
use App\Http\Requests\CruiseRequest;
use App\Http\Requests\DeckRequest;
use App\Models\Agent;
use App\Models\Boat;
use App\Models\Cabin;
use App\Models\Cruise;
use App\Models\CruiseOperator;
use App\Models\Deck;
use App\Models\BoatOperator;
use App\Repositories\BoatRepository;
use App\Repositories\CruiseOperatorRepository;
use App\Repositories\CruiseRepository;
use App\Services\PromotionService;
use App\Utils\ID;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BoatController extends Controller
{
    private $boatRepository;
    private $cruiseRepository;
    private $promotionService;
    private $cruiseOperatorRepository;

    public function __construct(BoatRepository $boatRepository, CruiseRepository $cruiseRepository, PromotionService $promotionService, CruiseOperatorRepository $cruiseOperatorRepository)
    {
        $this->boatRepository = $boatRepository;
        $this->cruiseRepository = $cruiseRepository;
        $this->promotionService = $promotionService;
        $this->cruiseOperatorRepository = $cruiseOperatorRepository;
    }

    public function index(BoatRequest $request)
    {
        $user = Auth::user();
        if ($user->can('listAll', Boat::class)) {
            $boats = $this->boatRepository->all();
        } elseif ($user->can('listAssociated', Boat::class) && $user->boatOperator instanceof BoatOperator) {
            $boats = $this->boatRepository->allForBoatOperator($user->boatOperator);
        } elseif ($user->can('listAssociated', Boat::class) && $user->cruiseOperator instanceof CruiseOperator) {
            $boats = $this->boatRepository->allForCruiseOperator($user->cruiseOperator);
        } elseif ($user->can('listAssociated', Boat::class) && $user->agent instanceof Agent) {
            $boats = $this->boatRepository->allForAgent($user->agent);
        } else {
            $boats= [];
        }
        return $this->sendResponse($boats, 'Boats retrieved successfully');
    }

    public function show(BoatRequest $request, $hashId)
    {
        $boat = $this->boatRepository->getById(ID::decode($hashId));
        $this->authorize('view', $boat);
        return $this->sendResponse($boat, 'Boat retrieved successfully');
    }

    public function store(BoatRequest $request)
    {
        $this->authorize('create', Boat::class);
        $data = $request->all();
        $boat = $this->boatRepository->create($data);
        if (isset($data['boat_operators'])) $this->boatRepository->syncBoatOperators($boat, ID::decode($data['boat_operators']));
        logActivity('Stored boat: ' . $boat->name, Auth::user());
        return $this->sendResponse($boat, 'Boat stored successfully');
    }

    public function update(BoatRequest $request, $hashId)
    {
        $data = $request->all();
        $boat = $this->boatRepository->getById(ID::decode($hashId));
        $this->authorize('update', $boat);
        $boat = $this->boatRepository->updateById($boat->id, $data);
        if (isset($data['boat_operators'])) $this->boatRepository->syncBoatOperators($boat, ID::decode($data['boat_operators']));
        logActivity('Updated boat: ' . $boat->name, Auth::user());
        return $this->sendResponse($boat, 'Boat updated successfully');
    }

    public function destroy(BoatRequest $request, $hashId)
    {
        $boat = $this->boatRepository->getById(ID::decode($hashId));
        $this->authorize('delete', $boat);
        $this->boatRepository->deleteById($boat->id);
        logActivity('Deleted boat: ' . $boat->name, Auth::user());
        return $this->sendResponse(null, 'Boat deleted successfully');
    }


    // DECKS

    public function indexDeck(DeckRequest $request, $boatHashId)
    {
        $this->authorize('listAssociated', Deck::class);
        $boat = $this->boatRepository->getById(ID::decode($boatHashId));
        $decks = $this->boatRepository->allDeck($boat);
        return $this->sendResponse($decks, 'Decks retrieved successfully');
    }

    public function showDeck(DeckRequest $request, $boatHashId, $deckHashId)
    {
        $boat = $this->boatRepository->getById(ID::decode($boatHashId));
        $deck = $this->boatRepository->getByIdDeck($boat, ID::decode($deckHashId));
        $this->authorize('view', $deck);
        return $this->sendResponse($deck, 'Deck retrieved successfully');
    }

    public function storeDeck(DeckRequest $request, $boatHashId)
    {
        $boat = $this->boatRepository->getById(ID::decode($boatHashId));
        $this->authorize('create', [Deck::class, $boat]);
        $deck = $this->boatRepository->storeDeck($boat, $request->all());
        logActivity('Stored deck: ' . $deck->name . ' on boat: ' . $deck->boat->name, Auth::user());
        return $this->sendResponse($deck, 'Deck stored successfully');
    }

    public function updateDeck(DeckRequest $request, $boatHashId, $deckHashId)
    {
        $boat = $this->boatRepository->getById(ID::decode($boatHashId));
        $deck = $this->boatRepository->getByIdDeck($boat, ID::decode($deckHashId));
        $this->authorize('update', $deck);
        $deck = $this->boatRepository->updateDeck($boat, $deck->id, $request->all());
        logActivity('Updated deck: ' . $deck->name . ' on boat: ' . $deck->boat->name, Auth::user());
        return $this->sendResponse($deck, 'Deck updated successfully');
    }

    public function destroyDeck(DeckRequest $request, $boatHashId, $deckHashId)
    {
        $boat = $this->boatRepository->getById(ID::decode($boatHashId));
        $deck = $this->boatRepository->getByIdDeck($boat, ID::decode($deckHashId));
        $this->authorize('delete', $deck);
        $this->boatRepository->destroyDeck($boat, $deck->id);
        logActivity('Deleted deck: ' . $deck->name . ' on boat: ' . $boat->name, Auth::user());
        return $this->sendResponse(null, 'Deck deleted successfully');
    }


    // CABINS

    public function indexCabin(CabinRequest $request, $boatHashId)
    {
        $this->authorize('listAssociated', Cabin::class);
        $boat = $this->boatRepository->getById(ID::decode($boatHashId));
        $cabins = $this->boatRepository->allCabin($boat);
        return $this->sendResponse($cabins, 'Cabins retrieved successfully');
    }

    public function showCabin(CabinRequest $request, $boatHashId, $cabinHashId)
    {
        $boat = $this->boatRepository->getById(ID::decode($boatHashId));
        $cabin = $this->boatRepository->getByIdCabin($boat, ID::decode($cabinHashId));
        $this->authorize('view', $cabin);
        return $this->sendResponse($cabin, 'Cabin retrieved successfully');
    }

    public function storeCabin(CabinRequest $request, $boatHashId)
    {
        $boat = $this->boatRepository->getById(ID::decode($boatHashId));
        $this->authorize('create', [Cabin::class, $boat]);
        $cabin = $this->boatRepository->storeCabin($boat, $request->all());
        logActivity('Stored cabin: ' . $cabin->name . ' on boat: ' . $boat->name, Auth::user());
        return $this->sendResponse($cabin, 'Cabin stored successfully');
    }

    public function updateCabin(CabinRequest $request, $boatHashId, $cabinHashId)
    {
        $boat = $this->boatRepository->getById(ID::decode($boatHashId));
        $cabin = $this->boatRepository->getByIdCabin($boat, ID::decode($cabinHashId));
        $this->authorize('update', $cabin);
        $cabin = $this->boatRepository->updateCabin($boat, $cabin->id, $request->all());
        logActivity('Updated cabin: ' . $cabin->name . ' on boat: ' . $boat->name, Auth::user());
        return $this->sendResponse($cabin, 'Cabin updated successfully');
    }

    public function destroyCabin(CabinRequest $request, $boatHashId, $cabinHashId)
    {
        $boat = $this->boatRepository->getById(ID::decode($boatHashId));
        $cabin = $this->boatRepository->getByIdCabin($boat, ID::decode($cabinHashId));
        $this->authorize('delete', $cabin);
        $this->boatRepository->destroyCabin($boat, $cabin->id);
        logActivity('Deleted cabin: ' . $cabin->name . ' on boat: ' . $boat->name, Auth::user());
        return $this->sendResponse(null, 'Cabin deleted successfully');
    }


    // CRUISES

    public function indexCruise(CruiseRequest $request, $boatHashId)
    {
        $user = Auth::user();
        $boat = $this->boatRepository->skipEagerLoad()->getById(ID::decode($boatHashId));
        if ($user->can('listAll', Cruise::class) || ($user->can('listAssociated', Cruise::class) && $user->boatOperator instanceof BoatOperator)) {
            $cruises = $this->cruiseRepository->allByBoat($boat);
        } elseif ($user->can('listAssociated', Cruise::class) && $user->cruiseOperator instanceof CruiseOperator) {
            $cruises = $this->cruiseRepository->allByBoatForCruiseOperator($boat, $user->cruiseOperator);
        } elseif ($user->can('listAssociated', Cruise::class) && $user->agent instanceof Agent) {
            $cruises = $this->cruiseRepository->allByBoatForAgent($boat, $user->agent);
        } else {
            $cruises = [];
        }
        return $this->sendResponse($cruises, 'Cruises retrieved successfully');

    }

    public function showCruise(CruiseRequest $request, $boatHashId, $cruiseHashId)
    {
        $boat = $this->boatRepository->skipEagerLoad()->getById(ID::decode($boatHashId));
        $cruise = $this->cruiseRepository->getByBoat($boat, ID::decode($cruiseHashId));
        $this->authorize('view', $cruise);
        $data = $cruise->toArray();
        $cabinFreeSpaces = [];
        $cabinPrices = [];
        foreach ($cruise->cruiseCabinOccupancies as $cabinOccupancy) {
            $cabinFreeSpaces[$cabinOccupancy->cabin->hashId] = $cabinOccupancy->cabin->max_pax - $cabinOccupancy->occupancy;
            $cabinPrices[$cabinOccupancy->cabin->hashId] = $cabinOccupancy->price;
        }
        $data['cabin_free_spaces'] = $cabinFreeSpaces;
        $data['cabin_prices'] = $cabinPrices;
        return $this->sendResponse($data, 'Cruise retrieved successfully');
    }

    public function storeCruise(CruiseRequest $request, $boatHashId)
    {
        $boat = $this->boatRepository->skipEagerLoad()->getById(ID::decode($boatHashId));
        $this->authorize('create', [Cruise::class, $boat]);
        $data = $request->all();
        if (isset($data['many']) && $data['many'] == true) {
            $startDate = Carbon::parse($data['start_date']);
            $endDate = Carbon::parse($startDate->format('Y-m-d'))->addDays($data['nights']);
            for ($i = 0; $data['count'] > $i; $i++) {
                if (!$this->cruiseRepository->checkIfDatesAvailable($boat, $startDate->format('Y-m-d'), $endDate->format('Y-m-d'))) {
                    return $this->sendError('Invalid dates range');
                }
                $startDate = Carbon::parse($endDate->format('Y-m-d'))->addDays($data['pause']);
                $endDate = Carbon::parse($startDate->format('Y-m-d'))->addDays($data['nights']);
            }
            $cruises = [];
            $startDate = Carbon::parse($data['start_date']);
            $endDate = Carbon::parse($startDate->format('Y-m-d'))->addDays($data['nights']);
            for ($i = 0; $data['count'] > $i; $i++) {
                $cruise = $this->cruiseRepository->createByBoat($boat, ['start_date' => $startDate, 'end_date' => $endDate]);
                $cruise = $this->setCruiseProperties($cruise, $data);
                $cruises[] = $this->cruiseRepository->getByBoat($boat, $cruise->id);
                $startDate = Carbon::parse($endDate->format('Y-m-d'))->addDays($data['pause']);
                $endDate = Carbon::parse($startDate->format('Y-m-d'))->addDays($data['nights']);
            }
            foreach ($cruises as $cruise) {
                $this->promotionService->checkCruisePromotions($cruise);
            }
            logActivity('Stored many cruises on boat: ' . $boat->name, Auth::user());
            return $this->sendResponse($cruises, 'Cruises stored successfully');
        } else {
            if (!$this->cruiseRepository->checkIfDatesAvailable($boat, $data['start_date'], $data['end_date'])) {
                return $this->sendError('Invalid dates range');
            }
            $cruise = $this->cruiseRepository->createByBoat($boat, []);
            $cruise = $this->setCruiseProperties($cruise, $data);
            $this->promotionService->checkCruisePromotions($cruise);
            $cruise = $this->cruiseRepository->getByBoat($boat, $cruise->id);
            logActivity('Stored cruise: ' . $cruise->start_date . ' - '. $cruise->end_date . ' on boat: ' . $boat->name, Auth::user());
            return $this->sendResponse($cruise, 'Cruise stored successfully');
        }
    }

    private function setCruiseProperties(Cruise $cruise, array $data)
    {
        $user = Auth::user();
        if ($user->can('setDates', $cruise) && isset($data['end_date'])) {
            $cruise = $this->cruiseRepository->setDates($cruise, $data['start_date'], $data['end_date']);
        }
        if ($user->can('setPlaces', $cruise)) {
            $cruise = $this->cruiseRepository->setPlaces($cruise, $data['start_place'], $data['end_place']);
        }
        if ($user->can('setConfirmation', $cruise) && isset($data['confirmed'])) {
            $cruise = $this->cruiseRepository->setConfirmation($cruise, $data['confirmed']);
        }
        if ($user->can('setItinerary', $cruise) && isset($data['hash_itinerary_id'])) {
            $cruise = $this->cruiseRepository->setItinerary($cruise, ID::decode($data['hash_itinerary_id']));
        }
        if ($user->can('setCurrency', $cruise) && isset($data['currency'])) {
            $cruise = $this->cruiseRepository->setCurrency($cruise, $data['currency']);
        }
        if (isset($data['occupancy_per_cruise']) && $data['occupancy_per_cruise'] == true) {
            if ($user->can('setPax', $cruise)) {
                $cruise = $this->cruiseRepository->setOccupancyPerCruisePax($cruise, $data['free_pax']);
            }
            if ($user->can('setPrice', $cruise)) {
                $cruise = $this->cruiseRepository->setOccupancyPerCruisePrice($cruise, $data['price_per_pax']);
            }
        } elseif (isset($data['occupancy_per_cruise']) && $data['occupancy_per_cruise'] == false) {
            if ($user->can('setPax', $cruise)) {
                $cruise = $this->cruiseRepository->setOccupancyPerCabinPax($cruise, $data['cabin_free_spaces']);
            }
            if ($user->can('setPrice', $cruise)) {
                $cruise = $this->cruiseRepository->setOccupancyPerCabinPrice($cruise, $data['cabin_prices']);
            }
        }
        if ($user->can('setSurcharges', $cruise) && isset($data['cruise_surcharges'])) {
            $cruise = $this->cruiseRepository->setSurcharges($cruise, $data['cruise_surcharges']);
        }
        if ($user->can('setPromotions', $cruise) && isset($data['cruise_promotions'])) {
            $cruise = $this->cruiseRepository->setPromotions($cruise, $data['cruise_promotions']);
        }
        $cruise->save();
        return $cruise;
    }

    public function updateCruise(CruiseRequest $request, $boatHashId, $cruiseHashId)
    {
        $boat = $this->boatRepository->getById(ID::decode($boatHashId));
        $cruise = $this->cruiseRepository->getByBoat($boat, ID::decode($cruiseHashId));
        $this->authorize('update', $cruise);
        $data = $request->all();
        if (isset($data['start_date']) && isset($data['end_date']) && !$this->cruiseRepository->checkIfDatesAvailable($boat, $data['start_date'], $data['end_date'], ID::decode($cruiseHashId))) {
            return $this->sendError('Invalid dates range');
        }
        $cruise = $this->setCruiseProperties($cruise, $data);
        $this->promotionService->checkCruisePromotions($cruise);
        $cruise = $this->cruiseRepository->getByBoat($boat, $cruise->id);
        logActivity('Updated cruise: ' . $cruise->start_date . ' - '. $cruise->end_date . ' on boat: ' . $boat->name, Auth::user());
        return $this->sendResponse($cruise, 'Cruise updated successfully');
    }

    public function destroyCruise(CruiseRequest $request, $boatHashId, $cruiseHashId)
    {
        $boat = $this->boatRepository->getById(ID::decode($boatHashId));
        $cruise = $this->cruiseRepository->getByBoat($boat, ID::decode($cruiseHashId));
        $this->authorize('delete', $cruise);
        $this->cruiseRepository->deleteByBoat($boat, $cruise->id);
        logActivity('Deleted cruise: ' . $cruise->start_date . ' - '. $cruise->end_date . ' on boat: ' . $boat->name, Auth::user());
        return $this->sendResponse(null, 'Cruise deleted successfully');
    }

    public function addPromotionToBoatCruises(Request $request, $boatHashId)
    {
        $data = $request->all();
        $boat = $this->boatRepository->getById(ID::decode($boatHashId));
        $cruises = $this->cruiseRepository->getCruisesByBoatFromDateRange($boat, $data['cruises_from'], $data['cruises_to']);
        foreach ($cruises as $cruise) {
            $this->authorize('setPromotions', $cruise);
        }
        foreach ($cruises as $cruise) {
            $this->cruiseRepository->addPromotion($cruise, $data);
            $this->promotionService->checkCruisePromotions($cruise);
        }
        logActivity('Added promotion to cruises on boat: ' . $boat->name, Auth::user());
        return $this->sendResponse(null, 'Promotion added successfully');
    }

    public function assignToCruiseOperator(Request $request, $boatHashId, $cruiseHashId)
    {
        $boat = $this->boatRepository->skipEagerLoad()->getById(ID::decode($boatHashId));
        $cruise = $this->cruiseRepository->getByBoat($boat, ID::decode($cruiseHashId));
        $this->authorize('assign', $cruise);
        $cruiseOperator = $this->cruiseOperatorRepository->getById(ID::decode($request->hash_cruise_operator_id));
        $cruise = $this->cruiseRepository->assignToCruiseOperator($cruise, $cruiseOperator);
        logActivity('Assigned boat: ' . $boat->name . ' to cruise operator: ' . $cruiseOperator->name, Auth::user());
        return $this->sendResponse($cruise, 'Cruise assigned successfully');
    }

    public function unassignFromCruiseOperator(Request $request, $boatHashId, $cruiseHashId)
    {
        $boat = $this->boatRepository->skipEagerLoad()->getById(ID::decode($boatHashId));
        $cruise = $this->cruiseRepository->getByBoat($boat, ID::decode($cruiseHashId));
        $this->authorize('unassign', $cruise);
        $cruiseOperatorName = $cruise->cruiseOperator ? $cruise->cruiseOperator->name : '-';
        $cruise = $this->cruiseRepository->unassignFromCruiseOperator($cruise);
        logActivity('Unassigned boat: ' . $boat->name . ' from cruise operator: ' . $cruiseOperatorName, Auth::user());
        return $this->sendResponse($cruise, 'Cruise unassigned successfully');
    }

}
