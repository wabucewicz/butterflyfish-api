<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\BoatRepository;
use App\Services\CsvService;
use App\Utils\ID;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DataExchangeController extends Controller
{
    private $csvService;
    private $boatRepository;

    public function __construct(CsvService $csvService, BoatRepository $boatRepository)
    {
        $this->csvService = $csvService;
        $this->boatRepository = $boatRepository;
    }

    public function csvExportByBoat($boatHashCode)
    {
        $boat = $this->boatRepository->where('hash_code', $boatHashCode)->first();
        if ($boat) {
            $this->csvService->export($boat->id);
        } else {
            return $this->sendError('Boat not found');
        }

    }

    public function csvExport()
    {
        $this->csvService->export();
    }

    public function csvDownload()
    {

    }
}
