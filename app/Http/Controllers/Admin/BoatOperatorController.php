<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Repositories\BoatOperatorRepository;
use App\Utils\ID;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BoatOperatorController extends Controller
{
    private $boatOperatorRepository;

    public function __construct(BoatOperatorRepository $boatOperatorRepository)
    {
        $this->boatOperatorRepository = $boatOperatorRepository;
    }

    public function index()
    {
        $operators = $this->boatOperatorRepository->all();
        return $this->sendResponse($operators, 'Operators retrieved successfully');

    }

    public function show($hashId)
    {
        $operator = $this->boatOperatorRepository->getById(ID::decode($hashId));
        return $this->sendResponse($operator, 'Operator retrieved successfully');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $operator = $this->boatOperatorRepository->create($data);
        if (isset($data['boats'])) $operator->boats()->sync(ID::decode($data['boats']));
        if (isset($data['users'])) {
            foreach ($data['users'] as $userHashId) {
                $user = User::find(ID::decode($userHashId));
                if ($user) {
                    $operator->users()->save($user);
                }
            }
        }
        return $this->sendResponse($operator, 'Operator stored successfully');
    }

    public function update(Request $request, $hashId)
    {
        $data = $request->all();
        $operator = $this->boatOperatorRepository->updateById(ID::decode($hashId), $data);
        if (isset($data['boats'])) $operator->boats()->sync(ID::decode($data['boats']));
        if (isset($data['users'])) {
            foreach ($operator->users as $user) {
                $user->boatOperator()->dissociate();
                $user->save();
            }
            foreach ($data['users'] as $userHashId) {
                $user = User::find(ID::decode($userHashId));
                if ($user) {
                    $operator->users()->save($user);
                }
            }
        }
        return $this->sendResponse($operator, 'Operator updated successfully');
    }

    public function destroy($hashId)
    {
        $this->boatOperatorRepository->deleteById(ID::decode($hashId));
        return $this->sendResponse(null, 'Operator deleted successfully');
    }
}
