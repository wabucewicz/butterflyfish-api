<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\RoleRepository;
use App\Utils\ID;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    private $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function index()
    {
        $roles = $this->roleRepository->all();
        return $this->sendResponse($roles, 'Roles retrieved successfully');

    }

    public function show($hashId)
    {
        $role = $this->roleRepository->getById(ID::decode($hashId));
        return $this->sendResponse($role, 'Role retrieved successfully');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['name'] = str_slug($data['display_name'], '-');
        $role = $this->roleRepository->store($data);
        if (isset($data['permissions'])) $role->permissions()->sync(ID::decode($data['permissions']));
        return $this->sendResponse($role, 'Role stored successfully');
    }

    public function update(Request $request, $hashId)
    {
        $data = $request->all();
        $data['name'] = str_slug($data['display_name'], '-');
        $role = $this->roleRepository->updateById(ID::decode($hashId), $data);
        if (isset($data['permissions'])) $role->permissions()->sync(ID::decode($data['permissions']));
        return $this->sendResponse($role, 'Role updated successfully');
    }

    public function destroy($hashId)
    {
        $this->roleRepository->deleteById(ID::decode($hashId));
        return $this->sendResponse(null, 'Role deleted successfully');
    }

}
