<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ReservationRequest;
use App\Models\Agent;
use App\Models\Reservation;
use App\Repositories\CruiseRepository;
use App\Repositories\ReservationRepository;
use App\Services\ReservationService;
use App\Utils\ID;
use App\Http\Controllers\Controller;
use Auth;

class ReservationController extends Controller
{
    private $reservationRepository;
    private $reservationService;
    private $cruiseRepository;

    public function __construct(ReservationRepository $reservationRepository, ReservationService $reservationService, CruiseRepository $cruiseRepository)
    {
        $this->reservationRepository = $reservationRepository;
        $this->reservationService = $reservationService;
        $this->cruiseRepository = $cruiseRepository;
    }

    public function index()
    {
        $user = Auth::user();
        if ($user->can('listAll', Reservation::class)) {
            $reservations = $this->reservationRepository->all();
        } elseif ($user->can('listAssociated', Reservation::class)) {
            $reservations = $this->reservationRepository->allForUser($user);
        } else {
            $reservations = [];
        }
        return $this->sendResponse($reservations, 'Reservations retrieved successfully');
    }

    public function show($hashId)
    {
        $reservation = $this->reservationRepository->getById(ID::decode($hashId));
        $this->authorize('view', $reservation);
        return $this->sendResponse($reservation, 'Reservation retrieved successfully');
    }

    public function store(ReservationRequest $request)
    {
        $params = $request->only(['hash_cruise_id', 'hash_customer_id', 'passengers_data']);
        $data['cruise_id'] = ID::decode($params['hash_cruise_id']);
        $data['customer_id'] = ID::decode($params['hash_customer_id']);
        $data['user_id'] = Auth::user()->id;
        $user = Auth::user();
        if (!empty($user) && $user->agent instanceof Agent) {
            $data['agent_id'] = $user->agent->id;
        }
        $cruise = $this->cruiseRepository->getById($data['cruise_id']);
        $this->authorize('create', [Reservation::class, $cruise]);
        if (count($params['passengers_data']) > $cruise->free_pax) {
            return $this->sendError('Not enough free pax');
        }
        foreach ($params['passengers_data'] as $key => $passenger) {
            $data['passengers_data_arr'][$key]['first_name'] = $passenger['first_name'];
            $data['passengers_data_arr'][$key]['last_name'] = $passenger['last_name'];
            if (!empty($passenger['hash_cabin_id'])) {
                $data['passengers_data_arr'][$key]['cabin_id'] = ID::decode($passenger['hash_cabin_id']);
            }
        }
        $reservation = $this->reservationService->create($data);
        return $this->sendResponse($reservation, 'Reservation stored successfully');
    }

    public function update(ReservationRequest $request, $hashId)
    {
        $params = $request->only(['passengers_data']);
        $reservation = $this->reservationRepository->getById(ID::decode($hashId));
        $this->authorize('update', [Reservation::class, $reservation]);
        $paxChange = count($reservation->passengers_data_arr) - count($params['passengers_data']);
        if (($reservation->cruise->free_pax + $paxChange) > $reservation->cruise->free_pax) {
            return $this->sendError('Not enough free pax');
        }
        foreach ($params['passengers_data'] as $key => $passenger) {
            $data['passengers_data_arr'][$key]['first_name'] = $passenger['first_name'];
            $data['passengers_data_arr'][$key]['last_name'] = $passenger['last_name'];
            if (!empty($passenger['hash_cabin_id'])) {
                $data['passengers_data_arr'][$key]['cabin_id'] = ID::decode($passenger['hash_cabin_id']);
            }
        }
        $reservation = $this->reservationService->update($reservation, $data);
        return $this->sendResponse($reservation, 'Reservation updated successfully');
    }

    public function destroy($hashId)
    {
        $reservation = $this->reservationRepository->getById(ID::decode($hashId));
        $this->authorize('delete', $reservation);
        $this->reservationService->delete($reservation);
        return $this->sendResponse(null, 'Reservation deleted successfully');
    }
}
