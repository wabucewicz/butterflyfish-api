<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\SurchargeRepository;
use App\Utils\ID;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SurchargeController extends Controller
{
    private $surchargeRepository;

    public function __construct(SurchargeRepository $surchargeRepository)
    {
        $this->surchargeRepository = $surchargeRepository;
    }

    public function index()
    {
        $surcharges = $this->surchargeRepository->all();
        return $this->sendResponse($surcharges, 'Surcharges retrieved successfully');

    }

    public function show($hashId)
    {
        $surcharge = $this->surchargeRepository->getById(ID::decode($hashId));
        return $this->sendResponse($surcharge, 'Surcharge retrieved successfully');
    }

    public function store(Request $request)
    {
//        $data = $request->all();
//        $surcharge = $this->surchargeRepository->store($data);
//        return $this->sendResponse( $surcharge, 'Surcharge stored successfully');
    }

    public function update(Request $request, $hashId)
    {
//        $data = $request->all();
//        $surcharge = $this->surchargeRepository->updateById(ID::decode($hashId), $data);
//        return $this->sendResponse($surcharge, 'Surcharge updated successfully');
    }

    public function destroy($hashId)
    {
//        $this->surchargeRepository->deleteById(ID::decode($hashId));
//        return $this->sendResponse(null, 'Surcharge deleted successfully');
    }

}
