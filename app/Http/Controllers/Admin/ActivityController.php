<?php

namespace App\Http\Controllers\Admin;

use App\Models\Activity;
use App\Http\Controllers\Controller;
use App\Repositories\ActivityRepository;

class ActivityController extends Controller
{
    private $activityRepository;

    public function __construct(ActivityRepository $activityRepository)
    {
        $this->activityRepository = $activityRepository;
    }

    public function index()
    {
        $activities = $this->activityRepository->all();
        return $this->sendResponse($activities, 'Activities retrieved successfully');
    }

}
