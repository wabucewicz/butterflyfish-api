<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Repositories\AgentRepository;
use App\Utils\ID;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgentController extends Controller
{
    private $agentRepository;

    public function __construct(AgentRepository $agentRepository)
    {
        $this->agentRepository = $agentRepository;

    }

    public function index()
    {
        $agents = $this->agentRepository->all();
        return $this->sendResponse($agents, 'Agents retrieved successfully');

    }

    public function show($hashId)
    {
        $agent = $this->agentRepository->getById(ID::decode($hashId));
        return $this->sendResponse($agent, 'Agent retrieved successfully');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $agent = $this->agentRepository->create($data);
        if (isset($data['users'])) {
            foreach ($data['users'] as $userHashId) {
                $user = User::find(ID::decode($userHashId));
                if ($user) {
                    $agent->users()->save($user);
                }
            }
        }
        return $this->sendResponse($agent, 'Agent stored successfully');
    }

    public function update(Request $request, $hashId)
    {
        $data = $request->all();
        $agent = $this->agentRepository->updateById(ID::decode($hashId), $data);
        if (isset($data['users'])) {
            foreach ($agent->users as $user) {
                $user->agent()->dissociate();
                $user->save();
            }
            foreach ($data['users'] as $userHashId) {
                $user = User::find(ID::decode($userHashId));
                if ($user) {
                    $agent->users()->save($user);
                }
            }
        }
        return $this->sendResponse($agent, 'Agent updated successfully');
    }

    public function destroy($hashId)
    {
        $this->agentRepository->deleteById(ID::decode($hashId));
        return $this->sendResponse(null, 'Agent deleted successfully');
    }
}
