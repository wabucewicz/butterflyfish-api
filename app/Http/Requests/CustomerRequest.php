<?php

namespace App\Http\Requests;


class CustomerRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'title' => 'required|string|in:mr,mrs',
                    'first_name' => 'required|string',
                    'last_name' => 'required|string',
                    'contact_email' => 'required|email',
                    'born_date' => 'required|date_format:Y-m-d|before:today',
                ];
            }
            case 'PUT':
            {
                return [
                    'title' => 'required|string|in:mr,mrs',
                    'first_name' => 'required|string',
                    'last_name' => 'required|string',
                    'contact_email' => 'required|email',
                    'born_date' => 'required|date_format:Y-m-d|before:today',
                ];
            }
            case 'PATCH':
            default:break;
        }
    }
}
