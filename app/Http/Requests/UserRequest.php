<?php

namespace App\Http\Requests;


use App\Models\User;
use App\Utils\ID;

class UserRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find(ID::decode($this->route('user')));
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'email' => 'required|email|unique:users,email',
                    'password' => 'required|min:6|max:128|confirmed',
                    'hash_role_id' => 'required|string|exists_hash_id:roles',
                    'hash_boat_operator_id' => 'nullable|string|exists_hash_id:boat_operators',
                    'hash_cruise_operator_id' => 'nullable|string|exists_hash_id:cruise_operators',
                    'hash_agent_id' => 'nullable|string|exists_hash_id:agents',
                ];
            }
            case 'PUT':
            {
                return [
                    'email' => 'required|email|unique:users,email,' . $user->id,
                    'password' => 'min:6|max:128|confirmed',
                    'hash_role_id' => 'required|string|exists_hash_id:roles',
                    'hash_boat_operator_id' => 'nullable|string|exists_hash_id:boat_operators',
                    'hash_cruise_operator_id' => 'nullable|string|exists_hash_id:cruise_operators',
                    'hash_agent_id' => 'nullable|string|exists_hash_id:agents',
                ];
            }
            case 'PATCH':
            default:break;
        }
    }
}
