<?php

namespace App\Http\Requests;


class CruiseRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                if (isset($this->many) && $this->many == true) {
                    return [
                        'start_date' => 'required',
                        'nights' => 'required',
                        'pause' => 'required',
                        'start_place' => 'required',
                        'end_place' => 'required',
                        'count' =>  'required'
                    ];
                } else {
                    return [
                        'start_date' => 'required',
                        'end_date' => 'required',
                        'start_place' => 'required',
                        'end_place' => 'required',
                        'occupancy_per_cruise' => 'required'
                    ];
                }
            }
            case 'PUT':
            {
                return [
//                    'start_date' => 'required',
//                    'end_date' => 'required',
//                    'start_place' => 'required',
//                    'end_place' => 'required',
//                    'occupancy_per_cruise' => 'required'
                ];
            }
            case 'PATCH':
            default:break;
        }
    }
}
