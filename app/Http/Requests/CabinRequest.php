<?php

namespace App\Http\Requests;


class CabinRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'name' => 'required',
                    'sng_beds' => 'required',
                    'dbl_beds' => 'required'
                ];
            }
            case 'PUT':
            {
                return [
                    'name' => 'required',
                    'sng_beds' => 'required',
                    'dbl_beds' => 'required'
                ];
            }
            case 'PATCH':
            default:break;
        }
    }
}
