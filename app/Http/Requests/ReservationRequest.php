<?php

namespace App\Http\Requests;


class ReservationRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'hash_cruise_id' => 'required|exists_hash_id:cruises',
                    'hash_customer_id' => 'required|exists_hash_id:customers',
                    'passengers_data.*.first_name' => 'required|string',
                    'passengers_data.*.last_name' => 'required|string',
                    'passengers_data.*.hash_cabin_id' => 'string|required_if_occupancy_per_cabin|available_space_in_cabin'
                ];
            }
            case 'PUT':
            {
                return [
                    'passengers_data.*.first_name' => 'required|string',
                    'passengers_data.*.last_name' => 'required|string',
                    'passengers_data.*.hash_cabin_id' => 'string|required_if_occupancy_per_cabin|available_space_in_cabin'
                ];
            }
            case 'PATCH':
            default:break;
        }
    }
}
