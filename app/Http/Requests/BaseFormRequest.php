<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class BaseFormRequest extends FormRequest
{
    public function response(array $errors)
    {
        return response()->error('Some fields are not filled correctly', $errors);
    }
}