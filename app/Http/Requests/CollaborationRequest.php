<?php

namespace App\Http\Requests;


class CollaborationRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            {
                return [];
            }
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'hash_operator_id' => 'required|string',
                    'operator_type' => 'required|string|in:boat_operator,cruise_operator',
                    'hash_agent_id' => 'required|string|exists_hash_id:agents',
                ];
            }
            case 'PUT':
            {
                return [];
            }
            case 'PATCH':
            {
                return [];
            }
            default:break;
        }
    }
}
