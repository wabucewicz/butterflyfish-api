<?php

namespace App\Http\Requests;


class GetOffersRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'query' => 'required|string|min:3',
                    'date_from' => 'required|date_format:Y-m-d|after:today',
                    'date_to' => 'required|date_format:Y-m-d|after:date_from',
                    'passengers' => 'required|in:1,2,3,4,5,6'
                ];
            }
            case 'PUT':
            {
                return [];
            }
            case 'PATCH':
            default:break;
        }
    }
}
